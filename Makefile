#!/usr/bin/make

DOCKER_KAFKA=docker-compose exec -T kafka
KAFKA_TOPICS=$(DOCKER_KAFKA) kafka-topics.sh --zookeeper zookeeper:2181
EMAIL_TOPIC=--topic notification.emails.v1
EVENT_TOPIC=--topic events.v1
BOOTSTRAP_SERVER=--bootstrap-server kafka:9092
KAFKA_CONSUMER=$(DOCKER_KAFKA) kafka-console-consumer.sh

DOCKER_MONGO=docker-compose exec -T order-service-mongo

kafka-email-consumer:
	$(KAFKA_CONSUMER) $(BOOTSTRAP_SERVER) $(EMAIL_TOPIC) --from-beginning

kafka-event-consumer:
	$(KAFKA_CONSUMER) $(BOOTSTRAP_SERVER) $(EVENT_TOPIC) --from-beginning