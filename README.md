[![pipeline status](https://gitlab.com/foodprojectdemo/order-service/badges/main/pipeline.svg)](https://gitlab.com/foodprojectdemo/order-service/-/pipelines)
[![coverage report](https://gitlab.com/foodprojectdemo/order-service/badges/main/coverage.svg?job=test-ut)](https://foodprojectdemo.gitlab.io/order-service/coverage/ut/)
[![coverage report](https://gitlab.com/foodprojectdemo/order-service/badges/main/coverage.svg?job=test-it)](https://foodprojectdemo.gitlab.io/order-service/coverage/it/)


# Order Service

Order Service - a core reactive RESTFul microservice with Kotlin, DDD and Event-driven architecture, Spring WebFlux,
MongoDB and Apache Kafka. Order service manages shopping carts of customers, calculates and applies discounts and allows
consumers to place orders.

### REST API docs

[REST API docs](https://foodprojectdemo.gitlab.io/order-service/docs/)

### Order state machine

![images/order_state_machine.png](images/order_state_machine.png)

### Domain Model of Promos

![images/promo.png](images/promo.png)

The most difficult part of domain model are promos. Restaurants want to attract new customers and increase their
traffic. For it, they are offering promos.

Promos can be of two types: promo action and promo code. Any promo gives some benefit. It could be a discount, gift,
etc.

The [promo](https://gitlab.com/foodprojectdemo/order-service/-/tree/master/src/main/kotlin/pro/korobovn/foodproject/orderservice/domain/promo)
action gives benefit if some condition is satisfied. For example, ``GiftByCartAmountPromoAction`` gives a gift if the
cart satisfies ``ByCartAmountCondition``.

The promo code gives benefit without any conditions.

## Technical Overview

### Reliable domain event publishing

The service implements a reliable approach to sending domain events. Domain event sending is implemented as
Cross-cutting concern with AspectJ.
The [aspect](https://gitlab.com/foodprojectdemo/order-service/-/tree/master/src/main/kotlin/pro/korobovn/foodproject/orderservice/infrastructure/persistence/SaveAggregateWithEventsAspect.kt)
intercepts the call to save aggregates and within the transaction saves aggregates and their events too. Then the
dedicated thread publishes the events.
