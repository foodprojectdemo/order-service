package com.gitlab.foodprojectdemo.orderservice.ui.rest.customer

import com.gitlab.foodprojectdemo.orderservice.application.customer.CustomerService
import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.ui.rest.AbstractUITest
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation
import reactor.kotlin.core.publisher.toMono

@WebFluxTest(CustomerController::class)
private class CustomerControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var customerService: CustomerService

    @Test
    fun `should sign up customer`(customer: Customer) {
        whenever(customerService.signUpCustomer()).thenReturn(customer.toMono())

        webClient.post()
            .uri("/api/v1/customers")
            .exchange()
            .expectStatus().isCreated
            .expectHeader().valueMatches(HttpHeaders.LOCATION, ".+/api/v1/customers/${customer.id}")
            .expectBody()
            .jsonPath("$.id").isEqualTo(customer.id.toString())
            .consumeWith(WebTestClientRestDocumentation.document("sign-up-customer"))
    }

    @Test
    fun `should return customer`(customer: Customer) {
        whenever(customerService.getCustomer(customer.id)).thenReturn(customer.toMono())

        webClient.get()
            .uri("/api/v1/customers/{customerId}", customer.id)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.id").isEqualTo(customer.id.toString())
            .consumeWith(WebTestClientRestDocumentation.document("get-customer"))
    }
}
