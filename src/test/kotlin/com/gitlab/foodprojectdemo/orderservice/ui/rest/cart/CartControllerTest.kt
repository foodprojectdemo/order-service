package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.CartNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.ui.rest.AbstractUITest
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.kotlin.core.publisher.toMono

@WebFluxTest(CartController::class)
private class CartControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var cartService: CartService

    @Test
    fun `should create new cart`(@EmptyCart cart: Cart) {
        whenever(cartService.createCart()).thenReturn(cart.toMono())

        webClient.post()
            .uri("/api/v1/carts/")
            .exchange()
            .expectStatus().isCreated
            .expectHeader().valueMatches(HttpHeaders.LOCATION, ".+/api/v1/carts/${cart.id}")
            .expectBody()
            .jsonPath("$.id").isEqualTo(cart.id.toString())
            .consumeWith(document("create-cart"))
    }

    @Test
    fun `should return existing cart`(cart: Cart) {
        whenever(cartService.getCart(cart.id)).thenReturn(cart.toMono())

        webClient.get()
            .uri("/api/v1/carts/{cartId}", cart.id)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.id").isEqualTo(cart.id.toString())
            .jsonPath("$.subTotal").isEqualTo(cart.subTotal.toString())
            .jsonPath("$.total").isEqualTo(cart.total.toString())
            .jsonPath("$.items[0].dishId").isEqualTo(cart.items.first().dish.id.toString())
            .jsonPath("$.items[0].price").isEqualTo(cart.items.first().dish.price.toString())
            .jsonPath("$.items[0].quantity").isEqualTo(cart.items.first().quantity.toInt())
            .jsonPath("$.items[0].totalAmount").isEqualTo(cart.items.first().totalAmount.toString())
            .consumeWith(document("get-cart"))
    }

    @Test
    fun `should return an error when the cart doesn't exist`(cartId: CartId) {
        whenever(cartService.getCart(cartId)).thenReturn(CartNotFoundException(cartId).toMono())

        webClient.get()
            .uri("/api/v1/carts/{cartId}", cartId)
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .jsonPath("$.message").isEqualTo("Cart with ID=$cartId not found")
    }
}
