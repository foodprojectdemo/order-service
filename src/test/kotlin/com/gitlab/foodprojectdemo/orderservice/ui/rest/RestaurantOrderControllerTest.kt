package com.gitlab.foodprojectdemo.orderservice.ui.rest

import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.ui.rest.order.RestaurantOrdersController
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.kotlin.core.publisher.toFlux

@WebFluxTest(RestaurantOrdersController::class)
private class RestaurantOrderControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var orderService: OrderService

    @Test
    fun `should return restaurant's orders`(order: Order) {
        whenever(orderService.getRestaurantOrders(order.restaurantId)).thenReturn(listOf(order).toFlux())

        webClient.get()
            .uri("/api/v1/restaurant/{restaurantId}/orders", order.restaurantId)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$[0].id").isEqualTo(order.id.toString())
            .jsonPath("$[0].customerId").isEqualTo(order.customerId.toString())
            .jsonPath("$[0].restaurantId").isEqualTo(order.restaurantId.toString())
            .jsonPath("$[0].status").isEqualTo(order.status.toString())
            .jsonPath("$[0].address.street").isEqualTo(order.address.street)
            .jsonPath("$[0].address.house").isEqualTo(order.address.house)
            .jsonPath("$[0].address.flat").isEqualTo(order.address.flat)
            .jsonPath("$[0].contactInformation.name").isEqualTo(order.contactInformation.name)
            .jsonPath("$[0].contactInformation.phone").isEqualTo(order.contactInformation.phone)
            .jsonPath("$[0].contactInformation.email").isEqualTo(order.contactInformation.email)
            .jsonPath("$[0].items[0].dishId").isEqualTo(order.items.first().dishId.toString())
            .jsonPath("$[0].items[0].name").isEqualTo(order.items.first().name)
            .jsonPath("$[0].items[0].price").isEqualTo(order.items.first().price.toString())
            .jsonPath("$[0].items[0].quantity").isEqualTo(order.items.first().quantity.toString())
            .jsonPath("$[0].subTotal").isEqualTo(order.subTotal.toString())
            .jsonPath("$[0].discount").isEqualTo(order.discount.toString())
            .jsonPath("$[0].total").isEqualTo(order.total.toString())
            .consumeWith(document("get-restaurant-orders"))

        verify(orderService).getRestaurantOrders(order.restaurantId)
    }
}
