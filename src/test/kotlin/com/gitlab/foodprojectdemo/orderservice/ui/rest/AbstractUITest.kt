package com.gitlab.foodprojectdemo.orderservice.ui.rest

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.OrderServiceApplication
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.WebClientConfig
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.client.ExchangeStrategies

@ExtendWith(RestDocumentationExtension::class)
@ContextConfiguration(classes = [OrderServiceApplication::class, WebClientConfig::class])
abstract class AbstractUITest : AbstractTest() {

    protected lateinit var webClient: WebTestClient

    @Autowired
    private lateinit var exchangeStrategies: ExchangeStrategies

    @BeforeEach
    private fun setUp(
        applicationContext: ApplicationContext,
        restDocumentation: RestDocumentationContextProvider
    ) {
        webClient = WebTestClient.bindToApplicationContext(applicationContext)
            .configureClient()
            .exchangeStrategies(exchangeStrategies)
            .filter(
                documentationConfiguration(restDocumentation)
                    .operationPreprocessors()
                    .withRequestDefaults(prettyPrint())
                    .withResponseDefaults(prettyPrint())
            )
            .build()
    }
}
