package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.WithPreOrder
import com.gitlab.foodprojectdemo.orderservice.ui.rest.AbstractUITest
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.kotlin.core.publisher.toMono

@WebFluxTest(PreOrderController::class)
private class PreOrderControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var cartService: CartService

    @Test
    fun `should return preorder`(@WithPreOrder cart: Cart) {
        whenever(cartService.getPreOrder(cart.id)).thenReturn(cart.preOrder.toMono())

        webClient.get()
            .uri("/api/v1/carts/{cartId}/pre-order", cart.id)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.subTotal").isEqualTo(cart.subTotal.toString())
            .jsonPath("$.total").isEqualTo(cart.total.toString())
            .jsonPath("$.items[0].dishId").isEqualTo(cart.items.first().dish.id.toString())
            .jsonPath("$.items[0].price").isEqualTo(cart.items.first().dish.price.toString())
            .jsonPath("$.items[0].quantity").isEqualTo(cart.items.first().quantity.toInt())
            .jsonPath("$.items[0].totalAmount").isEqualTo(cart.items.first().totalAmount.toString())
            .consumeWith(document("get-pre-order"))
    }

    @Test
    fun `should place preorder`(@WithPreOrder cart: Cart) {
        whenever(cartService.placePreOrder(cart.id)).thenReturn(cart.preOrder.toMono())

        webClient.post()
            .uri("/api/v1/carts/{cartId}/pre-order", cart.id)
            .exchange()
            .expectStatus().isCreated
            .expectHeader().valueMatches(HttpHeaders.LOCATION, ".+/api/v1/carts/${cart.id}/pre-order")
            .expectBody()
            .consumeWith(document("place-pre-order"))
    }
}
