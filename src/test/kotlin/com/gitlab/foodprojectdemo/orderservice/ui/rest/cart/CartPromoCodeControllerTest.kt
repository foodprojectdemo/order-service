package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.ui.rest.AbstractUITest
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.kotlin.core.publisher.toMono

@WebFluxTest(CartPromoCodeController::class)
private class CartPromoCodeControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var cartService: CartService

    private val cartPromoCodeUri = "/api/v1/carts/{cartId}/promo-code"

    @Test
    fun `should apply promo code`(cart: Cart, promoCodeId: PromoCodeId) {
        val request = ApplyPromoCodeRequest(promoCodeId)
        val command = request.toCommand(cart.id)

        whenever(cartService.applyPromoCode(command)).thenReturn(cart.toMono())

        webClient.put()
            .uri(cartPromoCodeUri, cart.id)
            .bodyValue(request)
            .exchange()
            .expectStatus().isNoContent
            .expectBody()
            .consumeWith(document("apply-promo-code"))

        verify(cartService).applyPromoCode(command)
    }

    @Test
    fun `should remove promo code`(cart: Cart) {
        whenever(cartService.removePromoCode(cart.id)).thenReturn(cart.toMono())

        webClient.delete()
            .uri(cartPromoCodeUri, cart.id)
            .exchange()
            .expectStatus().isNoContent
            .expectBody()
            .consumeWith(document("remove-promo-code"))

        verify(cartService).removePromoCode(cart.id)
    }
}
