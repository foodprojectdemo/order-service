package com.gitlab.foodprojectdemo.orderservice.ui.rest

import com.gitlab.foodprojectdemo.orderservice.application.order.CheckoutCommand
import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.ui.rest.order.OrderController
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

private data class CheckoutRequest(
    val customerId: CustomerId,
    val cartId: CartId,
    val shippingAddressStreet: String,
    val shippingAddressHouse: String,
    val shippingAddressFlat: Int,
    val contactInformationName: String,
    val contactInformationPhone: String,
    val contactInformationEmail: String
) {

    fun toCommand() = CheckoutCommand.valueOf(
        customerId,
        cartId,
        shippingAddressStreet,
        shippingAddressHouse,
        shippingAddressFlat,
        contactInformationName,
        contactInformationPhone,
        contactInformationEmail
    )
}

@WebFluxTest(OrderController::class)
private class OrderControllerTest : AbstractUITest() {

    @MockBean
    private lateinit var orderService: OrderService

    @Test
    fun `should return order`(order: Order) {
        whenever(orderService.getOrder(order.id)).thenReturn(order.toMono())

        webClient.get()
            .uri("/api/v1/orders/{orderId}", order.id)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.id").isEqualTo(order.id.toString())
            .jsonPath("$.customerId").isEqualTo(order.customerId.toString())
            .jsonPath("$.restaurantId").isEqualTo(order.restaurantId.toString())
            .jsonPath("$.status").isEqualTo(order.status.toString())
            .jsonPath("$.address.street").isEqualTo(order.address.street)
            .jsonPath("$.address.house").isEqualTo(order.address.house)
            .jsonPath("$.address.flat").isEqualTo(order.address.flat)
            .jsonPath("$.contactInformation.name").isEqualTo(order.contactInformation.name)
            .jsonPath("$.contactInformation.phone").isEqualTo(order.contactInformation.phone)
            .jsonPath("$.contactInformation.email").isEqualTo(order.contactInformation.email)
            .jsonPath("$.items[0].dishId").isEqualTo(order.items.first().dishId.toString())
            .jsonPath("$.items[0].name").isEqualTo(order.items.first().name)
            .jsonPath("$.items[0].price").isEqualTo(order.items.first().price.toString())
            .jsonPath("$.items[0].quantity").isEqualTo(order.items.first().quantity.toString())
            .jsonPath("$.subTotal").isEqualTo(order.subTotal.toString())
            .jsonPath("$.discount").isEqualTo(order.discount.toString())
            .jsonPath("$.total").isEqualTo(order.total.toString())
            .consumeWith(document("get-order"))
    }

    @Test
    fun `should place order`(cartId: CartId, customerId: CustomerId, order: Order) {
        val request = CheckoutRequest(
            customerId,
            cartId,
            order.address.street,
            order.address.house,
            order.address.flat,
            order.contactInformation.name,
            order.contactInformation.phone,
            order.contactInformation.email
        )

        whenever(orderService.checkout(request.toCommand())).thenReturn(order.toMono())

        webClient.post()
            .uri("/api/v1/orders")
            .bodyValue(request)
            .exchange()
            .expectStatus().isCreated
            .expectBody()
            .jsonPath("$.id").isEqualTo(order.id.toString())
            .consumeWith(document("place-order"))
    }

    @Test
    fun `should return customer's orders`(order: Order) {
        whenever(orderService.getCustomerOrders(order.customerId)).thenReturn(listOf(order).toFlux())

        webClient.get()
            .uri("/api/v1/orders?customerId=${order.customerId}")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$[0].id").isEqualTo(order.id.toString())
            .jsonPath("$[0].customerId").isEqualTo(order.customerId.toString())
            .jsonPath("$[0].restaurantId").isEqualTo(order.restaurantId.toString())
            .jsonPath("$[0].status").isEqualTo(order.status.toString())
            .jsonPath("$[0].address.street").isEqualTo(order.address.street)
            .jsonPath("$[0].address.house").isEqualTo(order.address.house)
            .jsonPath("$[0].address.flat").isEqualTo(order.address.flat)
            .jsonPath("$[0].contactInformation.name").isEqualTo(order.contactInformation.name)
            .jsonPath("$[0].contactInformation.phone").isEqualTo(order.contactInformation.phone)
            .jsonPath("$[0].contactInformation.email").isEqualTo(order.contactInformation.email)
            .jsonPath("$[0].items[0].dishId").isEqualTo(order.items.first().dishId.toString())
            .jsonPath("$[0].items[0].name").isEqualTo(order.items.first().name)
            .jsonPath("$[0].items[0].price").isEqualTo(order.items.first().price.toString())
            .jsonPath("$[0].items[0].quantity").isEqualTo(order.items.first().quantity.toString())
            .jsonPath("$[0].subTotal").isEqualTo(order.subTotal.toString())
            .jsonPath("$[0].discount").isEqualTo(order.discount.toString())
            .jsonPath("$[0].total").isEqualTo(order.total.toString())
            .consumeWith(document("get-customer-orders"))

        verify(orderService).getCustomerOrders(order.customerId)
    }
}
