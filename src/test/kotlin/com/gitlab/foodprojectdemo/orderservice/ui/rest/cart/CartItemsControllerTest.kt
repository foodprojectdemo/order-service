package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.RemoveCartItemCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.CartItemNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.CartNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.DishNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.AnotherRestaurantDishException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.faker
import com.gitlab.foodprojectdemo.orderservice.ui.rest.AbstractUITest
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@WebFluxTest(CartItemsController::class)
private class CartItemsControllerTest : AbstractUITest() {
    @MockBean
    private lateinit var cartService: CartService

    private val cartItemsUri = "/api/v1/carts/{cartId}/items"
    private val cartItemUri = "/api/v1/carts/{cartId}/items/{index}"

    @Test
    fun `should add a dish to the cart`(cart: Cart) {
        val request = addDishToCartRequest(cart)
        val command = request.toCommand(cart.id)

        whenever(cartService.addDishToCart(command)).thenReturn(cart.toMono())

        webClient.post()
            .uri(cartItemsUri, cart.id)
            .bodyValue(request)
            .exchange()
            .expectStatus().isCreated
            .expectHeader().valueMatches(HttpHeaders.LOCATION, ".+/api/v1/carts/${cart.id}/items/${cart.items.lastIndex}")
            .expectBody()
            .jsonPath("$.id").isEqualTo(cart.items.lastIndex)
            .consumeWith(document("add-dish-to-cart"))

        verify(cartService).addDishToCart(command)
    }

    @Test
    fun `should change cart item quantity`(cart: Cart, newQuantity: Quantity) {
        val itemIndex = faker.number().numberBetween(0, cart.items.lastIndex)
        val request = ChangeCartItemQuantityRequest(newQuantity)
        val command = request.toCommand(cart.id, itemIndex)

        whenever(cartService.changeCartItemQuantity(command)).thenReturn(cart.toMono())

        webClient.patch()
            .uri(cartItemUri, cart.id, itemIndex)
            .bodyValue(request)
            .exchange()
            .expectStatus().isNoContent
            .expectBody()
            .consumeWith(document("change-cart-item-quantity"))

        verify(cartService).changeCartItemQuantity(command)
    }

    @Test
    fun `should remove cart item`(cart: Cart) {
        val itemIndex = faker.number().numberBetween(0, cart.items.lastIndex)
        val command = RemoveCartItemCommand(cart.id, itemIndex)

        whenever(cartService.removeCartItem(command)).thenReturn(Mono.empty())

        webClient.delete()
            .uri(cartItemUri, cart.id, itemIndex)
            .exchange()
            .expectStatus().isNoContent
            .expectBody()
            .consumeWith(document("remove-cart-item"))

        verify(cartService).removeCartItem(command)
    }

    @Test
    fun `should clear cart`(@EmptyCart cart: Cart) {
        whenever(cartService.clearCart(cart.id)).thenReturn(cart.toMono())

        webClient.delete()
            .uri(cartItemsUri, cart.id)
            .exchange()
            .expectStatus().isNoContent
            .expectBody()
            .consumeWith(document("clear-cart"))

        verify(cartService).clearCart(cart.id)
    }

    @Nested
    inner class AddDishToCartErrors {
        @Test
        fun `should return forbidden error when adding a dish from another restaurant`(cart: Cart) {
            val request = addDishToCartRequest(cart)
            val command = request.toCommand(cart.id)

            whenever(cartService.addDishToCart(command)).thenReturn(AnotherRestaurantDishException().toMono())

            webClient.post()
                .uri(cartItemsUri, cart.id)
                .bodyValue(request)
                .exchange()
                .expectStatus().isForbidden
                .expectBody()
                .jsonPath("$.message").isEqualTo("You cannot add dishes from different restaurants into the cart")
                .jsonPath("$.error").isEqualTo("ANOTHER_RESTAURANT_DISH")

            verify(cartService).addDishToCart(command)
        }

        @Test
        fun `should return not found error when cart not found`(cart: Cart) {
            val request = addDishToCartRequest(cart)
            val command = request.toCommand(cart.id)

            whenever(cartService.addDishToCart(command)).thenReturn(CartNotFoundException(cart.id).toMono())

            webClient.post()
                .uri(cartItemsUri, cart.id)
                .bodyValue(request)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .jsonPath("$.message").isEqualTo("Cart with ID=${cart.id} not found")

            verify(cartService).addDishToCart(command)
        }

        @Test
        fun `should return bad request error when dish not found`(cart: Cart) {
            val request = addDishToCartRequest(cart)
            val command = request.toCommand(cart.id)

            whenever(cartService.addDishToCart(command)).thenReturn(DishNotFoundException(command.dishId).toMono())

            webClient.post()
                .uri(cartItemsUri, cart.id)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.message").isEqualTo("Dish with ID=${command.dishId} not found")

            verify(cartService).addDishToCart(command)
        }

        @Test
        fun `should return bad request error when quantity is invalid`(cart: Cart) {
            val quantity = faker.number().numberBetween(Int.MIN_VALUE, 0)
            val request = mapOf("dishId" to cart.items.last().dish.id, "quantity" to quantity)

            webClient.post()
                .uri(cartItemsUri, cart.id)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.message").isEqualTo("Quantity must be greater than 0, but was given $quantity")

            verify(cartService, never()).addDishToCart(any())
        }

        @Test
        fun `should return bad request error when dishId is invalid`(cart: Cart) {
            val dishId = faker.lorem().word()
            val request = mapOf("dishId" to dishId, "quantity" to cart.items.last().quantity)

            webClient.post()
                .uri(cartItemsUri, cart.id)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.message").isEqualTo("Invalid dishId: $dishId")

            verify(cartService, never()).addDishToCart(any())
        }
    }

    @Nested
    inner class ChangeCartItemQuantityError {
        @Test
        fun `should return not found error when cart item not found`(cart: Cart, newQuantity: Quantity) {
            val itemIndex = faker.number().numberBetween(cart.items.size, Int.MAX_VALUE)
            val request = ChangeCartItemQuantityRequest(newQuantity)
            val command = request.toCommand(cart.id, itemIndex)

            whenever(cartService.changeCartItemQuantity(command)).thenReturn(CartItemNotFoundException(itemIndex).toMono())

            webClient.patch()
                .uri(cartItemUri, cart.id, itemIndex)
                .bodyValue(request)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .jsonPath("$.message").isEqualTo("Cart Item with index $itemIndex not found")

            verify(cartService).changeCartItemQuantity(command)
        }

        @Test
        fun `should return bad request error when quantity is invalid`(cart: Cart) {
            val quantity = faker.number().numberBetween(Int.MIN_VALUE, 0)
            val itemIndex = faker.number().numberBetween(0, cart.items.lastIndex)
            val request = mapOf("quantity" to quantity)

            webClient.patch()
                .uri(cartItemUri, cart.id, itemIndex)
                .bodyValue(request)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.message").isEqualTo("Quantity must be greater than 0, but was given $quantity")

            verify(cartService, never()).changeCartItemQuantity(any())
        }
    }

    @Test
    fun `should return not found error when cart item not found`(cart: Cart) {
        val itemIndex = faker.number().numberBetween(cart.items.lastIndex, Int.MAX_VALUE)
        val command = RemoveCartItemCommand(cart.id, itemIndex)

        whenever(cartService.removeCartItem(command)).thenReturn(CartItemNotFoundException(itemIndex).toMono())

        webClient.delete()
            .uri(cartItemUri, cart.id, itemIndex)
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .jsonPath("$.message").isEqualTo("Cart Item with index $itemIndex not found")

        verify(cartService).removeCartItem(command)
    }

    private fun addDishToCartRequest(cart: Cart) =
        AddDishToCartRequest(cart.items.last().dish.id, cart.items.last().quantity)
}
