package com.gitlab.foodprojectdemo.orderservice.application.order

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.ACCEPTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.NEW
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.READY
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.REJECTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderWithStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

private class NewOrderStatusTest : AbstractTest() {

    @Test
    fun `should set accepted status`(@OrderWithStatus(NEW) order: Order) {
        assertThat(NewOrderStatus.ACCEPTED.changeStatus(order).status).isEqualTo(ACCEPTED)
    }

    @Test
    fun `should set rejected status`(@OrderWithStatus(NEW) order: Order) {
        assertThat(NewOrderStatus.REJECTED.changeStatus(order).status).isEqualTo(REJECTED)
    }

    @Test
    fun `should set ready status`(@OrderWithStatus(ACCEPTED) order: Order) {
        assertThat(NewOrderStatus.READY.changeStatus(order).status).isEqualTo(READY)
    }
}
