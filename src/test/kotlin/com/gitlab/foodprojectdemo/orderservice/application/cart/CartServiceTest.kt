package com.gitlab.foodprojectdemo.orderservice.application.cart

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.AddDishToCartCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ApplyPromoCodeCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ChangeCartItemQuantityCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.RemoveCartItemCommand
import com.gitlab.foodprojectdemo.orderservice.domain.WithSpy
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartItem
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrderItem
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoService
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import reactor.kotlin.core.publisher.toMono
import kotlin.random.Random

private class CartServiceTest : AbstractTest() {

    @Mock
    private lateinit var cartRepository: CartRepository

    @Mock
    private lateinit var dishRepository: DishRepository

    @Mock
    private lateinit var cartPromoService: CartPromoService

    @Mock
    private lateinit var promoCodeRepository: PromoCodeRepository

    @InjectMocks
    private lateinit var cartService: CartService

    @Test
    fun `should create an empty cart`() {
        whenever(cartRepository.save(any())).thenAnswer { it.arguments[0].toMono() }

        val actual = cartService.createCart().block()

        assertThat(actual!!.isEmpty).isTrue

        verify(cartRepository).save(any())
    }

    @Test
    fun `should return the cart`(cart: Cart) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartPromoService.applyTo(cart)).thenReturn(cart.toMono())

        val actual = cartService.getCart(cart.id).block()

        assertThat(actual).isEqualTo(cart)
        verify(cartRepository).findById(cart.id)
        verify(cartPromoService).applyTo(cart)
    }

    @Test
    fun `should add a dish to the cart`(@WithSpy @EmptyCart cart: Cart, dish: Dish, quantity: Quantity) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(dishRepository.getDish(dish.id)).thenReturn(dish.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        cartService.addDishToCart(AddDishToCartCommand(cart.id, dish.id, quantity)).block()

        verify(cart).addDish(dish, quantity)
        verify(cartRepository).findById(cart.id)
        verify(dishRepository).getDish(dish.id)
        verify(cartRepository).save(cart)
    }

    @Test
    fun `should change cart item quantity`(@WithSpy cart: Cart, quantity: Quantity) {
        val cartItem = selectRandomCartItem(cart)

        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        cartService.changeCartItemQuantity(
            ChangeCartItemQuantityCommand(
                cart.id,
                cart.items.indexOf(cartItem),
                quantity
            )
        ).block()

        verify(cart).changeQuantity(cartItem, quantity)
        verify(cartRepository).findById(cart.id)
        verify(cartRepository).save(cart)
    }

    @Test
    fun `should remove cart item`(@WithSpy cart: Cart) {
        val cartItem = selectRandomCartItem(cart)

        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        cartService.removeCartItem(RemoveCartItemCommand(cart.id, cart.items.indexOf(cartItem))).block()

        verify(cart).remove(cartItem)
        verify(cartRepository).findById(cart.id)
        verify(cartRepository).save(cart)
    }

    @Test
    fun `should apply promo code`(cart: Cart, promoCode: PromoCode) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(
            promoCodeRepository.getRestaurantPromoCode(
                promoCode.id,
                cart.restaurantId
            )
        ).thenReturn(promoCode.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        val actual = cartService.applyPromoCode(ApplyPromoCodeCommand(cart.id, promoCode.id)).block()

        assertThat(actual!!.promoCodeId).isEqualTo(promoCode.id)
    }

    @Test
    fun `should remove promo code`(@WithSpy cart: Cart) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        cartService.removePromoCode(cart.id).block()

        verify(cart).removePromoCode()
        verify(cartRepository).findById(cart.id)
        verify(cartRepository).save(cart)
    }

    @Test
    fun `should clear cart`(@WithSpy cart: Cart) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartRepository.save(cart)).thenAnswer { it.arguments[0].toMono() }

        cartService.clearCart(cart.id).block()

        verify(cart).clear()
        verify(cartRepository).findById(cart.id)
        verify(cartRepository).save(cart)
    }

    @Test
    fun `should place pre order`(cart: Cart) {
        whenever(cartRepository.findById(cart.id)).thenReturn(cart.toMono())
        whenever(cartPromoService.applyTo(any())).thenAnswer { it.arguments[0].toMono() }
        whenever(cartRepository.save(any())).thenAnswer { it.arguments[0].toMono() }

        val actual = cartService.placePreOrder(cart.id).block()

        assertThat(actual!!.restaurantId).isEqualTo(cart.restaurantId)
        assertThat(actual.items).containsAll(cart.items.toPreOrderItems() + cart.gifts.toPreOrderItems(true))
        assertThat(actual.discount).isEqualTo(cart.discount)
        assertThat(actual.subTotal).isEqualTo(cart.subTotal)
        assertThat(actual.total).isEqualTo(cart.total)
    }

    private fun Collection<CartItem>.toPreOrderItems(isFree: Boolean = false) =
        map {
            PreOrderItem(
                it.dish.id,
                it.dish.name,
                if (isFree) Money.zero else it.dish.price,
                it.quantity
            )
        }

    private fun selectRandomCartItem(cart: Cart) = cart.items[Random.nextInt(0, cart.items.size)]
}
