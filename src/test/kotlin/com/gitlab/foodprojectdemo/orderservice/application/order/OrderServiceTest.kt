package com.gitlab.foodprojectdemo.orderservice.application.order

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.order.NewOrderStatus.ACCEPTED
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrderItem
import com.gitlab.foodprojectdemo.orderservice.domain.cart.WithPreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderItem
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderRepository
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

private class OrderServiceTest : AbstractTest() {
    @Mock
    private lateinit var cartService: CartService

    @Mock
    private lateinit var orderRepository: OrderRepository

    @InjectMocks
    private lateinit var orderService: OrderService

    @Test
    fun `should return order`(order: Order) {
        whenever(orderRepository.findById(order.id)).thenReturn(order.toMono())

        val actual = orderService.getOrder(order.id).block()

        assertThat(actual).isEqualTo(order)
    }

    @Test
    fun `should return customer's orders`(customerId: CustomerId, order: Order) {
        whenever(orderRepository.findByCustomerId(customerId)).thenReturn(listOf(order).toFlux())

        val actual = orderService.getCustomerOrders(customerId).collectList().block()

        assertThat(actual).contains(order)
    }

    @Test
    fun `should return restaurant's orders`(restaurantId: RestaurantId, order: Order) {
        whenever(orderRepository.findByRestaurantId(restaurantId)).thenReturn(listOf(order).toFlux())

        val actual = orderService.getRestaurantOrders(restaurantId).collectList().block()

        assertThat(actual).contains(order)
    }

    @Test
    fun `should set accepted status`(order: Order) {
        whenever(orderRepository.findById(order.id)).thenReturn(order.toMono())
        whenever(orderRepository.save(any())).thenAnswer {
            it.arguments[0].toMono()
        }

        val actual = orderService.changeStatus(order.id, ACCEPTED).block()

        assertThat(actual!!.status).isEqualTo(OrderStatus.ACCEPTED)
        verify(orderRepository).save(order)
    }

    @Test
    fun `should checkout`(@WithPreOrder cart: Cart, @EmptyCart emptyCart: Cart, customerId: CustomerId, order: Order) {
        val preOrder = cart.preOrder
        whenever(cartService.getPreOrder(cart.id)).thenReturn(preOrder.toMono())

        whenever(cartService.clearCart(cart.id)).thenReturn(emptyCart.toMono())
        whenever(orderRepository.save(any())).thenAnswer {
            it.arguments[0].toMono()
        }

        val checkoutCommand = CheckoutCommand.valueOf(
            customerId,
            cart.id,
            order.address.street,
            order.address.house,
            order.address.flat,
            order.contactInformation.name,
            order.contactInformation.phone,
            order.contactInformation.email
        )

        val actual = orderService.checkout(checkoutCommand).block()

        assertThat(actual!!.items).isEqualTo(preOrder!!.items.toOrderItems())
        assertThat(actual.address).isEqualTo(order.address)
        assertThat(actual.contactInformation).isEqualTo(order.contactInformation)
    }

    private fun Collection<PreOrderItem>.toOrderItems() =
        map { preOrderItem ->
            OrderItem(
                preOrderItem.dishId,
                preOrderItem.name,
                preOrderItem.price,
                preOrderItem.quantity
            )
        }
}
