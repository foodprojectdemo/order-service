package com.gitlab.foodprojectdemo.orderservice.application.customer

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import reactor.kotlin.core.publisher.toMono

private class CustomerServiceTest : AbstractTest() {

    @Mock
    private lateinit var customerRepository: CustomerRepository

    @InjectMocks
    private lateinit var customerService: CustomerService

    @Test
    fun `should sign up a customer`() {
        whenever(customerRepository.save(any())).thenAnswer { it.arguments[0].toMono() }

        val actual = customerService.signUpCustomer().block()

        assertThat(actual).isNotNull
    }

    @Test
    fun `should return the customer`(customer: Customer) {
        whenever(customerRepository.findById(customer.id)).thenReturn(customer.toMono())

        val actual = customerService.getCustomer(customer.id).block()

        assertThat(actual).isEqualTo(customer)
    }
}
