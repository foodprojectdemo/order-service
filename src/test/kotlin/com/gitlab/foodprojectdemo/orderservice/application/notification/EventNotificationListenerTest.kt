package com.gitlab.foodprojectdemo.orderservice.application.notification

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.event.OrderPlacedEvent
import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService.EmailNotification
import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.SystemTime
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.nhaarman.mockito_kotlin.argThat
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.InjectMocks
import org.mockito.Mock
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

private class EventNotificationListenerTest : AbstractTest() {

    @Mock
    private lateinit var orderService: OrderService

    @Mock
    private lateinit var notificationService: NotificationService

    @InjectMocks
    private lateinit var eventNotificationListener: EventNotificationListener

    @Captor
    private lateinit var emailNotificationCaptor: ArgumentCaptor<EmailNotification>

    @Test
    fun `should send notification`(order: Order) {
        val orderPlacedEvent = OrderPlacedEvent(DomainEventId(), order.name, order.id, SystemTime.now())

        whenever(orderService.getOrder(order.id)).thenReturn(order.toMono())

        whenever(
            notificationService.sendEmailNotification(
                argThat {
                    subject == "New order ${order.id}" &&
                        text == "Hi ${order.contactInformation.name}! New order was placed" &&
                        toEmail == order.contactInformation.email
                }
            )
        ).thenReturn(Mono.empty<Void>().then())

        eventNotificationListener.onEventSendNotification(orderPlacedEvent)
    }
}
