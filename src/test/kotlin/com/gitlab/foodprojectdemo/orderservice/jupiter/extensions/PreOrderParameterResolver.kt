package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.PreOrderFixture.preOrder
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class PreOrderParameterResolver : TypeBasedParameterResolver<PreOrder>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): PreOrder {
        return preOrder()
    }
}
