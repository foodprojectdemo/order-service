package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.fixture.CustomerFixture.customer
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class CustomerParameterResolver : TypeBasedParameterResolver<Customer>() {
    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Customer {
        return customer()
    }
}
