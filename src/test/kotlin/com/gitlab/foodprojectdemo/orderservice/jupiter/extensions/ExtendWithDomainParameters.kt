package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import org.junit.jupiter.api.extension.ExtendWith
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.CLASS

@Retention(RUNTIME)
@Target(CLASS)
@ExtendWith(
    value = [
        CartParameterResolver::class,
        CartIdParameterResolver::class,
        CustomerIdParameterResolver::class,
        CustomerParameterResolver::class,
        DishIdParameterResolver::class,
        DishParameterResolver::class,
        MoneyParameterResolver::class,
        OrderParameterResolver::class,
        PercentParameterResolver::class,
        PromoCodeIdParameterResolver::class,
        PromoCodeParameterResolver::class,
        QuantityParameterResolver::class,
        RestaurantIdParameterResolver::class,
        AddressParameterResolver::class,
        ContactInformationParameterResolver::class,
        PreOrderParameterResolver::class
    ]
)
annotation class ExtendWithDomainParameters
