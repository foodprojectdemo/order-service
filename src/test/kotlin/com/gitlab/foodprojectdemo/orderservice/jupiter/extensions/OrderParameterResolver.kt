package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.ACCEPTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.NEW
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.READY
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.REJECTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderWithStatus
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.acceptedOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.newOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.readyOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.rejectedOrder
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class OrderParameterResolver : TypeBasedParameterResolver<Order>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Order {
        if (parameterContext.isAnnotated(OrderWithStatus::class.java)) {
            val annotation = parameterContext.findAnnotation(OrderWithStatus::class.java).get()
            return when (annotation.value) {
                NEW -> newOrder()
                ACCEPTED -> acceptedOrder()
                REJECTED -> rejectedOrder()
                READY -> readyOrder()
            }
        }
        return newOrder()
    }
}
