package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryDishRepository
import com.gitlab.foodprojectdemo.orderservice.domain.Saved
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver
import org.springframework.test.context.junit.jupiter.SpringExtension

class DishParameterResolver : ParameterResolver {
    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Boolean {
        return parameterContext.parameter.type == Dish::class.java
    }

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any {

        val dish = dish()

        if (parameterContext.isAnnotated(Saved::class.java)) {
            val applicationContext = SpringExtension.getApplicationContext(extensionContext)
            val dishRepository = applicationContext.getBean(InMemoryDishRepository::class.java)

            dishRepository.save(dish)
        }

        return dish
    }
}
