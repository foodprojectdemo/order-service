package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscount
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class AbsoluteDiscountParameterResolver : TypeBasedParameterResolver<AbsoluteDiscount>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): AbsoluteDiscount {
        return Money.random().toBenefit()
    }
}
