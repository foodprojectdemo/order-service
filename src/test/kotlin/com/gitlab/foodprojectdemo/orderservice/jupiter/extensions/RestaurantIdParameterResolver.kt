package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class RestaurantIdParameterResolver : TypeBasedParameterResolver<RestaurantId>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): RestaurantId {
        return RestaurantFixture.restaurantId()
    }
}
