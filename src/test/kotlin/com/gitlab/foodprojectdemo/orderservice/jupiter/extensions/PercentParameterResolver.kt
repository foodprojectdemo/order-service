package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class PercentParameterResolver : TypeBasedParameterResolver<Percent>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Percent {
        return Percent.random()
    }
}
