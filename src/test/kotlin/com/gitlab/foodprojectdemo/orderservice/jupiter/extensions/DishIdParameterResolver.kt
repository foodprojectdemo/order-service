package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class DishIdParameterResolver : TypeBasedParameterResolver<DishId>() {
    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): DishId {
        return DishFixture.dishId()
    }
}
