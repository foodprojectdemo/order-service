package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension

class RestaurantStubExtension : BeforeAllCallback {

    override fun beforeAll(extensionContext: ExtensionContext) {
        val applicationContext = SpringExtension.getApplicationContext(extensionContext)
        val wireMockServer = applicationContext.getBean(WireMockServer::class.java)

        val dish = dish()

        wireMockServer.stubFor(
            get(urlPathMatching("/api/v1/dishes/([0-9]+)"))
                .willReturn(
                    aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(
                            """
                            {
                                "id": {{request.pathSegments.[3]}},
                                "restaurantId": ${dish.restaurantId.toString().toLong()},
                                "name": "${dish.name}",
                                "amount": ${dish.price.toInt()}
                            }
                            """
                        )
                        .withTransformers("response-template")
                )
        )

        wireMockServer.stubFor(
            get(urlPathMatching("/api/v1/restaurants/([0-9]+)/promo-actions"))
                .willReturn(
                    aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody("[]")
                        .withTransformers("response-template")
                )
        )
    }
}
