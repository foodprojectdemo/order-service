package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class QuantityParameterResolver : TypeBasedParameterResolver<Quantity>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Quantity {
        return Quantity.random()
    }
}
