package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.Saved
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartId
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartWithItems
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.emptyCart
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver
import org.springframework.test.context.junit.jupiter.SpringExtension

class CartIdParameterResolver : TypeBasedParameterResolver<CartId>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): CartId {
        return if (parameterContext.isAnnotated(Saved::class.java)) {
            val cart = if (parameterContext.isAnnotated(EmptyCart::class.java)) emptyCart() else cartWithItems()

            val applicationContext = SpringExtension.getApplicationContext(extensionContext)
            val cartRepository = applicationContext.getBean(CartRepository::class.java)

            cartRepository.save(cart)

            cart.id
        } else {
            cartId()
        }
    }
}
