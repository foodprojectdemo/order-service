package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.Saved
import com.gitlab.foodprojectdemo.orderservice.domain.WithSpy
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.WithPreOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartWithItems
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.emptyCart
import com.nhaarman.mockito_kotlin.spy
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver
import org.springframework.test.context.junit.jupiter.SpringExtension

class CartParameterResolver : TypeBasedParameterResolver<Cart>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Cart {
        var cart = if (parameterContext.isAnnotated(EmptyCart::class.java)) emptyCart() else cartWithItems()

        savedCart(parameterContext, extensionContext, cart)
        cart = cartWithPreOrder(parameterContext, cart)
        cart = cartWithSpy(parameterContext, cart)

        return cart
    }

    private fun savedCart(
        parameterContext: ParameterContext,
        extensionContext: ExtensionContext,
        cart: Cart
    ) {
        if (parameterContext.isAnnotated(Saved::class.java)) {
            val applicationContext = SpringExtension.getApplicationContext(extensionContext)
            val cartRepository = applicationContext.getBean(CartRepository::class.java)

            cartRepository.save(cart)
        }
    }

    private fun cartWithPreOrder(parameterContext: ParameterContext, cart: Cart) = cart.apply {
        if (parameterContext.isAnnotated(WithPreOrder::class.java)) {
            placePreOrder()
        }
    }

    private fun cartWithSpy(parameterContext: ParameterContext, cart: Cart): Cart {
        if (parameterContext.isAnnotated(WithSpy::class.java)) {
            return spy(cart)
        }
        return cart
    }
}
