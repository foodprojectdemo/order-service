package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoCodeFixture
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class PromoCodeParameterResolver : TypeBasedParameterResolver<PromoCode>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): PromoCode {
        return PromoCodeFixture.promoCode()
    }
}
