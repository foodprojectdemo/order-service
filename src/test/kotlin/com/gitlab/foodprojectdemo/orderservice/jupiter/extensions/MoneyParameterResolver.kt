package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class MoneyParameterResolver : TypeBasedParameterResolver<Money>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Money {
        return Money.random()
    }
}
