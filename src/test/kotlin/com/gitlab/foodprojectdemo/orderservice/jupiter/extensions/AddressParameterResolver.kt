package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.order.Address
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class AddressParameterResolver : TypeBasedParameterResolver<Address>() {

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Address {
        return OrderFixture.address()
    }
}
