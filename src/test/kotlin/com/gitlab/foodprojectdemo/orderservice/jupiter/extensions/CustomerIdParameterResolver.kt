package com.gitlab.foodprojectdemo.orderservice.jupiter.extensions

import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.fixture.CustomerFixture
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver

class CustomerIdParameterResolver : TypeBasedParameterResolver<CustomerId>() {
    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): CustomerId {
        return CustomerFixture.customerId()
    }
}
