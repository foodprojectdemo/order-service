package com.gitlab.foodprojectdemo.orderservice.system

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.order.Address
import com.gitlab.foodprojectdemo.orderservice.domain.order.ContactInformation
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.OrderServiceApplication
import com.gitlab.foodprojectdemo.orderservice.jupiter.extensions.RestaurantStubExtension
import com.gitlab.foodprojectdemo.orderservice.logger
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.UUID

private data class IdDto(val id: String)

private data class CartDto(
    val id: String,
    val items: List<CartItemDto>,
    val gifts: List<CartItemDto>,
    val total: String,
    val subTotal: String,
    val discount: String,
    val promoCodeId: String?
)

private data class CartItemDto(
    val dishId: String,
    val price: String,
    val totalAmount: String,
    val quantity: Int
)

private data class PreOrderDto(
    val restaurantId: String,
    val items: List<CartItemDto>,
    val total: String,
    val subTotal: String,
    val discount: String
)

@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(RestaurantStubExtension::class)
@AutoConfigureWireMock(port = 0)
@ContextConfiguration(classes = [OrderServiceApplication::class])
@TestPropertySource(
    properties = [
        "restaurant-service.url=http://localhost:\${wiremock.server.port}/",
        "logging.level.com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway=off"
    ]
)
private class SystemIT : AbstractTest() {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @LocalServerPort
    private var port: Int = 0

    @Autowired
    private lateinit var builder: WebClient.Builder

    private lateinit var webClient: WebClient

    @BeforeEach
    fun setUp() {
        webClient = builder.baseUrl("http://localhost:$port").build()
    }

    @Test
    fun `should create empty cart`() {
        val cartId = createCartRequest()
        val cart = getCartRequest(cartId)

        assertThat(cart.items).isEmpty()
    }

    @Test
    fun `should add a dish with the quantity to the cart`(dishId: DishId, quantity: Quantity) {

        val cartId = createCartRequest()

        addDishRequest(cartId, dishId, quantity)

        val cart = getCartRequest(cartId)

        assertThat(cart.items).anyMatch { it.dishId == dishId.toString() && it.quantity == quantity.toInt() }
    }

    @Test
    fun `should place pre order`(dishId: DishId, quantity: Quantity) {
        val cartId = createCartRequest()
        addDishRequest(cartId, dishId, quantity)
        placePreOrderRequest(cartId)

        val preOrder = getPreOrderRequest(cartId)

        assertThat(preOrder.items).anyMatch { it.dishId == dishId.toString() && it.quantity == quantity.toInt() }
    }

    @Test
    fun `should create a new customer`() {
        val customerId = signUpCustomerRequest()
        val customer = getCustomerRequest(customerId)
        assertThat(customer).isNotNull
        logger(this).info("{}", getCustomerRequest(customerId))
    }

    @Test
    fun `should create a new order`(
        dishId: DishId,
        quantity: Quantity,
        address: Address,
        contactInformation: ContactInformation
    ) {
        val cartId = createCartRequest()
        addDishRequest(cartId, dishId, quantity)
        placePreOrderRequest(cartId)
        val customerId = signUpCustomerRequest()

        val checkoutRequestDto = CheckoutRequestDto(
            customerId,
            cartId,
            address.street,
            address.house,
            address.flat,
            contactInformation.name,
            contactInformation.phone,
            contactInformation.email
        )

        val orderId = checkoutOrderRequest(checkoutRequestDto)

        log.info("orderId {}", orderId)
    }

    private fun createCartRequest() =
        webClient.post().uri("/api/v1/carts/").retrieve()
            .bodyToMono(IdDto::class.java).map {
                CartId(UUID.fromString(it.id))
            }.block()!!

    private fun getCartRequest(cartId: CartId) = webClient.get().uri("/api/v1/carts/{cartId}", cartId).retrieve()
        .bodyToMono(CartDto::class.java).block()!!

    private fun addDishRequest(cartId: CartId, dishId: DishId, quantity: Quantity = Quantity.one) =
        webClient.post().uri("/api/v1/carts/{cartId}/items", cartId)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapOf("dishId" to dishId, "quantity" to quantity))
            .retrieve()
            .toBodilessEntity()
            .flatMap { response ->
                if (response.statusCode != HttpStatus.CREATED)
                    RuntimeException("Not CREATED").toMono()
                else
                    Mono.empty<Void>()
            }
            .block()

    private fun placePreOrderRequest(cartId: CartId) = webClient.post().uri("/api/v1/carts/{cartId}/pre-order", cartId)
        .contentType(MediaType.APPLICATION_JSON)
        .retrieve()
        .toBodilessEntity()
        .flatMap { response ->
            if (response.statusCode != HttpStatus.CREATED)
                RuntimeException("Not CREATED").toMono()
            else
                Mono.empty<Void>()
        }
        .block()

    private fun getPreOrderRequest(cartId: CartId) =
        webClient.get().uri("/api/v1/carts/{cartId}/pre-order", cartId).retrieve()
            .bodyToMono(PreOrderDto::class.java).block()!!

    private fun signUpCustomerRequest() = webClient.post().uri("/api/v1/customers")
        .retrieve()
        .bodyToMono(IdDto::class.java).map {
            CustomerId(UUID.fromString(it.id))
        }.block()!!

    private fun getCustomerRequest(customerId: CustomerId) =
        webClient.get().uri("/api/v1/customers/{customerId}", customerId).retrieve()
            .bodyToMono(Customer::class.java).block()!!

    private fun checkoutOrderRequest(request: CheckoutRequestDto) =
        webClient.post().uri("/api/v1/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(request)
            .retrieve()
            .bodyToMono(IdDto::class.java).map {
                OrderId(UUID.fromString(it.id))
            }.block()!!
}

private data class CheckoutRequestDto(
    val customerId: CustomerId,
    val cartId: CartId,
    val shippingAddressStreet: String,
    val shippingAddressHouse: String,
    val shippingAddressFlat: Int,
    val contactInformationName: String,
    val contactInformationPhone: String,
    val contactInformationEmail: String
)
