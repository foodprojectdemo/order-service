package com.gitlab.foodprojectdemo.orderservice.json

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.exception.BusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.faker
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.JacksonConfig
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.JsonTest
import org.springframework.boot.test.json.JacksonTester
import org.springframework.test.context.ContextConfiguration

@JsonTest
@ContextConfiguration(classes = [JacksonConfig::class])
class QuantityJsonTest : AbstractTest() {
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private lateinit var tester: JacksonTester<Quantity>

    @Test
    fun serialize(quantity: Quantity) {
        val json = tester.write(quantity)
        assertThat(json).isEqualToJson(quantity.toString())
    }

    @Test
    fun deserialize(quantity: Quantity) {
        val actual = tester.parse(quantity.toString()).getObject()
        assertThat(actual).isEqualTo(quantity)
    }

    @Test
    fun deserialize_invalid() {
        val quantity = faker.number().numberBetween(Int.MIN_VALUE, 0)
        assertThatThrownBy { tester.parse(quantity.toString()).getObject() }
            .isInstanceOf(BusinessLogicException::class.java)
            .hasMessage("Quantity must be greater than 0, but was given $quantity")
    }
}
