package com.gitlab.foodprojectdemo.orderservice.json

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderPlacedEvent
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.order
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.JacksonConfig
import com.gitlab.foodprojectdemo.orderservice.logger
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.JsonTest
import org.springframework.boot.test.json.JacksonTester
import org.springframework.test.context.ContextConfiguration

@JsonTest
@ContextConfiguration(classes = [JacksonConfig::class])
class DomainEventJsonTest : AbstractTest() {
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private lateinit var tester: JacksonTester<OrderPlacedEvent>

    @Test
    fun serialize() {
        val order = order()
        val event = OrderPlacedEvent(order)

        val json = tester.write(event)
        logger(this).info(json.json)
//        assertThat(json).isEqualToJson(quantity.toString())
    }
}
