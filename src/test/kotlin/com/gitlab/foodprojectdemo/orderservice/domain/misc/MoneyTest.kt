package com.gitlab.foodprojectdemo.orderservice.domain.misc

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money.Companion.zero
import com.gitlab.foodprojectdemo.orderservice.faker
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import java.util.Currency

private class MoneyTest : AbstractTest() {

    companion object {
        private const val MUST_BE_GREATER_THAN_ZERO = "Money must be greater than 0"
    }

    private fun randomAmount() = faker.number().numberBetween(1L, 1000L)
    private fun randomQuantity() = faker.number().numberBetween(1, 100)

    @Test
    fun getCurrency() {
        val money = Money(randomAmount())
        assertThat(money.currency).isEqualTo(Currency.getInstance("USD"))
    }

    @Test
    fun plus() {
        val amount1 = randomAmount()
        val amount2 = randomAmount()
        assertThat(Money(amount1) + Money(amount2)).isEqualTo(Money(amount1 + amount2))
    }

    @Test
    fun plus_zero() {
        val amount1 = randomAmount()
        assertThat(Money(amount1) + zero).isEqualTo(Money(amount1))
    }

    @Test
    fun plus_overflow_InvalidMoneyAmountException() {
        val amount1 = faker.number().numberBetween(Long.MAX_VALUE / 2 + 1, Long.MAX_VALUE)
        val amount2 = faker.number().numberBetween(Long.MAX_VALUE / 2 + 1, Long.MAX_VALUE)

        assertThatExceptionOfType(InvalidMoneyAmountException::class.java)
            .isThrownBy { Money(amount1) + Money(amount2) }
            .withMessage(MUST_BE_GREATER_THAN_ZERO)
    }

    @Test
    fun minus() {
        val amount1 = randomAmount()
        val amount2 = faker.number().numberBetween(1L, amount1)
        assertThat(Money(amount1) - Money(amount2)).isEqualTo(Money(amount1 - amount2))
    }

    @Test
    fun minus_ExpectZero() {
        val amount = randomAmount()
        assertThat(Money(amount) - Money(amount)).isEqualTo(zero)
    }

    @Test
    fun minus_InvalidMoneyAmountException() {
        val amount1 = randomAmount()
        val amount2 = faker.number().numberBetween(1L, amount1)

        assertThatExceptionOfType(InvalidMoneyAmountException::class.java)
            .isThrownBy { Money(amount2) - Money(amount1) }
            .withMessage(MUST_BE_GREATER_THAN_ZERO)
    }

    @Test
    fun compareTo() {
    }

    @Test
    fun times() {
    }

    @Test
    fun testTimes() {
    }

    @Test
    fun testTimes1() {
    }

    @Test
    fun timesPercent() {
//        percent
    }

    @Test
    fun div() {
        val amount1 = randomAmount()
        val amount2 = randomAmount()
        assertThat(Money(amount1) / Money(amount2)).isEqualTo(amount1.toDouble() / amount2.toDouble())
    }

    @Test
    fun divByQuantity() {
        val amount = randomAmount()
        val quantity = faker.number().numberBetween(1, amount.toInt())
        assertThat(Money(amount) / Quantity(quantity)).isEqualTo(Money(amount / quantity))
    }

    @Test
    fun divByDouble() {
        val amount = randomAmount()
        val divider = faker.number().randomDouble(2, 1L, amount)
        assertThat(Money(amount) / divider).isEqualTo(Money((amount.toDouble() / divider).toLong()))
    }

    @Test
    fun testDivDoubleDivider_InvalidMoneyAmountException() {
        val amount = randomAmount()
        val divider = faker.number().randomDouble(2, amount, Long.MAX_VALUE)

        assertThatExceptionOfType(InvalidMoneyAmountException::class.java)
            .isThrownBy { Money(amount) / divider }
            .withMessage(MUST_BE_GREATER_THAN_ZERO)
    }

    @Test
    fun rem() {
        val amount = randomAmount()
        val quantity = randomQuantity()
        assertThat(Money(amount) % Quantity(quantity)).isEqualTo(amount % quantity)
    }

    @Test
    fun testToString() {
        val amount = randomAmount()
        val money = Money(amount)
        assertThat(money.toString()).isEqualTo("$$amount")
    }

    @Test
    fun toLong() {
        val amount = randomAmount()
        val money = Money(amount)
        assertThat(money.toLong()).isEqualTo(amount)
    }
}
