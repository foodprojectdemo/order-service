package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

private class CartContainsDishWithMinQuantityConditionTest : AbstractTest() {

    @Test
    fun `should satisfy`(cart: Cart) {
        val condition = CartContainsDishWithMinQuantityCondition(
            Money.random().toBenefit(), cart.items[0].dish,
            Quantity.one
        )

        assertThat(cart satisfies condition).isTrue
    }

    @Test
    fun `should not satisfy`(cart: Cart, dish: Dish) {
        val condition = CartContainsDishWithMinQuantityCondition(Money.random().toBenefit(), dish, Quantity.one)

        assertThat(cart satisfies condition).isFalse
    }
}
