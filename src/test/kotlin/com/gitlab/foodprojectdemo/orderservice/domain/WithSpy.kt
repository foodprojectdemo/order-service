package com.gitlab.foodprojectdemo.orderservice.domain

import kotlin.annotation.AnnotationRetention.RUNTIME

@Retention(RUNTIME)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class WithSpy
