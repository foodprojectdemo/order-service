package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.fixture.BenefitFixture.benefit
import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
import com.gitlab.foodprojectdemo.orderservice.fixture.randomMoreThanThis
import com.gitlab.foodprojectdemo.orderservice.jupiter.extensions.CartParameterResolver
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(CartParameterResolver::class)
private class ByCartAmountConditionTest : AbstractTest() {

    @Test
    fun `should satisfy`(cart: Cart) {
        val condition = ByCartAmountCondition(cart.subTotal.randomLessThanThis(), benefit())

        assertThat(cart satisfies condition).isTrue
    }

    @Test
    fun `should not satisfy`(cart: Cart) {
        val condition = ByCartAmountCondition(cart.subTotal.randomMoreThanThis(), benefit())

        assertThat(cart satisfies condition).isFalse
    }
}
