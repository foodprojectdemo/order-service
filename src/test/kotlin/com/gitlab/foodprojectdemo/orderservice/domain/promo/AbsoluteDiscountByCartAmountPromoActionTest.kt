package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.absoluteDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
import com.gitlab.foodprojectdemo.orderservice.fixture.randomMoreThanThis
import com.gitlab.foodprojectdemo.orderservice.jupiter.extensions.AbsoluteDiscountParameterResolver
import com.gitlab.foodprojectdemo.orderservice.jupiter.extensions.CartParameterResolver
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(
    CartParameterResolver::class,
    AbsoluteDiscountParameterResolver::class
)
private class AbsoluteDiscountByCartAmountPromoActionTest : AbstractTest() {

    @Test
    fun `should not return any discount`(cart: Cart, first: AbsoluteDiscount, second: AbsoluteDiscount, third: AbsoluteDiscount) {
        val promoAction = absoluteDiscountByCartAmountPromoAction(
            discountMap = mapOf(
                cart.subTotal.randomMoreThanThis() to first,
                cart.subTotal.randomMoreThanThis() to second,
                cart.subTotal.randomMoreThanThis() to third
            )
        )

        assertThat(promoAction.verify(cart)).isNull()
    }

    @Test
    fun `should return first discount`(cart: Cart, first: AbsoluteDiscount, second: AbsoluteDiscount, third: AbsoluteDiscount) {
        val promoAction = absoluteDiscountByCartAmountPromoAction(
            discountMap = mapOf(
                cart.subTotal to first,
                cart.subTotal.randomMoreThanThis() to second,
                cart.subTotal.randomMoreThanThis() to third
            )
        )

        assertThat(promoAction.verify(cart)).isEqualTo(first)
    }

    @Test
    fun `should return second discount`(cart: Cart, first: AbsoluteDiscount, second: AbsoluteDiscount, third: AbsoluteDiscount) {
        val promoAction = absoluteDiscountByCartAmountPromoAction(
            discountMap = mapOf(
                cart.subTotal.randomLessThanThis() to first,
                cart.subTotal to second,
                cart.subTotal.randomMoreThanThis() to third
            )
        )

        assertThat(promoAction.verify(cart)).isEqualTo(second)
    }

    @Test
    fun `should return third discount`(cart: Cart, first: AbsoluteDiscount, second: AbsoluteDiscount, third: AbsoluteDiscount) {
        val promoAction = absoluteDiscountByCartAmountPromoAction(
            discountMap = mapOf(
                cart.subTotal.randomLessThanThis() to first,
                cart.subTotal.randomLessThanThis() to second,
                cart.subTotal to third
            )
        )

        assertThat(promoAction.verify(cart)).isEqualTo(third)
    }
}
