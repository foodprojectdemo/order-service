package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.AbstractTest

private class PromoActionCalculatorResolverTest : AbstractTest() {
//
//    @Mock
//    private lateinit var dishRepository: DishRepository
//
//    @InjectMocks
//    private lateinit var promoActionCalculatorResolver: PromoActionCalculatorResolver
//
//    @ParameterizedTest
//    @MethodSource("promoActionSource")
//    fun `should resolve promo action`(promoAction: PromoAction, promoActionCalculatorClass: KClass<out PromoActionCalculator>) {
//        val calculator = promoActionCalculatorResolver.resolve(promoAction).block()
//        assertThat(calculator).isInstanceOf(promoActionCalculatorClass.java)
//    }
//
//    @ParameterizedTest
//    @MethodSource("promoCodeSource")
//    fun `should resolve promo code`(promoCode: PromoCode, promoActionCalculatorClass: KClass<out PromoActionCalculator>) {
//        val calculator = promoActionCalculatorResolver.resolve(promoCode).block()
//        assertThat(calculator).isInstanceOf(promoActionCalculatorClass.java)
//    }
//
//    @Test
//    fun `should return empty for unknown promo`() {
//        val unknownPromo = object : PromoAction {
//            override val id = promoActionId()
//            override val restaurantId = restaurantId()
//            override val conditions = "it does not matter"
//        }
//        val calculator = promoActionCalculatorResolver.resolve(unknownPromo).block()
//        assertThat(calculator).isNull()
//    }
//
//    companion object {
//        @JvmStatic
//        private fun promoActionSource(): Stream<Arguments> {
//            return Stream.of(
//                Arguments.of(
//                    absoluteDiscountByCartAmountPromoAction(),
//                    AbsoluteDiscountByCartAmountPromoActionCalculator::class
//                ),
//                Arguments.of(
//                    percentDiscountByCartAmountPromoAction(),
//                    PercentDiscountByCartAmountPromoActionCalculator::class
//                ),
//                Arguments.of(
//                    giftByCartAmountPromoAction(),
//                    GiftByCartAmountPromoActionCalculator::class
//                )
//            )
//        }
//
//        @JvmStatic
//        private fun promoCodeSource(): Stream<Arguments> {
//            return Stream.of(
//                Arguments.of(
//                    absoluteDiscountPromoCode(),
//                    AbsoluteDiscountByCartAmountPromoActionCalculator::class
//                ),
//                Arguments.of(
//                    percentDiscountPromoCode(),
//                    PercentDiscountByCartAmountPromoActionCalculator::class
//                ),
//                Arguments.of(
//                    giftPromoCode(),
//                    GiftByCartAmountPromoActionCalculator::class
//                )
//            )
//        }
//    }
}
