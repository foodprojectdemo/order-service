package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.emptyCart
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.promoActionId
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
import com.gitlab.foodprojectdemo.orderservice.fixture.randomMoreThanThis
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import com.gitlab.foodprojectdemo.orderservice.toMoney
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PromoTest : AbstractTest() {

    @Test
    fun `should satisfy condition`() {
        val cartAmount = Money.random()
        val discount = cartAmount.randomLessThanThis().toBenefit()
        val condition = ByCartAmountCondition(cartAmount, discount)
        val cart = emptyCart()
        val dish = dish(price = cartAmount.randomMoreThanThis())

        cart.addDish(dish)

        assertThat(cart satisfies condition).isTrue
    }

    @Test
    fun `should not return any benefit`() {
        val condition1 = ByCartAmountCondition(1000.toMoney(), 100.toMoney().toBenefit())
        val condition2 = ByCartAmountCondition(2000.toMoney(), 300.toMoney().toBenefit())

        val promo = AbsoluteDiscountByCartAmountPromoAction(promoActionId(), restaurantId(), listOf(condition1, condition2))
        val cart = emptyCart()

        assertThat(promo.verify(cart)).isNull()
    }
}
