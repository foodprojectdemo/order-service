package com.gitlab.foodprojectdemo.orderservice.domain

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan

@TestConfiguration
@ComponentScan
class DomainContextConfiguration
