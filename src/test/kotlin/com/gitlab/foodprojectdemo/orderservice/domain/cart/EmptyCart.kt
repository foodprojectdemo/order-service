package com.gitlab.foodprojectdemo.orderservice.domain.cart

import kotlin.annotation.AnnotationRetention.RUNTIME

@Retention(RUNTIME)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class EmptyCart
