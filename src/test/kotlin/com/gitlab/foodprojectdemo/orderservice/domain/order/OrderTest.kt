package com.gitlab.foodprojectdemo.orderservice.domain.order

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.ACCEPTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.NEW
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.READY
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.REJECTED
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderAcceptedEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderReadyEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderRejectedEvent
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

private class OrderTest : AbstractTest() {

    @Test
    fun `should accept new order`(@OrderWithStatus(NEW) order: Order) {
        order.accept()
        assertThat(order.status).isEqualTo(ACCEPTED)
        assertThat(order.events).anyMatch { event -> event is OrderAcceptedEvent }
    }

    @Test
    fun `should reject new order`(@OrderWithStatus(NEW) order: Order) {
        order.reject()
        assertThat(order.status).isEqualTo(REJECTED)
        assertThat(order.events).anyMatch { event -> event is OrderRejectedEvent }
    }

    @Test
    fun `should be ready accepted order`(@OrderWithStatus(ACCEPTED) order: Order) {
        order.ready()
        assertThat(order.status).isEqualTo(READY)
        assertThat(order.events).anyMatch { event -> event is OrderReadyEvent }
    }

    @Test
    fun `should be idempotent order acceptance`(@OrderWithStatus(ACCEPTED) order: Order) {
        val eventCount = order.events.size
        order.accept()
        assertThat(order.status).isEqualTo(ACCEPTED)
        assertThat(eventCount).isEqualTo(order.events.size)
    }

    @Test
    fun `should be idempotent order rejection`(@OrderWithStatus(REJECTED) order: Order) {
        val eventCount = order.events.size
        order.reject()
        assertThat(order.status).isEqualTo(REJECTED)
        assertThat(eventCount).isEqualTo(order.events.size)
    }

    @Test
    fun `should be idempotent order making`(@OrderWithStatus(READY) order: Order) {
        val eventCount = order.events.size
        order.ready()
        assertThat(order.status).isEqualTo(READY)
        assertThat(eventCount).isEqualTo(order.events.size)
    }
}
