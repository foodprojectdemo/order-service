package com.gitlab.foodprojectdemo.orderservice.domain.order

import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderStatus.NEW
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

@Retention(RUNTIME)
@Target(VALUE_PARAMETER)
annotation class OrderWithStatus(val value: OrderStatus = NEW)
