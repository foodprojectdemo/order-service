package com.gitlab.foodprojectdemo.orderservice.domain.cart

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartItem
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture
import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
import com.gitlab.foodprojectdemo.orderservice.fixture.randomMoreThanThis
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

private class CartTest : AbstractTest() {

    @Test
    fun `should be equal`(cartId: CartId) {
        val cart1 = Cart(cartId)
        val cart2 = Cart(cartId)
        assertThat(cart1).isEqualTo(cart2)
    }

    @Test
    fun `should add one dish`(@EmptyCart cart: Cart, dish: Dish) {
        cart.addDish(dish)

        assertThat(cart.items).anyMatch { cartItem ->
            cartItem.dish == dish && cartItem.quantity == Quantity.one
        }

        assertThat(cart.total).isEqualTo(dish.price)
        assertThat(cart.discount).isEqualTo(Money.zero)
    }

    @Test
    fun `should add a dish with some quantity`(@EmptyCart cart: Cart, dish: Dish, quantity: Quantity) {
        cart.addDish(dish, quantity)

        assertThat(cart.items).anyMatch { cartItem ->
            cartItem.dish == dish && cartItem.quantity == quantity
        }

        assertThat(cart.total).isEqualTo(dish.price * quantity)
        assertThat(cart.discount).isEqualTo(Money.zero)
    }

    @Test
    fun `should add a gift with some quantity`(
        @EmptyCart cart: Cart,
        dish: Dish,
        quantity: Quantity
    ) {
        cart.addDish(dish)
        val gift = DishFixture.dish(restaurantId = dish.restaurantId)
        cart.addGift(gift, quantity)

        assertThat(cart.gifts).anyMatch { cartItem ->
            cartItem.dish == gift && cartItem.quantity == quantity
        }

        assertThat(cart.total).isEqualTo(dish.price)
        assertThat(cart.discount).isEqualTo(Money.zero)
    }

    @Test
    fun `should not add a gift from another restaurant`(cart: Cart, anotherRestaurantDish: Dish) {
        assertThat(cart.restaurantId).isNotEqualTo(anotherRestaurantDish.restaurantId)

        assertThrows<AnotherRestaurantDishException> {
            cart.addGift(anotherRestaurantDish)
        }
    }

    @Test
    fun `should not add dishes from different restaurants, expect AnotherRestaurantDishException`(
        cart: Cart,
        anotherRestaurantDish: Dish
    ) {
        assertThat(cart.restaurantId).isNotEqualTo(anotherRestaurantDish.restaurantId)

        assertThrows<AnotherRestaurantDishException> {
            cart.addDish(anotherRestaurantDish)
        }
    }

    @Test
    fun `should add a dish from another restaurant after remove`(
        @EmptyCart cart: Cart,
        dish: Dish,
        anotherRestaurantDish: Dish
    ) {
        assertThat(anotherRestaurantDish.restaurantId).isNotEqualTo(dish.restaurantId)

        cart.addDish(dish)

        assertThrows<AnotherRestaurantDishException> {
            cart.addDish(anotherRestaurantDish)
        }

        cart.remove(cart.items.first())

        cart.addDish(anotherRestaurantDish)

        assertThat(cart.items).anyMatch { it.dish == anotherRestaurantDish }
    }

    @Test
    fun `should add another restaurant dish after clear`(
        cart: Cart,
        anotherRestaurantDish: Dish
    ) {
        assertThat(cart.restaurantId).isNotEqualTo(anotherRestaurantDish.restaurantId)

        assertThrows<AnotherRestaurantDishException> {
            cart.addDish(anotherRestaurantDish)
        }

        cart.clear()

        cart.addDish(anotherRestaurantDish)

        assertThat(cart.items).anyMatch { it.dish == anotherRestaurantDish }
    }

    @Test
    fun `should change the quantity`(cart: Cart, quantity: Quantity) {
        cart.changeQuantity(cart.items.first(), quantity)

        assertThat(cart.items.first().quantity).isEqualTo(quantity)
    }

    @Test
    fun `should not change quantity if the cart item does not exist`(cart: Cart, quantity: Quantity) {
        val unknownCartItem = cartItem()
        assertThat(cart.items).doesNotContain(unknownCartItem)

        val total = cart.total
        cart.changeQuantity(unknownCartItem, quantity)

        assertThat(cart.total).isEqualTo(total)
    }

    @Test
    fun `should assign a discount`(cart: Cart) {
        val discount = cart.subTotal.randomLessThanThis()

        cart.assignDiscount(discount)

        assertThat(cart.discount).isEqualTo(discount)
        assertThat(cart.total).isEqualTo(cart.subTotal - discount)
    }

    @Test
    fun `should assign a max discount equal the subtotal`(cart: Cart) {
        val discount = cart.subTotal.randomMoreThanThis()

        cart.assignDiscount(discount)

        assertThat(cart.discount).isEqualTo(cart.subTotal)
        assertThat(cart.total).isEqualTo(Money.zero)
    }

    @Test
    fun `should add a discount`(cart: Cart) {
        val discount1 = cart.total.randomLessThanThis()

        cart add discount1

        assertThat(cart.discount).isEqualTo(discount1)

        val discount2 = cart.total.randomLessThanThis()
        cart add discount2

        assertThat(cart.discount).isEqualTo(discount1 + discount2)
        assertThat(cart.total).isEqualTo(cart.subTotal - (discount1 + discount2))
    }

    @Test
    fun `should not add a discount more than the cart total`(@EmptyCart cart: Cart, dish: Dish, quantity: Quantity) {
        cart.addDish(dish, quantity)
        val discount1 = cart.total.randomLessThanThis()

        cart add discount1

        assertThat(cart.discount).isEqualTo(discount1)
        assertThat(cart.total).isEqualTo(dish.price * quantity - discount1)

        val discount2 = cart.total.randomMoreThanThis()
        cart add discount2

        assertThat(cart.discount).isEqualTo(cart.subTotal)
        assertThat(cart.total).isEqualTo(Money.zero)
    }

    @Test
    fun `should remove the cart item`(cart: Cart) {
        val item = cart.items.first()

        cart.remove(cart.items.first())

        assertThat(cart.items).doesNotContain(item)
    }

    @Test
    fun `should clear the cart`(cart: Cart) {
        assertThat(cart.isEmpty).isFalse

        cart.clear()

        assertThat(cart.isEmpty).isTrue
        assertThat(cart.items).isEmpty()
    }
}
