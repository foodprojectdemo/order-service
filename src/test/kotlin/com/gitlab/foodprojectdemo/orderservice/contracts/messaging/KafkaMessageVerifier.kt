package com.gitlab.foodprojectdemo.orderservice.contracts.messaging

import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.TestConsumer
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.verifier.converter.YamlContract
import org.springframework.cloud.contract.verifier.messaging.MessageVerifier
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.CyclicBarrier
import java.util.concurrent.TimeUnit

@Component
class KafkaMessageVerifier(
    private val kafkaSender: KafkaSender<String, String>
) : MessageVerifier<Message<*>> {

    @Autowired
    private lateinit var testConsumer: TestConsumer

    private val cyclicBarrier = CyclicBarrier(1)

    override fun send(message: Message<*>?, destination: String?, contract: YamlContract?) {
        send(message!!.payload, message.headers, destination, contract)
    }

    override fun <T : Any?> send(
        payload: T,
        headers: MutableMap<String, Any>?,
        destination: String?,
        contract: YamlContract?
    ) {
        kafkaSender.send(
            SenderRecord.create(
                ProducerRecord<String, String>(destination, payload.toString()),
                payload.hashCode()
            ).toMono()
        ).next().block()
    }

    override fun receive(
        destination: String,
        timeout: Long,
        timeUnit: TimeUnit,
        contract: YamlContract?
    ): Message<*>? {
        cyclicBarrier.await(timeout, timeUnit)
        return message(destination)
    }

    override fun receive(destination: String, contract: YamlContract): Message<*>? {
        return receive(destination, 5, TimeUnit.SECONDS, contract)
    }

    private fun message(destination: String): Message<*>? {
        return testConsumer.pop(destination)?.let {
            MessageBuilder.createMessage(it.value(), MessageHeaders(mapOf()))
        }
    }
}
