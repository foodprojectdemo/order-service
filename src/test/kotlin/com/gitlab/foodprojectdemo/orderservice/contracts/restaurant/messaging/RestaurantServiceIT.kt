package com.gitlab.foodprojectdemo.orderservice.contracts.restaurant.messaging

import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketAcceptedEvent
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketReadyEvent
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketRejectedEvent
import com.gitlab.foodprojectdemo.orderservice.contracts.messaging.KafkaMessageVerifier
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.AbstractKafkaTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener.EventListener
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener.KafkaEventListener
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import org.awaitility.Awaitility
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.contract.stubrunner.StubTrigger
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner
import org.springframework.test.context.ContextConfiguration

@AutoConfigureStubRunner(
    ids = ["/META-INF/com.gitlab.foodprojectdemo/restaurant-service/**/mappings/**/*.json"]
)
@ContextConfiguration(classes = [KafkaMessageVerifier::class, KafkaEventListener::class])
private class RestaurantServiceIT : AbstractKafkaTest() {
    @Autowired
    private lateinit var trigger: StubTrigger

    @MockBean
    private lateinit var eventListener: EventListener

    @Test
    fun `should receive ticket accepted event`() {
        testTemplate<TicketAcceptedEvent>("TicketAcceptedEvent")
    }

    @Test
    fun `should receive ticket rejected event`() {
        testTemplate<TicketRejectedEvent>("TicketRejectedEvent")
    }

    @Test
    fun `should receive ticket ready event`() {
        testTemplate<TicketReadyEvent>("TicketReadyEvent")
    }

    private inline fun <reified T : DomainEventDto<*>> testTemplate(eventLabel: String) {
        trigger.trigger(eventLabel)

        Awaitility.await().untilAsserted {
            verify(eventListener).onEvent(any<T>())
        }
    }
}
