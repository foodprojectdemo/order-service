package com.gitlab.foodprojectdemo.orderservice.contracts.messaging

import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.order
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.AbstractKafkaTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher.DomainEventPublisher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier
import org.springframework.test.context.ContextConfiguration

@AutoConfigureMessageVerifier
@ContextConfiguration(classes = [DomainEventPublisher::class, KafkaMessageVerifier::class])
class EventsBaseTest : AbstractKafkaTest() {

    @Autowired
    private lateinit var domainEventPublisher: DomainEventPublisher

    fun orderPlacedEvent() {
        val order = order()
        domainEventPublisher.publish(order.events.first()).block()
    }
}
