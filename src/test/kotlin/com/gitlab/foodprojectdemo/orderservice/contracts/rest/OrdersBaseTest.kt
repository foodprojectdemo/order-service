package com.gitlab.foodprojectdemo.orderservice.contracts.rest

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.ui.rest.order.OrderController
import com.gitlab.foodprojectdemo.orderservice.ui.rest.order.RestaurantOrdersController
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.restassured.module.webtestclient.RestAssuredWebTestClient
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@AutoConfigureMessageVerifier
@WebFluxTest(
    value = [
        OrderController::class,
        RestaurantOrdersController::class
    ]
)
open class OrdersBaseTest : AbstractTest() {

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockBean
    private lateinit var orderService: OrderService

    @BeforeEach
    fun setUp(order: Order) {
        whenever(orderService.getCustomerOrders(any())).thenReturn(listOf(order).toFlux())
        whenever(orderService.getRestaurantOrders(any())).thenReturn(listOf(order).toFlux())
        whenever(orderService.checkout(any())).thenReturn(order.toMono())
        whenever(orderService.getOrder(any())).thenReturn(order.toMono())

        RestAssuredWebTestClient.webTestClient(webTestClient)
    }
}
