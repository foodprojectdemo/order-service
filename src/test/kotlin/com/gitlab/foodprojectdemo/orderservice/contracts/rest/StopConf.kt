package com.gitlab.foodprojectdemo.orderservice.contracts.rest

import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.webflux.WebFluxConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import

@SpringBootApplication(
    scanBasePackages = [
        "com.gitlab.foodprojectdemo.orderservice.ui",
    ]
)
@Import(WebFluxConfig::class)
class StopConf
