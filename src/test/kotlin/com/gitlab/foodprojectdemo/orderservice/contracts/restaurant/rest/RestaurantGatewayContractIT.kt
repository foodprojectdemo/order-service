package com.gitlab.foodprojectdemo.orderservice.contracts.restaurant.rest

import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.AbstractGatewayTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.AbsoluteDiscountPromoActionDto
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.AbsoluteDiscountPromoCodeDto
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.GiftByCartAmountPromoActionDto
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.PercentDiscountPromoActionDto
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.PercentDiscountPromoCodeDto
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant.RestaurantGateway
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource

@AutoConfigureWireMock(
    stubs = ["classpath:/META-INF/com.gitlab.foodprojectdemo/restaurant-service/**/mappings/**/*.json"],
    port = 0
)
@ContextConfiguration(classes = [RestaurantGateway::class])
@TestPropertySource(properties = ["restaurant-service.url=http://localhost:\${wiremock.server.port}/"])
class RestaurantGatewayContractIT : AbstractGatewayTest() {
    @Autowired
    private lateinit var restaurantGateway: RestaurantGateway

    @Test
    fun `should return a dish`(dishId: DishId) {
        restaurantGateway.getDish(dishId).block()
    }

    @Test
    fun `should return promo actions`(restaurantId: RestaurantId) {
        val promos = restaurantGateway.getRestaurantPromoActions(restaurantId).collectList().block()

        assertThat(promos!!.size).isEqualTo(3)
        assertThat(promos).anySatisfy {
            assertThat(it).isInstanceOf(AbsoluteDiscountPromoActionDto::class.java)
        }
        assertThat(promos).anySatisfy {
            assertThat(it).isInstanceOf(PercentDiscountPromoActionDto::class.java)
        }
        assertThat(promos).anySatisfy {
            assertThat(it).isInstanceOf(GiftByCartAmountPromoActionDto::class.java)
        }
    }

    @Test
    fun `should return absolute promo code`(restaurantId: RestaurantId) {
        val promoCode = restaurantGateway.getRestaurantPromoCode(PromoCodeId("ABSOLUTE"), restaurantId).block()
        assertThat(promoCode).isInstanceOf(AbsoluteDiscountPromoCodeDto::class.java)
    }

    @Test
    fun `should return percent promo code`(restaurantId: RestaurantId) {
        val promoCode = restaurantGateway.getRestaurantPromoCode(PromoCodeId("PERCENT"), restaurantId).block()
        assertThat(promoCode).isInstanceOf(PercentDiscountPromoCodeDto::class.java)
    }
}
