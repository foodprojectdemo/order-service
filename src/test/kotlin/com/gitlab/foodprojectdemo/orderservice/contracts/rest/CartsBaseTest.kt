package com.gitlab.foodprojectdemo.orderservice.contracts.rest

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartWithItems
import com.gitlab.foodprojectdemo.orderservice.ui.rest.cart.CartController
import com.gitlab.foodprojectdemo.orderservice.ui.rest.cart.CartItemsController
import com.gitlab.foodprojectdemo.orderservice.ui.rest.cart.CartPromoCodeController
import com.gitlab.foodprojectdemo.orderservice.ui.rest.cart.PreOrderController
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.restassured.module.webtestclient.RestAssuredWebTestClient
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@AutoConfigureMessageVerifier
@WebFluxTest(
    value = [
        CartController::class,
        CartItemsController::class,
        CartPromoCodeController::class,
        PreOrderController::class
    ]
)
open class CartsBaseTest : AbstractTest() {

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockBean
    private lateinit var cartService: CartService

    @BeforeEach
    fun setUp() {
        val cart = cartWithItems().placePreOrder().toMono()

        whenever(cartService.createCart()).thenReturn(cart)
        whenever(cartService.getCart(any())).thenReturn(cart)
        whenever(cartService.addDishToCart(any())).thenReturn(cart)
        whenever(cartService.changeCartItemQuantity(any())).thenReturn(cart)
        whenever(cartService.removeCartItem(any())).thenReturn(cart)
        whenever(cartService.clearCart(any())).thenReturn(cart)
        whenever(cartService.applyPromoCode(any())).thenReturn(cart)
        whenever(cartService.removePromoCode(any())).thenReturn(cart)
        whenever(cartService.placePreOrder(any())).thenReturn(Mono.empty())
        whenever(cartService.getPreOrder(any())).thenReturn(cart.flatMap { it.preOrder.toMono() })

        RestAssuredWebTestClient.webTestClient(webTestClient)
    }
}
