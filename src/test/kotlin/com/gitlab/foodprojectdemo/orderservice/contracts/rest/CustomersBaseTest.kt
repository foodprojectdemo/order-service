package com.gitlab.foodprojectdemo.orderservice.contracts.rest

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.customer.CustomerService
import com.gitlab.foodprojectdemo.orderservice.fixture.CustomerFixture.customer
import com.gitlab.foodprojectdemo.orderservice.ui.rest.customer.CustomerController
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.restassured.module.webtestclient.RestAssuredWebTestClient
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toMono

@AutoConfigureMessageVerifier
@WebFluxTest(CustomerController::class)
open class CustomersBaseTest : AbstractTest() {

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockBean
    private lateinit var customerService: CustomerService

    @BeforeEach
    fun setUp() {
        val customer = customer().toMono()
        whenever(customerService.signUpCustomer()).thenReturn(customer)
        whenever(customerService.getCustomer(any())).thenReturn(customer)
        RestAssuredWebTestClient.webTestClient(webTestClient)
    }
}
