package com.gitlab.foodprojectdemo.orderservice.componenttest

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@Tag("component")
abstract class AbstractComponentTest : AbstractTest()
