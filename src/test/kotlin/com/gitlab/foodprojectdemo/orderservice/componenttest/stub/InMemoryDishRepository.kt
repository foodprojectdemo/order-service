package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

@TestComponent
class InMemoryDishRepository : DishRepository {
    protected val storage = ConcurrentHashMap<DishId, Dish>()

    override fun getDish(id: DishId): Mono<Dish> = storage.getOrDefault(id, null).toMono()

    fun save(dish: Dish): Mono<Dish> {
        storage[dish.id] = dish
        return dish.toMono()
    }
}
