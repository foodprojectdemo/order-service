package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoActionRepository
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionId
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

@TestComponent("testCartPromoActionRepository")
class InMemoryCartPromoActionRepository : CartPromoActionRepository {

    protected val storage = ConcurrentHashMap<PromoActionId, CartPromoAction>()

    fun findById(id: PromoActionId): Mono<CartPromoAction> = storage.getOrDefault(id, null).toMono()

    fun save(entity: CartPromoAction): Mono<CartPromoAction> {
        storage[entity.id] = entity
        return entity.toMono()
    }

    fun clear() {
        storage.clear()
    }

    override fun getRestaurantCartPromoActions(restaurantId: RestaurantId): Flux<CartPromoAction> =
        Flux.fromIterable(storage.values)
            .filter { promoAction -> promoAction.restaurantId == restaurantId }
}
