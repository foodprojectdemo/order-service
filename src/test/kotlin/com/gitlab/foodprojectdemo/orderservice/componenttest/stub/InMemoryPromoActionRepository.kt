package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionRepository
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

@TestComponent("testPromoActionRepository")
class InMemoryPromoActionRepository : PromoActionRepository {

    protected val storage = ConcurrentHashMap<PromoActionId, PromoAction<*>>()

    fun findById(id: PromoActionId): Mono<PromoAction<*>> = storage.getOrDefault(id, null).toMono()

    fun save(entity: PromoAction<*>): Mono<PromoAction<*>> {
        storage[entity.id] = entity
        return entity.toMono()
    }

    fun clear() {
        storage.clear()
    }

    override fun findByRestaurantId(restaurantId: RestaurantId): Flux<PromoAction<*>> =
        Flux.fromIterable(storage.values)
            .filter { promoAction -> promoAction.restaurantId == restaurantId }
}
