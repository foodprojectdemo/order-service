package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeRepository
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

@TestComponent
class InMemoryPromoCodeRepository : PromoCodeRepository {
    protected val storage = ConcurrentHashMap<PromoCodeId, PromoCode>()

    fun save(entity: PromoCode): Mono<PromoCode> {
        storage[entity.id] = entity
        return entity.toMono()
    }

    override fun getRestaurantPromoCode(promoCodeId: PromoCodeId, restaurantId: RestaurantId): Mono<PromoCode> =
        Flux.fromIterable(storage.values)
            .filter { it.id == promoCodeId && it.restaurantId == restaurantId }
            .take(1)
            .log("take")
            .next()
            .log("findByIdAndRestaurantId")
}
