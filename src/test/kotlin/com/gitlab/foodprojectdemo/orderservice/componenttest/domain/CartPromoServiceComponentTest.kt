package com.gitlab.foodprojectdemo.orderservice.componenttest.domain

import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryCartPromoActionRepository
import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryPromoActionStrategyResolver
import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryPromoCodeRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money.Companion.zero
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity.Companion.one
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoService
import com.gitlab.foodprojectdemo.orderservice.domain.promo.SumDiscountPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.emptyCart
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.absoluteDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.giftByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.percentDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoCodeFixture.absoluteDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoCodeFixture.giftPromoCode
import com.gitlab.foodprojectdemo.orderservice.fixture.PromoCodeFixture.percentDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId
import com.gitlab.foodprojectdemo.orderservice.fixture.random
import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.toMoney
import com.gitlab.foodprojectdemo.orderservice.toQuantity
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

private class CartPromoServiceComponentTest : AbstractDomainComponentTest() {

    @Autowired
    private lateinit var cartPromoActionRepository: InMemoryCartPromoActionRepository

    @Autowired
    private lateinit var promoActionStrategyResolver: InMemoryPromoActionStrategyResolver

    @Autowired
    private lateinit var promoCodeRepository: InMemoryPromoCodeRepository

    @Autowired
    private lateinit var cartPromoService: CartPromoService

    @BeforeEach
    fun setUp() {
        // TODO: clear other repositories and check saving data between tests
        cartPromoActionRepository.clear()
    }

    @Test
    fun `should just return empty cart`(@EmptyCart cart: Cart) {
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.isEmpty).isTrue
    }

    @Test
    fun `should just return the cart`(cart: Cart) {
        cart.clearGifts()

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(zero)
        assertThat(actual.gifts).isEmpty()
    }

    @Test
    fun `should apply absolute discount`(cart: Cart) {
        val cartAmount = cart.subTotal
        val discount = cartAmount.randomLessThanThis()
        val promo = absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)

        cartPromoActionRepository.save(promo)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(discount)
    }

    @Test
    fun `should apply percent discount`(cart: Cart, percent: Percent) {
        val cartAmount = cart.subTotal
        val promo = percentDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, percent)

        cartPromoActionRepository.save(promo)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(cart.subTotal * percent)
    }

    @Test
    fun `should use max discount strategy`(cart: Cart, percent: Percent) {
        val cartAmount = cart.subTotal
        val discount = cartAmount.randomLessThanThis()
        val absolutePromo = absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
        val percentPromo = percentDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, percent)

        cartPromoActionRepository.save(absolutePromo)
        cartPromoActionRepository.save(percentPromo)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(maxOf(cart.subTotal * percent, discount))
    }

    @Test
    fun `should use sum discount strategy`(cart: Cart) {
        val cartAmount = cart.subTotal
        val percentDiscount = Percent.random(1, 50)
        val discount = (cartAmount * (Percent.hundred - percentDiscount)).randomLessThanThis()

        val absolutePromo = absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
        val percentPromo = percentDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, percentDiscount)

        cartPromoActionRepository.save(absolutePromo)
        cartPromoActionRepository.save(percentPromo)
        promoActionStrategyResolver.save(cart.restaurantId, SumDiscountPromoActionStrategy)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(cart.subTotal * percentDiscount + discount)
    }

    @Test
    fun `should apply gift`(cart: Cart) {
        val cartAmount = cart.subTotal
        val gift = dish(restaurantId = cart.restaurantId)
        val quantity = Quantity.random()
        val promo = giftByCartAmountPromoAction(cart.restaurantId, cartAmount, gift, quantity)

        cartPromoActionRepository.save(promo)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.gifts).anySatisfy { cartItem ->
            assertThat(cartItem.dish).isEqualTo(gift)
            assertThat(cartItem.quantity).isEqualTo(quantity)
        }
    }

    @Test
    fun `should apply discount and gift`(cart: Cart) {
        val cartAmount = cart.subTotal
        val discount = cartAmount.randomLessThanThis()
        val gift = dish(restaurantId = cart.restaurantId)
        val quantity = Quantity.random()

        val absolutePromo =
            absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
        val giftPromo = giftByCartAmountPromoAction(cart.restaurantId, cartAmount, gift, quantity)

        cartPromoActionRepository.save(absolutePromo)
        cartPromoActionRepository.save(giftPromo)

        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(discount)
        assertThat(actual.gifts).anySatisfy { cartItem ->
            assertThat(cartItem.dish).isEqualTo(gift)
            assertThat(cartItem.quantity).isEqualTo(quantity)
        }
    }

    @Test
    fun `should apply absolute discount promo code`(cart: Cart) {
        val promoCode = absoluteDiscountPromoCode(cart.restaurantId, cart.subTotal.randomLessThanThis())
        promoCodeRepository.save(promoCode)

        cart.applyPromoCode(promoCode.id)
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(promoCode.benefit.money)
    }

    @Test
    fun `should apply percent discount promo code`(cart: Cart) {
        val promoCode = percentDiscountPromoCode(cart.restaurantId)
        promoCodeRepository.save(promoCode)

        cart.applyPromoCode(promoCode.id)
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(cart.subTotal * promoCode.benefit.percent)
    }

    @Test
    fun `should apply gift promo code`(cart: Cart) {
        val gift = dish(cart.restaurantId)
        val promoCode = giftPromoCode(cart.restaurantId, gift)
        promoCodeRepository.save(promoCode)

        cart.applyPromoCode(promoCode.id)
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.gifts).anySatisfy { cartItem ->
            assertThat(cartItem.dish).isEqualTo(gift)
            assertThat(cartItem.quantity).isEqualTo(one)
        }
    }

    @Test
    fun `should apply discount promo, gift promo and discount promo code`(cart: Cart) {
        val cartAmount = cart.subTotal
        val halfOfCartAmount = (cartAmount / 2.0)
        val discount = halfOfCartAmount.randomLessThanThis()
        val gift = dish(restaurantId = cart.restaurantId)
        val quantity = Quantity.random()

        val absolutePromo = absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
        val giftPromo = giftByCartAmountPromoAction(cart.restaurantId, cartAmount, gift, quantity)
        cartPromoActionRepository.save(absolutePromo)
        cartPromoActionRepository.save(giftPromo)

        val promoCode = absoluteDiscountPromoCode(cart.restaurantId, halfOfCartAmount.randomLessThanThis())
        promoCodeRepository.save(promoCode)

        cart.applyPromoCode(promoCode.id)
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(discount + promoCode.benefit.money)
        assertThat(actual.gifts).anySatisfy { cartItem ->
            assertThat(cartItem.dish).isEqualTo(gift)
            assertThat(cartItem.quantity).isEqualTo(quantity)
        }
    }

    @Test
    fun `should apply discount promo, gift promo and gift promo code`(cart: Cart) {
        val cartAmount = cart.subTotal
        val discount = cartAmount.randomLessThanThis()
        val gift = dish(restaurantId = cart.restaurantId)
        val quantity = Quantity.random()

        val absolutePromo = absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
        val giftPromo = giftByCartAmountPromoAction(cart.restaurantId, cartAmount, gift, quantity)
        cartPromoActionRepository.save(absolutePromo)
        cartPromoActionRepository.save(giftPromo)

        val promoCode = giftPromoCode(cart.restaurantId, gift)
        promoCodeRepository.save(promoCode)

        cart.applyPromoCode(promoCode.id)
        val actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(discount)
        assertThat(actual.gifts).anySatisfy { cartItem ->
            assertThat(cartItem.dish).isEqualTo(gift)
            assertThat(cartItem.quantity).isEqualTo(quantity + one)
        }
    }

    @Test
    fun assignByStep() {
        val restaurantId = restaurantId()

        val cartAmount500 = 500.toMoney()
        val discount100 = 100.toMoney()
        val cartAmount1100 = 1100.toMoney()
        val discount200 = 200.toMoney()
        val cartAmount2000 = 2000.toMoney()
        val discount600 = 600.toMoney()

        val discountMap = mapOf(
            cartAmount500 to discount100,
            cartAmount1100 to discount200,
            cartAmount2000 to discount600,
        )
        val promo = absoluteDiscountByCartAmountPromoAction(restaurantId, discountMap)

        cartPromoActionRepository.save(promo)

        // No discount for empty cart
        val cart = emptyCart()

        var actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.discount).isEqualTo(zero)

        // first level discount
        cart.addDish(dish(restaurantId = restaurantId, price = 250.toMoney()), 2.toQuantity())

        actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.subTotal).isEqualTo(cartAmount500)
        assertThat(actual.discount).isEqualTo(discount100)

        // second level discount
        cart.addDish(dish(restaurantId = restaurantId, price = 200.toMoney()), 3.toQuantity())

        actual = cartPromoService.applyTo(cart).block()!!

        logger(this).info("CART {}", cart)

        assertThat(actual.subTotal).isEqualTo(cartAmount1100)
        assertThat(actual.discount).isEqualTo(discount200)

        // third level discount
        cart.addDish(dish(restaurantId = restaurantId, price = 1000.toMoney()))

        actual = cartPromoService.applyTo(cart).block()!!

        assertThat(actual.subTotal).isGreaterThan(cartAmount1100)
        assertThat(actual.discount).isEqualTo(discount600)
    }
}
