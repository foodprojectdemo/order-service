package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.DiscountPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionStrategyResolver
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

@TestComponent("testPromoActionStrategyResolver")
class InMemoryPromoActionStrategyResolver : PromoActionStrategyResolver {
    private val discountStorage = ConcurrentHashMap<RestaurantId, DiscountPromoActionStrategy>()
    private val giftStorage = ConcurrentHashMap<RestaurantId, GiftPromoActionStrategy>()

    override fun discountPromoActionStrategy(restaurantId: RestaurantId): Mono<DiscountPromoActionStrategy> =
        discountStorage.getOrDefault(restaurantId, null).toMono()

    override fun giftPromoActionStrategy(restaurantId: RestaurantId): Mono<GiftPromoActionStrategy> =
        giftStorage.getOrDefault(restaurantId, null).toMono()

    fun save(restaurantId: RestaurantId, promoActionStrategy: PromoActionStrategy) {
        when (promoActionStrategy) {
            is DiscountPromoActionStrategy -> discountStorage[restaurantId] = promoActionStrategy
            is GiftPromoActionStrategy -> giftStorage[restaurantId] = promoActionStrategy
        }
    }
}
