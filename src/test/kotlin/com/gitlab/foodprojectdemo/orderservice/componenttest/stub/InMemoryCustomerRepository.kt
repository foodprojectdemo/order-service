package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerRepository
import org.springframework.boot.test.context.TestComponent

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
@TestComponent
class InMemoryCustomerRepository : AbstractInMemoryRepository<Customer, CustomerId>(), CustomerRepository
