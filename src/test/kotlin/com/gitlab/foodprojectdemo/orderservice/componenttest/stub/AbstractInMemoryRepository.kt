package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Entity
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.ConcurrentHashMap

abstract class AbstractInMemoryRepository<ENTITY : Entity<ID>, ID : Any> {
    protected val storage = ConcurrentHashMap<ID, ENTITY>()

    fun findById(id: ID): Mono<ENTITY> = storage.getOrDefault(id, null).toMono()

    fun save(entity: ENTITY): Mono<ENTITY> {
        storage[entity.id] = entity
        return entity.toMono()
    }

    fun clear() {
        storage.clear()
    }
}
