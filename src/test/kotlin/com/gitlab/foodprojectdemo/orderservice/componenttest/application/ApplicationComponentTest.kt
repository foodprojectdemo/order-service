package com.gitlab.foodprojectdemo.orderservice.componenttest.application

import com.gitlab.foodprojectdemo.orderservice.application.ApplicationContextConfiguration
import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService
import com.gitlab.foodprojectdemo.orderservice.componenttest.domain.AbstractDomainComponentTest
import org.mockito.Mockito.mock
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.test.context.support.AnnotationConfigContextLoader

@TestConfiguration
class ApplicationStubs {
    @Bean
    fun notificationService(): NotificationService =
        mock(NotificationService::class.java)
}

@ContextHierarchy(
    value = [
        ContextConfiguration(
            name = "application-configuration",
            classes = [ApplicationStubs::class, ApplicationContextConfiguration::class],
            loader = AnnotationConfigContextLoader::class
        )
    ]
)
abstract class AbstractApplicationDomainComponentTest : AbstractDomainComponentTest()
