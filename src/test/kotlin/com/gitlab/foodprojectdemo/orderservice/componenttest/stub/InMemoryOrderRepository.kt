package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderRepository
import org.springframework.boot.test.context.TestComponent
import reactor.core.publisher.Flux

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
@TestComponent
class InMemoryOrderRepository : AbstractInMemoryRepository<Order, OrderId>(), OrderRepository {

    override fun findByCustomerId(customerId: CustomerId): Flux<Order> {
        TODO("Not yet implemented")
    }

    override fun findByRestaurantId(restaurantId: RestaurantId): Flux<Order> {
        TODO("Not yet implemented")
    }
}
