package com.gitlab.foodprojectdemo.orderservice.componenttest.domain

import com.gitlab.foodprojectdemo.orderservice.componenttest.AbstractComponentTest
import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.StubContextConfiguration
import com.gitlab.foodprojectdemo.orderservice.domain.DomainContextConfiguration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.test.context.support.AnnotationConfigContextLoader

@ContextHierarchy(
    value = [
        ContextConfiguration(
            name = "stub-repositories",
            classes = [StubContextConfiguration::class],
            loader = AnnotationConfigContextLoader::class
        ),
        ContextConfiguration(
            name = "domain-configuration",
            classes = [DomainContextConfiguration::class],
            loader = AnnotationConfigContextLoader::class
        ),

    ]
)
abstract class AbstractDomainComponentTest : AbstractComponentTest()
