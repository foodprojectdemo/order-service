package com.gitlab.foodprojectdemo.orderservice.componenttest.stub

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.ComponentScan

@ComponentScan
@TestConfiguration
class StubContextConfiguration
