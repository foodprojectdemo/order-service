package com.gitlab.foodprojectdemo.orderservice.componenttest.application

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.AddDishToCartCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ChangeCartItemQuantityCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.RemoveCartItemCommand
import com.gitlab.foodprojectdemo.orderservice.domain.Saved
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

private class CartServiceComponentTest : AbstractApplicationDomainComponentTest() {
    @Autowired
    private lateinit var cartService: CartService

    @Test
    fun `should create empty cart`() {
        val cart = cartService.createCart()
            .map(Cart::id)
            .flatMap(cartService::getCart).block()!!

        assertThat(cart.isEmpty).isTrue
    }

    @Test
    fun `should return empty cart`(@Saved @EmptyCart cartId: CartId) {
        val cart = cartService.getCart(cartId).block()!!

        assertThat(cart.isEmpty).isTrue
    }

    @Test
    fun `should return not empty cart`(@Saved cartId: CartId) {
        val cart = cartService.getCart(cartId).block()!!

        assertThat(cart.isNotEmpty).isTrue
    }

    @Test
    fun `should add a dish to the cart`(@Saved @EmptyCart cartId: CartId, @Saved dish: Dish, quantity: Quantity) {
        val cart = cartService.addDishToCart(AddDishToCartCommand(cartId, dish.id, quantity))
            .map(Cart::id)
            .flatMap(cartService::getCart).block()!!

        assertThat(cart.items).anyMatch { it.dish == dish }
    }

    @Test
    fun `should change cart item quantity`(@Saved cartId: CartId, quantity: Quantity) {
        val cart = cartService.changeCartItemQuantity(ChangeCartItemQuantityCommand(cartId, 0, quantity))
            .map(Cart::id)
            .flatMap(cartService::getCart).block()!!

        val cartItem = cart.items.first()

        assertThat(cartItem.quantity).isEqualTo(quantity)
    }

    @Test
    fun `should remove cart item`(@Saved cartId: CartId) {
        val cartItem = cartService.getCart(cartId).map(Cart::items).block()!!.first()

        val cart = cartService.removeCartItem(RemoveCartItemCommand(cartId, 0))
            .map(Cart::id)
            .flatMap(cartService::getCart).block()!!

        assertThat(cart.items).doesNotContain(cartItem)
    }

    @Test
    fun `should clear the cart`(@Saved cartId: CartId) {
        val cart = cartService.clearCart(cartId)
            .map(Cart::id)
            .flatMap(cartService::getCart).block()!!

        assertThat(cart.isEmpty).isTrue
    }
}
