// package com.gitlab.foodprojectdemo.orderservice.componenttest.domain
// TODO: restore
// import org.junit.jupiter.api.Test
// import org.junit.jupiter.api.assertThrows
// import org.springframework.beans.factory.annotation.Autowired
// import org.springframework.boot.test.context.TestConfiguration
// import org.springframework.context.annotation.Bean
// import org.springframework.test.context.ContextConfiguration
// import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryDishRepository
// import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryPromoActionRepository
// import com.gitlab.foodprojectdemo.orderservice.componenttest.stub.InMemoryPromoActionStrategyResolver
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionRepository
// import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
// import com.gitlab.foodprojectdemo.orderservice.domain.restaurant.RestaurantId
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.AllGiftsPromoActionStrategy
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.DiscountPromoActionStrategy
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftPromoActionStrategy
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.MaxDiscountPromoActionStrategy
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoAction
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionStrategyFactory.getStrategy
// import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionStrategyResolver
// import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartWithItems
// import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
// import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.absoluteDiscountByCartAmountPromoAction
// import com.gitlab.foodprojectdemo.orderservice.fixture.PromoActionFixture.giftByCartAmountPromoAction
// import com.gitlab.foodprojectdemo.orderservice.fixture.random
// import com.gitlab.foodprojectdemo.orderservice.fixture.randomLessThanThis
// import reactor.core.publisher.Flux
// import reactor.core.publisher.Mono
// import reactor.test.StepVerifier
// import java.time.Duration
//
// @TestConfiguration
// private class LagTestConfiguration {
//    @Bean
//    fun lagPromoActionRepository(): PromoActionRepository =
//        object : InMemoryPromoActionRepository() {
//            override fun findByRestaurantId(restaurantId: RestaurantId): Flux<PromoAction> =
//                super.findByRestaurantId(restaurantId)
//                    .delaySequence(Duration.ofSeconds(30))
//        }
//
//    @Bean
//    fun lagPromoActionStrategyResolver(): PromoActionStrategyResolver =
//        object : InMemoryPromoActionStrategyResolver() {
//            override fun discountPromoActionStrategy(restaurantId: RestaurantId): Mono<DiscountPromoActionStrategy> =
//                super.discountPromoActionStrategy(restaurantId)
//                    .delayElement(Duration.ofSeconds(20))
//
//            override fun giftPromoActionStrategy(restaurantId: RestaurantId): Mono<GiftPromoActionStrategy> =
//                super.giftPromoActionStrategy(restaurantId)
//                    .delayElement(Duration.ofSeconds(25))
//        }
// }
//
// @ContextConfiguration(name = "stub-repositories", classes = [LagTestConfiguration::class])
// private class LagApplyingPromoServiceComponentTest : AbstractDomainComponentTest() {
//
//    @Autowired
//    private lateinit var applyingPromoService: ApplyingPromoService
//
//    @Autowired
//    private lateinit var promoActionRepository: InMemoryPromoActionRepository
//
//    @Autowired
//    private lateinit var promoActionStrategyResolver: InMemoryPromoActionStrategyResolver
//
//    @Autowired
//    private lateinit var dishRepository: InMemoryDishRepository
//
//    @Test
//    fun lag() {
//        val cart = cartWithItems()
//        val cartAmount = cart.subTotal
//        val discount = cartAmount.randomLessThanThis()
//        val gift = dish(restaurantId = cart.restaurantId)
//        val quantity = Quantity.random()
//
//        val absolutePromo: PromoAction =
//            absoluteDiscountByCartAmountPromoAction(cart.restaurantId, cartAmount, discount)
//        val giftPromo: PromoAction = giftByCartAmountPromoAction(cart.restaurantId, cartAmount, gift.id, quantity)
//
//        promoActionStrategyResolver.save(cart.restaurantId, getStrategy(MaxDiscountPromoActionStrategy::class))
//        promoActionStrategyResolver.save(cart.restaurantId, getStrategy(AllGiftsPromoActionStrategy::class))
//
//        promoActionRepository.save(absolutePromo)
//        promoActionRepository.save(giftPromo)
//        dishRepository.save(gift)
//
//        assertThrows<AssertionError> {
//            StepVerifier.withVirtualTime {
//                applyingPromoService.applyTo(cart)
//            }
//                .thenAwait(Duration.ofSeconds(29))
//                .expectNextCount(1)
//                .expectComplete()
//                .verify(Duration.ofMillis(1))
//        }
//
//        StepVerifier.withVirtualTime {
//            applyingPromoService.applyTo(cart)
//        }
//            .thenAwait(Duration.ofSeconds(30))
//            .expectNextCount(1)
//            .expectComplete()
//            .verify(Duration.ofMillis(1))
//    }
// }
