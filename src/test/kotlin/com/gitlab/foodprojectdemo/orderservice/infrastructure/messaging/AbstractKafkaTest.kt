package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.KafkaSenderConfig
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.JacksonConfig
import org.junit.jupiter.api.Tag
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy

@SpringBootTest
@ContextHierarchy(
    value = [
        ContextConfiguration(
            name = "kafka-configuration",
            classes = [
                JacksonConfig::class,
                KafkaAutoConfiguration::class,
                TestConsumer::class,
                KafkaSenderConfig::class
            ]
        )
    ]
)
@EnableKafka
@Tag("kafka")
abstract class AbstractKafkaTest : AbstractTest()
