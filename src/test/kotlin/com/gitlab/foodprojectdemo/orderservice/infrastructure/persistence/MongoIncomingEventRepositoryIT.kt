package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener.IncomingEvent
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener.IncomingEventRepository
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import reactor.test.StepVerifier

private class MongoIncomingEventRepositoryIT : AbstractMongoTest() {

    @Autowired
    private lateinit var incomingEventRepository: IncomingEventRepository

    @Test
    fun `should save aggregate events`() {
        val incomingEvent = IncomingEvent(DomainEventId())

        incomingEventRepository.save(incomingEvent)
            .then(incomingEventRepository.existsById(incomingEvent.id))
            .`as`(this::withRollback)
            .`as`(StepVerifier::create)
            .expectNext(true)
            .verifyComplete()
    }
}
