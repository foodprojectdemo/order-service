package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.AbstractGatewayTest
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.ClientErrorResponseException
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource

@AutoConfigureWireMock(port = 0)
@ContextConfiguration(classes = [RestaurantGateway::class])
@TestPropertySource(properties = ["restaurant-service.url=http://localhost:\${wiremock.server.port}/"])
class RestaurantGatewayIT : AbstractGatewayTest() {

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private lateinit var wireMockServer: WireMockServer

    @Autowired
    private lateinit var restaurantGateway: RestaurantGateway

    @Test
    fun findById(dish: Dish) {
        wireMockServer.stubFor(
            WireMock.get(urlPathMatching("/api/v1/dishes/([0-9]+)"))
                .willReturn(
                    WireMock.aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(404)
                )
        )

        assertThatThrownBy {
            restaurantGateway.getDish(dish.id).block()
        }.isInstanceOf(ClientErrorResponseException::class.java)
            .hasMessage("An internal error has occurred. Please try again later.")
    }
}
