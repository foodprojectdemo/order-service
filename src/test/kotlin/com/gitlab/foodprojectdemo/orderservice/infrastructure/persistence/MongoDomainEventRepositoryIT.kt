package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.annotation.PersistenceConstructor
import reactor.test.StepVerifier
import java.time.ZonedDateTime
import java.util.UUID
import java.util.stream.Stream

private class MongoDomainEventRepositoryIT : AbstractMongoTest() {

    @Autowired
    private lateinit var mongoDomainEventRepository: MongoDomainEventRepository

    class TestRootAggregate(id: UUID = UUID.randomUUID()) : RootAggregate<TestRootAggregate, UUID>(id) {
        init {
            addEvent(TestDomainEvent(this))
        }
    }

    class TestDomainEvent : DomainEvent<TestRootAggregate, UUID> {
        @PersistenceConstructor
        private constructor(
            id: DomainEventId,
            type: String,
            aggregate: String,
            aggregateId: UUID,
            occurredOn: ZonedDateTime,
            body: Any?
        ) : super(id, type, aggregate, aggregateId, occurredOn, body)

        constructor(aggregate: TestRootAggregate) : super(aggregate, "test_domain_event")
    }

    @ParameterizedTest
    @MethodSource("aggregatesWithEvents")
    fun `should save aggregate events`(aggregate: RootAggregate<*, *>) {
        assertThat(aggregate.events).isNotEmpty

        mongoDomainEventRepository.deleteAll()
            .thenMany(mongoDomainEventRepository.saveAll(aggregate.events))
            .thenMany(mongoDomainEventRepository.findAll())
            .`as`(this::withRollback)
            .`as`(StepVerifier::create)
            .expectNextSequence(aggregate.events)
            .verifyComplete()
    }

    companion object {
        @JvmStatic
        fun aggregatesWithEvents(): Stream<Arguments> =
            Stream.of(
                Arguments.of(TestRootAggregate()),
                Arguments.of(
                    OrderFixture.order().apply {
                        accept()
                        ready()
                    }
                )
            )
    }
}
