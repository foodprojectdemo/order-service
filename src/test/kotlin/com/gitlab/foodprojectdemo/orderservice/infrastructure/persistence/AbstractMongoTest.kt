package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.mongo.ReactiveMongoConfig
import org.junit.jupiter.api.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.reactive.TransactionalOperator
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Tag("mongo")
@DataMongoTest
@ContextConfiguration(classes = [ReactiveMongoConfig::class])
abstract class AbstractMongoTest : AbstractTest() {
    @Autowired
    protected lateinit var transactionalOperator: TransactionalOperator

    protected fun <T> withRollback(publisher: Flux<T>): Flux<T> =
        transactionalOperator.execute { reactiveTransaction ->
            reactiveTransaction.setRollbackOnly()
            publisher
        }

    protected fun <T> withRollback(publisher: Mono<T>): Mono<T> =
        transactionalOperator.execute { reactiveTransaction ->
            reactiveTransaction.setRollbackOnly()
            publisher
        }.next()
}
