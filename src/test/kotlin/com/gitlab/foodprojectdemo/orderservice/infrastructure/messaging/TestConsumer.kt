package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging

import com.gitlab.foodprojectdemo.orderservice.logger
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
internal class TestConsumer {
    private val log = logger(this)

    private val broker: MutableMap<String, MutableList<ConsumerRecord<*, *>>> = ConcurrentHashMap()

    fun receiveFrom(topic: String): List<ConsumerRecord<*, *>>? =
        broker[topic]

    fun pop(topic: String): ConsumerRecord<*, *>? =
        broker[topic]?.removeLastOrNull()

    @KafkaListener(
        id = "test-listener",
        topicPattern = ".*",
        clientIdPrefix = "\${spring.kafka.client-id}-test"
    )
    fun listener(record: ConsumerRecord<*, *>, ack: Acknowledgment) {
        log.info("received {}", record)

        broker.compute(record.topic()) { _, list ->
            list?.apply {
                add(record)
            } ?: mutableListOf(record)
        }
        ack.acknowledge()
    }
}
