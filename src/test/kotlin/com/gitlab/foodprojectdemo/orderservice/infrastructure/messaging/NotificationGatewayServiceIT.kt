package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging

import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService.EmailNotification
import com.gitlab.foodprojectdemo.orderservice.faker
import org.assertj.core.api.Assertions
import org.awaitility.Awaitility
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(classes = [NotificationServiceGateway::class])
private class NotificationGatewayServiceIT : AbstractKafkaTest() {

    @Autowired
    private lateinit var notificationServiceGateway: NotificationServiceGateway

    @Autowired
    private lateinit var testConsumer: TestConsumer

    @Value("\${kafka.topic.emails-topic}")
    private lateinit var topic: String

    @Test
    fun sendEmailNotification() {
        Assertions.assertThat(testConsumer).isNotNull
        notificationServiceGateway.sendEmailNotification(
            EmailNotification.create(
                faker.lorem().word(), faker.lorem().paragraph(),
                faker.internet().emailAddress()
            )
        ).block()

        Awaitility.await().untilAsserted {
            Assertions.assertThat(testConsumer.receiveFrom(topic)).isNotNull()
        }
    }
}
