package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.WithPreOrder
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

private class MongoCartRepositoryIT : AbstractMongoTest() {

    @Autowired
    private lateinit var mongoCartRepository: MongoCartRepository

    @Test
    fun saveAndFindById(@WithPreOrder cart: Cart) {
        val cartDocument = CartDocument.from(cart)

        val actual = mongoCartRepository.save(cartDocument)
            .then(mongoCartRepository.findById(cart.id))
            .`as`(this::withRollback)
            .block()

        assertThat(actual).isEqualTo(cartDocument)
    }
}
