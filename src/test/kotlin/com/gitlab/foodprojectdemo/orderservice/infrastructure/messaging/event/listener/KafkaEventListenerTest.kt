package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException
import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.faker
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.kafka.support.Acknowledgment
import java.time.ZonedDateTime
import java.util.UUID

private class KafkaEventListenerTest : AbstractTest() {

    class TestDomainEventDto(
        override val id: DomainEventId,
        override val aggregate: String,
        override val aggregateId: UUID,
        override val occurredOn: ZonedDateTime
    ) : DomainEventDto<UUID>

    @Mock
    private lateinit var acknowledgment: Acknowledgment

    @Mock
    private lateinit var objectMapper: ObjectMapper

    @Mock
    private lateinit var eventListener: EventListener

    @InjectMocks
    private lateinit var kafkaEventListener: KafkaEventListener

    @Mock
    private lateinit var testDomainEventDto: TestDomainEventDto

    @Test
    fun `should send event to subscriber`() {
        val json = faker.lorem().characters()

        whenever(objectMapper.readValue(eq(json), any<TypeReference<DomainEventDto<*>>>()))
            .thenReturn(testDomainEventDto)

        kafkaEventListener.onEvent(json, acknowledgment)

        verify(eventListener).onEvent(testDomainEventDto)
        verify(acknowledgment).acknowledge()
    }

    @Test
    fun `should skip unknown event`() {
        val json = faker.lorem().characters()

        whenever(objectMapper.readValue(eq(json), any<TypeReference<DomainEventDto<*>>>()))
            .thenThrow(InvalidTypeIdException::class.java)

        kafkaEventListener.onEvent(json, acknowledgment)

        verify(eventListener, never()).onEvent(any())
        verify(acknowledgment).acknowledge()
    }
}
