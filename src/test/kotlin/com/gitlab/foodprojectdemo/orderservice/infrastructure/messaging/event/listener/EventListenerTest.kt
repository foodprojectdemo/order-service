package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.never
import reactor.kotlin.core.publisher.toMono
import java.time.ZonedDateTime
import java.util.UUID
import java.util.function.Consumer

private class EventListenerTest : AbstractTest() {

    private class TestDomainEventDto(
        override val id: DomainEventId,
        override val aggregate: String,
        override val aggregateId: UUID,
        override val occurredOn: ZonedDateTime
    ) : DomainEventDto<UUID>

    @Mock
    private lateinit var incomingEventRepository: IncomingEventRepository

    @InjectMocks
    private lateinit var eventListener: EventListener

    @Mock
    private lateinit var subscriber: Consumer<TestDomainEventDto>

    @Mock
    private lateinit var testDomainEventDto: TestDomainEventDto

    @BeforeEach
    fun setUp() {
        whenever(testDomainEventDto.id).thenReturn(DomainEventId())
    }

    @Test
    fun `should send event to subscriber`() {
        eventListener.subscribe(TestDomainEventDto::class, subscriber)

        val incomingEvent = IncomingEvent(testDomainEventDto.id)
        whenever(incomingEventRepository.existsById(testDomainEventDto.id)).thenReturn(false.toMono())
        whenever(incomingEventRepository.save(incomingEvent)).thenReturn(incomingEvent.toMono())

        eventListener.onEvent(testDomainEventDto)

        verify(subscriber).accept(testDomainEventDto)
        verify(incomingEventRepository).save(incomingEvent)
    }

    @Test
    fun `should skip processed event`() {
        eventListener.subscribe(TestDomainEventDto::class, subscriber)

        whenever(incomingEventRepository.existsById(testDomainEventDto.id)).thenReturn(true.toMono())

        eventListener.onEvent(testDomainEventDto)

        verify(subscriber, never()).accept(testDomainEventDto)
    }
}
