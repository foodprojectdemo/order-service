package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher

import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.AbstractKafkaTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.TestConsumer
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.test.context.ContextConfiguration
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux

@EnableScheduling
@ContextConfiguration(classes = [DomainEventPublisher::class, ScheduledEventsPublisher::class])
private class ScheduledEventsPublisherIT : AbstractKafkaTest() {

    @Value("\${kafka.topic.events-topic}")
    private lateinit var topic: String

    @Autowired
    private lateinit var testConsumer: TestConsumer

    @MockBean
    private lateinit var domainEventRepository: DomainEventRepository

    @Test
    fun `should publish saved events`(order: Order) {
        order.apply {
            accept()
            ready()
        }

        assertThat(order.events.size).isEqualTo(3)

        whenever(domainEventRepository.findAll()).thenReturn(order.events.toFlux())
        whenever(domainEventRepository.deleteById(any())).thenReturn(Mono.empty())

        Awaitility.await().untilAsserted {
            val consumerRecords = testConsumer.receiveFrom(topic)
            assertThat(consumerRecords?.size).isEqualTo(3)
        }
    }
}
