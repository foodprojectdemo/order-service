package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderRepository
import com.gitlab.foodprojectdemo.orderservice.fixture.OrderFixture.order
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

private class MongoOrderRepositoryIT : AbstractMongoTest() {

    @Autowired
    private lateinit var orderRepository: OrderRepository

    @Test
    fun saveAndFindById(order: Order) {
        orderRepository.save(order).block()
        val actual = orderRepository.findById(order.id).block()

        Assertions.assertThat(actual!!.id).isNotNull
    }

    @Test
    fun findByCustomerId() {
        val order = order()

        orderRepository.save(order).block()

        val actual = orderRepository.findByCustomerId(order.customerId).collectList().block()

        Assertions.assertThat(actual).isNotEmpty
    }

    @Test
    fun findByRestaurantId() {
        val order = order()

        orderRepository.save(order).block()

        val actual = orderRepository.findByRestaurantId(order.restaurantId).collectList().block()

        Assertions.assertThat(actual).isNotEmpty
    }
}
