package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainRepository
import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.SaveAggregateWithEventsAspectIT.TestRootAggregate
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.SaveAggregateWithEventsAspectIT.TestRootAggregateRepository
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.test.context.ContextConfiguration
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier
import java.time.ZonedDateTime
import java.util.UUID

private interface MongoTestRootAggregateRepository :
    ReactiveMongoRepository<TestRootAggregate, UUID>,
    TestRootAggregateRepository

private class SaveAggregateWithEventsAspectIT {

    interface TestRootAggregateRepository : DomainRepository<TestRootAggregate, UUID>

    @Document("test")
    class TestRootAggregate(id: UUID = UUID.randomUUID()) : RootAggregate<TestRootAggregate, UUID>(id) {
        init {
            addEvent(TestDomainEvent(this))
        }
    }

    class TestDomainEvent : DomainEvent<TestRootAggregate, UUID> {
        @PersistenceConstructor
        private constructor(
            id: DomainEventId,
            type: String,
            aggregate: String,
            aggregateId: UUID,
            occurredOn: ZonedDateTime,
            body: Any?
        ) : super(id, type, aggregate, aggregateId, occurredOn, body)

        constructor(aggregate: TestRootAggregate) : super(aggregate, "test_domain_event")
    }

    @Nested
    @ContextConfiguration(classes = [SaveAggregateWithEventsAspect::class])
    @EnableAspectJAutoProxy
    inner class Successful : AbstractMongoTest() {

        @Autowired
        private lateinit var testRootAggregateRepository: TestRootAggregateRepository

        @Autowired
        private lateinit var mongoDomainEventRepository: MongoDomainEventRepository

        @Test
        fun `should save domain events when aggregate are saving`() {
            val testRootAggregate = TestRootAggregate()
            val event = testRootAggregate.events.first()

            testRootAggregateRepository.save(testRootAggregate)
                .thenMany(
                    Flux.concat(
                        testRootAggregateRepository.findById(testRootAggregate.id),
                        mongoDomainEventRepository.findById(event.id)
                    )
                )
                .`as`(this::withRollback)
                .`as`(StepVerifier::create)
                .expectNext(testRootAggregate)
                .expectNext(event)
                .verifyComplete()
        }
    }

    private val myException = RuntimeException("My exception")

    @Nested
    @ContextConfiguration(classes = [SaveAggregateWithEventsAspect::class])
    @EnableAspectJAutoProxy
    inner class FailEventsSaving : AbstractMongoTest() {

        @Autowired
        private lateinit var testRootAggregateRepository: TestRootAggregateRepository

        @MockBean
        private lateinit var mongoDomainEventRepository: MongoDomainEventRepository

        @Test
        fun `should rollback transaction`() {
            val testRootAggregate = TestRootAggregate()

            whenever(mongoDomainEventRepository.saveAll(testRootAggregate.events)).thenReturn(myException.toFlux())

            testRootAggregateRepository.save(testRootAggregate)
                .onErrorResume { Mono.empty() }
                .then(testRootAggregateRepository.findById(testRootAggregate.id))
                .`as`(StepVerifier::create)
                .verifyComplete()
        }
    }

    @TestConfiguration
    class Config {
        val mock: TestRootAggregateRepository = Mockito.mock(TestRootAggregateRepository::class.java)

        @Bean
        fun testRootAggregateRepository() = mock
    }

    @Nested
    @ContextConfiguration(classes = [SaveAggregateWithEventsAspect::class, Config::class])
    @EnableAspectJAutoProxy
    inner class FailAggregateSaving : AbstractMongoTest() {

        @Autowired
        private lateinit var testRootAggregateRepository: TestRootAggregateRepository

        @Autowired
        private lateinit var mongoDomainEventRepository: MongoDomainEventRepository

        @Autowired
        private lateinit var config: Config

        @Test
        fun `should rollback transaction`() {
            val testRootAggregate = TestRootAggregate()
            val event = testRootAggregate.events.first()

            whenever(config.mock.save(testRootAggregate)).thenReturn(myException.toMono())

            testRootAggregateRepository.save(testRootAggregate)
                .onErrorResume { Mono.empty() }
                .thenMany(mongoDomainEventRepository.findById(event.id))
                .`as`(StepVerifier::create)
                .verifyComplete()
        }
    }
}
