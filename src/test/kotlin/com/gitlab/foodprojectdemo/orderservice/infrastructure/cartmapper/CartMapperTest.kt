package com.gitlab.foodprojectdemo.orderservice.infrastructure.cartmapper

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.EmptyCart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.CartDocument
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.MongoCartRepository
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import reactor.kotlin.core.publisher.toMono

private class CartMapperTest : AbstractTest() {

    @Mock
    private lateinit var mongoCartRepository: MongoCartRepository

    @Mock
    private lateinit var dishRepository: DishRepository

    @InjectMocks
    private lateinit var cartMapper: CartMapper

    @Test
    fun `should save and return the same cart`(@EmptyCart cart: Cart, dish: Dish) {
        cart.addDish(dish)
        cart.placePreOrder()
        val cartDocument = CartDocument.from(cart)

        whenever(mongoCartRepository.save(cartDocument)).thenReturn(cartDocument.toMono())
        whenever(dishRepository.getDish(dish.id)).thenReturn(dish.toMono())

        val actual = cartMapper.save(cart).block()

        actual!!.isEqualTo(cart)
    }

    @Test
    fun `should return cart`(@EmptyCart cart: Cart, dish: Dish) {
        cart.addDish(dish)
        cart.placePreOrder()
        val cartDocument = CartDocument.from(cart)

        whenever(mongoCartRepository.findById(cart.id)).thenReturn(cartDocument.toMono())
        whenever(dishRepository.getDish(dish.id)).thenReturn(dish.toMono())

        val actual = cartMapper.findById(cart.id).block()

        actual!!.isEqualTo(cart)
    }

    private fun Cart.isEqualTo(cart: Cart) {
        assertThat(id).isEqualTo(cart.id)
        assertThat(restaurantId).isEqualTo(cart.restaurantId)
        assertThat(items).isEqualTo(cart.items)
        assertThat(discount).isEqualTo(Money.zero)
        assertThat(subTotal).isEqualTo(cart.subTotal)
        assertThat(promoCodeId).isEqualTo(cart.promoCodeId)
        assertThat(preOrder).isEqualTo(cart.preOrder)
    }
}
