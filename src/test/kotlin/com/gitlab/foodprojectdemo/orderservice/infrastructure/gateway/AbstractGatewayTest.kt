package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway

import com.gitlab.foodprojectdemo.orderservice.AbstractTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.WebClientConfig
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@SpringBootTest
@ContextConfiguration(
    classes = [WebClientConfig::class]
)
abstract class AbstractGatewayTest : AbstractTest()
