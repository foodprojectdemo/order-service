package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.AbstractKafkaTest
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.TestConsumer
import com.jayway.jsonpath.JsonPath
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.annotation.PersistenceConstructor
import org.springframework.test.context.ContextConfiguration
import java.time.ZonedDateTime
import java.util.UUID

@ContextConfiguration(classes = [DomainEventPublisher::class])
private class DomainEventPublisherIT : AbstractKafkaTest() {
    @Autowired
    private lateinit var testConsumer: TestConsumer

    @Value("\${kafka.topic.events-topic}")
    private lateinit var topic: String

    @Autowired
    private lateinit var domainEventPublisher: DomainEventPublisher

    class TestRootAggregate(id: UUID = UUID.randomUUID()) : RootAggregate<TestRootAggregate, UUID>(id)

    class TestDomainEvent : DomainEvent<TestRootAggregate, UUID> {
        @PersistenceConstructor
        private constructor(
            id: DomainEventId,
            type: String,
            aggregate: String,
            aggregateId: UUID,
            occurredOn: ZonedDateTime,
            body: Any?
        ) : super(id, type, aggregate, aggregateId, occurredOn, body)

        constructor(aggregate: TestRootAggregate) : super(aggregate, "test_domain_event")
    }

    @Test
    fun `should send domain events to the topic`() {
        val testRootAggregate = TestRootAggregate()
        val testDomainEvent = TestDomainEvent(testRootAggregate)

        domainEventPublisher.publish(testDomainEvent).subscribe()

        Awaitility.await().untilAsserted {
            val consumerRecord = testConsumer.receiveFrom(topic)?.last()
            assertThat(consumerRecord).isNotNull
            val json = JsonPath.parse(consumerRecord!!.value() as String)
            assertThat(json.read("$.aggregateId", String::class.java)).isEqualTo(testDomainEvent.aggregateId.toString())
        }
    }
}
