package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.application.event.EventSubscriber
import com.gitlab.foodprojectdemo.orderservice.componenttest.AbstractComponentTest
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestComponent
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ContextConfiguration
import reactor.kotlin.core.publisher.toMono
import java.time.ZonedDateTime
import java.util.UUID

private class TestEvent1(
    override val id: DomainEventId,
    override val aggregate: String,
    override val aggregateId: UUID,
    override val occurredOn: ZonedDateTime
) : DomainEventDto<UUID>

private class TestEvent2(
    override val id: DomainEventId,
    override val aggregate: String,
    override val aggregateId: UUID,
    override val occurredOn: ZonedDateTime
) : DomainEventDto<UUID>

@TestComponent
private class TestSubscriber {
    var testEvent1: TestEvent1? = null
    var testEvent2: TestEvent2? = null

    @EventSubscriber
    fun onEvent(testEvent1: TestEvent1) {
        this.testEvent1 = testEvent1
    }

    @EventSubscriber
    fun onEvent(testEvent2: TestEvent2) {
        this.testEvent2 = testEvent2
    }
}

@ContextConfiguration(classes = [TestSubscriber::class, AutoSubscription::class, EventListener::class])
private class AutoSubscriptionTest : AbstractComponentTest() {

    @MockBean
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var incomingEventRepository: IncomingEventRepository

    @Autowired
    private lateinit var eventListener: EventListener

    @Autowired
    private lateinit var testSubscriber: TestSubscriber

    @Test
    fun subscribe(@Mock testEvent1: TestEvent1, @Mock testEvent2: TestEvent2) {
        whenever(testEvent1.id).thenReturn(DomainEventId())
        whenever(testEvent2.id).thenReturn(DomainEventId())
        whenever(incomingEventRepository.existsById(any())).thenReturn(false.toMono())
        whenever(incomingEventRepository.save(any())).thenReturn(IncomingEvent(DomainEventId()).toMono())

        eventListener.onEvent(testEvent1)
        eventListener.onEvent(testEvent2)

        assertThat(testSubscriber.testEvent1).isEqualTo(testEvent1)
        assertThat(testSubscriber.testEvent2).isEqualTo(testEvent2)
    }
}
