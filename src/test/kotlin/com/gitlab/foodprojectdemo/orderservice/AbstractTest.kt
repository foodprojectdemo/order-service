package com.gitlab.foodprojectdemo.orderservice

import com.gitlab.foodprojectdemo.orderservice.jupiter.extensions.ExtendWithDomainParameters
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@ExtendWith(MockitoExtension::class)
@ExtendWithDomainParameters
abstract class AbstractTest
