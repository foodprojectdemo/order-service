package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Address
import com.gitlab.foodprojectdemo.orderservice.domain.order.ContactInformation
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.faker
import com.gitlab.foodprojectdemo.orderservice.fixture.CustomerFixture.customerId
import com.gitlab.foodprojectdemo.orderservice.fixture.PreOrderFixture.preOrder
import java.util.UUID

object OrderFixture {

    fun orderId() = OrderId(UUID.randomUUID())

    fun order(customerId: CustomerId = customerId()) = newOrder(customerId)

    fun newOrder(customerId: CustomerId = customerId()) = Order.place(
        preOrder(),
        customerId,
        contactInformation(),
        address()
    )

    fun acceptedOrder(customerId: CustomerId = customerId()) = newOrder(customerId).apply {
        clearEvents()
        accept()
    }

    fun rejectedOrder(customerId: CustomerId = customerId()) = newOrder(customerId).apply {
        clearEvents()
        reject()
    }

    fun readyOrder(customerId: CustomerId = customerId()) = acceptedOrder(customerId).apply {
        clearEvents()
        ready()
    }

    fun address() =
        Address(
            faker.address().streetAddress(),
            faker.address().buildingNumber(),
            faker.number().numberBetween(1, 1000)
        )

    fun contactInformation() = ContactInformation(
        faker.name().fullName(),
        faker.phoneNumber().phoneNumber(),
        faker.internet().emailAddress()
    )
}
