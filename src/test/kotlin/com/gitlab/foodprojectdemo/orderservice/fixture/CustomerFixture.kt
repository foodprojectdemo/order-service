package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import java.util.UUID

object CustomerFixture {
    fun customerId() = CustomerId(UUID.randomUUID())

    fun customer() = Customer()
}
