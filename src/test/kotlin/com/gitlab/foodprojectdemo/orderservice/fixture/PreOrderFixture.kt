package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.fixture.CartFixture.cartWithItems

object PreOrderFixture {

    fun preOrder() = PreOrder(cartWithItems())
}
