package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.faker

private const val MIN_PERCENT = 1
private const val MAX_PERCENT = 100

fun Percent.Companion.random(min: Int = MIN_PERCENT, max: Int = MAX_PERCENT) =
    Percent(faker.number().numberBetween(min, max))

fun Percent.Companion.random(min: Percent, max: Percent) =
    Percent(faker.number().numberBetween(min.toInt(), max.toInt()))
