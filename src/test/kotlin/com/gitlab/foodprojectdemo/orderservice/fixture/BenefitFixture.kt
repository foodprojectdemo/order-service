package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.promo.Benefit
import com.gitlab.foodprojectdemo.orderservice.domain.promo.Gift
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import kotlin.random.Random

object BenefitFixture {
    fun benefit(): Benefit = when (Random.nextInt(1, 3)) {
        1 -> absoluteDiscount()
        2 -> percentDiscount()
        3 -> gift()
        else -> absoluteDiscount()
    }

    fun absoluteDiscount() = Money.random().toBenefit()

    fun percentDiscount() = Percent.random().toBenefit()

    fun gift() = Gift(dish(), Quantity.random())
}
