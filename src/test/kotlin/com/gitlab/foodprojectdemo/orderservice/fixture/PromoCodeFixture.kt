package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.Gift
import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftPromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PercentDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import java.util.UUID

object PromoCodeFixture {

    fun promoCodeId() = PromoCodeId(UUID.randomUUID().toString())

    fun promoCode(): PromoCode =
        listOf(absoluteDiscountPromoCode(), percentDiscountPromoCode(), giftPromoCode()).random()

    fun absoluteDiscountPromoCode(
        restaurantId: RestaurantId = restaurantId(),
        discount: Money = Money.random(),
        promoCodeId: PromoCodeId = promoCodeId()
    ) = AbsoluteDiscountPromoCode(
        promoCodeId,
        restaurantId,
        discount.toBenefit()
    )

    fun percentDiscountPromoCode(
        restaurantId: RestaurantId = restaurantId(),
        percent: Percent = Percent.random(),
        promoCodeId: PromoCodeId = promoCodeId()
    ) = PercentDiscountPromoCode(
        promoCodeId,
        restaurantId,
        percent.toBenefit()
    )

    fun giftPromoCode(
        restaurantId: RestaurantId = restaurantId(),
        dish: Dish = dish(),
        promoCodeId: PromoCodeId = promoCodeId()
    ) = GiftPromoCode(
        promoCodeId,
        restaurantId,
        Gift(dish, Quantity.one)
    )
}
