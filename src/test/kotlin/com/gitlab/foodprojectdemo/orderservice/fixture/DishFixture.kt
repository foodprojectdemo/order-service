package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.faker
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId

object DishFixture {

    fun dishId() = DishId(faker.number().numberBetween(1L, 1000L))

    fun dish(
        restaurantId: RestaurantId = restaurantId(),
        price: Money = Money.random(),
        name: String = faker.food().dish(),
        dishId: DishId = dishId()
    ) = Dish(dishId, restaurantId, name, price)
}
