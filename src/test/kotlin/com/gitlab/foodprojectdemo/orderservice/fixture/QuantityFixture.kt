package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.faker

private const val MIN_QUANTITY = 1
private const val MAX_QUANTITY = 100

fun Quantity.Companion.random() = Quantity(faker.number().numberBetween(MIN_QUANTITY, MAX_QUANTITY))

fun Quantity.Companion.random(min: Int): Quantity {
    assert(MAX_QUANTITY >= min)
    return Quantity(faker.number().numberBetween(min, MAX_QUANTITY))
}

fun Quantity.Companion.random(min: Int, max: Int) = Quantity(faker.number().numberBetween(min, max))

fun Quantity.randomLessThanThis() = Quantity.random(0, toInt() - 1)

val Quantity.Companion.two: Quantity
    get() = Quantity(2)

val Quantity.Companion.three: Quantity
    get() = Quantity(3)
