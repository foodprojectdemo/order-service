package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartItem
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dishId
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId
import java.util.UUID

object CartFixture {

    fun cartId() = CartId(UUID.randomUUID())

    fun emptyCart(cartId: CartId = cartId()) = Cart(cartId)

    fun cartWithItems(cartId: CartId = cartId()) = emptyCart(cartId).apply {
        val restaurantId = restaurantId()
        SeveralItemFactory.items { dish(restaurantId = restaurantId) }
            .forEach { this.addDish(it, Quantity.random()) }
        SeveralItemFactory.items { dish(restaurantId = restaurantId) }
            .forEach { this.addGift(it, Quantity.random()) }
    }

    fun cartItem(
        restaurantId: RestaurantId = restaurantId(),
        dishId: DishId = dishId(),
        quantity: Quantity = Quantity.random()
    ) = CartItem(dish(restaurantId = restaurantId, dishId = dishId), quantity)
}
