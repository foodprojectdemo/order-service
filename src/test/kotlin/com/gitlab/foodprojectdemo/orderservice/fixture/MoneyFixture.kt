package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.faker

private const val MIN_MONEY = 1L
private const val MAX_MONEY = 1000L

fun Money.Companion.random(min: Long = MIN_MONEY, max: Long = MAX_MONEY) = Money(faker.number().numberBetween(min, max))
fun Money.Companion.random(min: Money, max: Money) = Money(faker.number().numberBetween(min.toLong(), max.toLong()))
fun Money.randomLessThanThis() = Money.random(MIN_MONEY, toLong() - 1)
fun Money.randomMoreThanThis() = Money.random(toLong() + 1, toLong() + MAX_MONEY)

fun Money.Companion.randomEven(from: Long, until: Long): Money {
    var even: Long
    do {
        even = faker.number().numberBetween(from, until)
    } while ((even % 2) != 0L)
    return Money(even)
}
