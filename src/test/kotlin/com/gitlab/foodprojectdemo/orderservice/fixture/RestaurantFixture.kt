package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.faker

object RestaurantFixture {
    fun restaurantId() = RestaurantId(faker.number().numberBetween(1L, 1000L))
}
