package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity.Companion.one
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscount
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.ByCartAmountCondition
import com.gitlab.foodprojectdemo.orderservice.domain.promo.Gift
import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PercentDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionId
import com.gitlab.foodprojectdemo.orderservice.fixture.DishFixture.dish
import com.gitlab.foodprojectdemo.orderservice.fixture.IdFixture.id
import com.gitlab.foodprojectdemo.orderservice.fixture.RestaurantFixture.restaurantId
import com.gitlab.foodprojectdemo.orderservice.toBenefit

object PromoActionFixture {

    fun promoActionId() = PromoActionId(id())

    fun absoluteDiscountByCartAmountPromoAction(
        restaurantId: RestaurantId = restaurantId(),
        cartAmount: Money = Money.random(),
        discount: Money = Money.random(),
        promoActionId: PromoActionId = promoActionId()
    ) = AbsoluteDiscountByCartAmountPromoAction(
        promoActionId,
        restaurantId,
        listOf(ByCartAmountCondition(cartAmount, discount.toBenefit()))
    )

    fun absoluteDiscountByCartAmountPromoAction(
        restaurantId: RestaurantId = restaurantId(),
        discountMap: Map<Money, Money>,
        promoActionId: PromoActionId = promoActionId()
    ) = AbsoluteDiscountByCartAmountPromoAction(
        promoActionId,
        restaurantId,
        discountMap.entries.map { ByCartAmountCondition(it.key, it.value.toBenefit()) }.toList()
    )

    fun absoluteDiscountByCartAmountPromoAction(
        restaurantId: RestaurantId = restaurantId(),
        discountMap: Map<Money, AbsoluteDiscount>
    ) = AbsoluteDiscountByCartAmountPromoAction(
        promoActionId(),
        restaurantId,
        discountMap.entries.map { ByCartAmountCondition(it.key, it.value) }.toList()
    )

    fun percentDiscountByCartAmountPromoAction(
        restaurantId: RestaurantId = restaurantId(),
        cartAmount: Money = Money.random(),
        percent: Percent = Percent.random(),
        promoActionId: PromoActionId = promoActionId()
    ) = PercentDiscountByCartAmountPromoAction(
        promoActionId,
        restaurantId,
        listOf(ByCartAmountCondition(cartAmount, percent.toBenefit()))
    )

    fun giftByCartAmountPromoAction(
        restaurantId: RestaurantId = restaurantId(),
        cartAmount: Money = Money.random(),
        dish: Dish = dish(),
        quantity: Quantity = one,
        promoActionId: PromoActionId = promoActionId()
    ) = GiftByCartAmountPromoAction(
        promoActionId,
        restaurantId,
        listOf(ByCartAmountCondition(cartAmount, Gift(dish, quantity)))
    )
}
