package com.gitlab.foodprojectdemo.orderservice.fixture

import com.gitlab.foodprojectdemo.orderservice.faker

object IdFixture {
    fun id() = faker.number().numberBetween(1L, 1000L)
}
