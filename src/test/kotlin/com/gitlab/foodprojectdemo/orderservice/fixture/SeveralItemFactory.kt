package com.gitlab.foodprojectdemo.orderservice.fixture

import kotlin.random.Random

object SeveralItemFactory {
    private const val MAX = 5

    fun <T : Any> items(number: Int = number(), supplier: () -> T): Collection<T> =
        (1..number).map { supplier() }

    private fun number(max: Int = MAX) = Random.nextInt(1, max)
}
