package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Remove promo code"
    request {
        method DELETE()
        url $(regex("/api/v1/carts/${uuid()}/promo-code"))
    }
    response {
        status NO_CONTENT()
    }
}
