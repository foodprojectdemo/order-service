package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract

final MONEY_PATTERN = "\\\$[0-9]+"


Contract.make {
    description "Get cart by id"
    request {
        method GET()
        url $(regex("/api/v1/carts/${uuid()}"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                id          : $(anyUuid()),
                items       : [
                        [
                                dishId     : $(anyPositiveInt()),
                                price      : $(regex(MONEY_PATTERN)),
                                totalAmount: $(regex(MONEY_PATTERN)),
                                quantity   : $(anyPositiveInt())
                        ]
                ],
                gifts       : [
                        [
                                dishId     : $(anyPositiveInt()),
                                price      : $(regex(MONEY_PATTERN)),
                                totalAmount: $(regex(MONEY_PATTERN)),
                                quantity   : $(anyPositiveInt())
                        ]
                ],
                total       : $(regex(MONEY_PATTERN)),
                subTotal    : $(regex(MONEY_PATTERN)),
                discount    : $(regex(MONEY_PATTERN)),
        ])
    }
}
