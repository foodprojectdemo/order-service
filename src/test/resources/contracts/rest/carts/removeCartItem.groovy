package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Change cart item quantity"
    request {
        method DELETE()
        url $(regex("/api/v1/carts/${uuid()}/items/${positiveInt()}"))
    }
    response {
        status NO_CONTENT()
    }
}
