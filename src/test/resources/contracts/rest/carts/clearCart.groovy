package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Add a dish to the cart"
    request {
        method DELETE()
        url $(regex("/api/v1/carts/${uuid()}/items"))
    }
    response {
        status NO_CONTENT()
    }
}