package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "create cart"
    request {
        method POST()
        url "/api/v1/carts"
    }
    response {
        status CREATED()
        headers {
            contentType(applicationJson())
        }
        body([
                id: $(anyUuid())
        ])
    }
}