package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Add a dish to the cart"
    request {
        method POST()
        url $(regex("/api/v1/carts/${uuid()}/items"))
        headers {
            contentType(applicationJson())
        }
        body(
                dishId: $(anyPositiveInt()),
                quantity: $(anyPositiveInt())
        )
    }
    response {
        status CREATED()
    }
}
