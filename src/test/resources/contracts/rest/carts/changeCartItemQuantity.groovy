package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Change cart item quantity"
    request {
        method PATCH()
        url $(regex("/api/v1/carts/${uuid()}/items/${positiveInt()}"))
        headers {
            contentType(applicationJson())
        }
        body(
                quantity: $(anyPositiveInt())
        )
    }
    response {
        status NO_CONTENT()
    }
}
