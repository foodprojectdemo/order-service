package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Place pre order"
    request {
        method POST()
        url $(regex("/api/v1/carts/${uuid()}/pre-order"))
    }
    response {
        status CREATED()
    }
}