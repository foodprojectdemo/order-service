package contracts.rest.carts

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Apply Promo Code to the cart"
    request {
        method PUT()
        url $(regex("/api/v1/carts/${uuid()}/promo-code"))
        headers {
            contentType(applicationJson())
        }
        body(
                promoCodeId: $(anyNonBlankString()),
        )
    }
    response {
        status NO_CONTENT()
    }
}
