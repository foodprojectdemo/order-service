package contracts.rest.orders

import org.springframework.cloud.contract.spec.Contract

final MONEY_PATTERN = "\\\$[0-9]+"

Contract.make {
    description "should return restaurant`s orders"
    request {
        method GET()
        url $(regex("/api/v1/restaurant/${positiveInt()}/orders"))
    }
    response {
        status OK()
        body([
                [
                        id                : $(anyUuid()),
                        customerId        : $(anyUuid()),
                        restaurantId      : $(anyPositiveInt()),
                        status            : anyOf("NEW", "PAID", "DELIVERY"),
                        address           : [
                                street: $(c("1692  Sigley Road"), p(anyNonEmptyString())),
                                house : $(anyNonEmptyString()),
                                flat  : $(anyPositiveInt())
                        ],
                        contactInformation: [
                                name : $(anyNonEmptyString()),
                                phone: $(c("785-766-5255"), p(anyNonEmptyString())),
                                email: $(regex(email())),
                        ],
                        items             : [[
                                                     dishId     : $(anyPositiveInt()),
                                                     name       : $(anyNonEmptyString()),
                                                     price      : $(regex(MONEY_PATTERN)),
                                                     quantity   : $(anyPositiveInt()),
                                                     totalAmount: $(anyPositiveInt())
                                             ]],
                        subTotal          : $(c("\$1000"), p(regex(MONEY_PATTERN))),
                        discount          : $(c("\$200"), p(regex(MONEY_PATTERN))),
                        total             : $(c("\$800"), p(regex(MONEY_PATTERN))),
                ]
        ])
        headers {
            contentType(applicationJson())
        }
    }
}