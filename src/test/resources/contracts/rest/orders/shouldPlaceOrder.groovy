package contracts.rest.orders

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Order checkout"
    request {
        method POST()
        url "/api/v1/orders"
        headers {
            contentType(applicationJson())
        }
        body(
                customerId: $(anyUuid()),
                cartId: $(anyUuid()),
                paymentMethodId: $(anyUuid()),
                shippingMethodId: $(anyUuid()),
                shippingAddressStreet: $(anyNonBlankString()),
                shippingAddressHouse: $(anyNonBlankString()),
                shippingAddressFlat: $(anyPositiveInt()),
                contactInformationName: $(anyNonBlankString()),
                contactInformationPhone: $(anyNonBlankString()),
                contactInformationEmail: $(anyEmail())
        )
    }
    response {
        status CREATED()
        headers {
            contentType(applicationJson())
        }
        body([
                id: $(anyUuid())
        ])
    }
}