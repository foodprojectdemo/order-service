package contracts.rest.customers

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Sign up as a new customer"
    request {
        method POST()
        url "/api/v1/customers"
    }
    response {
        status CREATED()
        headers {
            contentType(applicationJson())
        }
        body([
id : $(anyUuid())
        ])
    }
}