package contracts.rest.customers

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Get customer by id"
    request {
        method GET()
        url $(regex("/api/v1/customers/${uuid()}"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                id                : $(anyUuid()),
//                contactInformation: [
//                        name : $(anyNonBlankString()),
//                        phone: $(anyNonBlankString()),
//                        email: $(anyEmail())
//                ],
//                address           : [
//                        street: $(anyNonBlankString()),
//                        house : $(anyNonBlankString()),
//                        flat  : $(anyPositiveInt())
//                ]
        ])
    }
}
