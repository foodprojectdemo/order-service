package contracts.messaging.events

import org.springframework.cloud.contract.spec.Contract

import java.time.ZonedDateTime


Contract.make {
    label "OrderPlacedEvent"
    input {
        triggeredBy("orderPlacedEvent()")
    }
    outputMessage {
        sentTo "events.v1"
        body([
                id         : $(uuid()),
                type       : "order_placed",
                aggregate  : "order",
                aggregateId: $(uuid()),
                occurredOn : $(p(iso8601WithOffset()), c(ZonedDateTime.now()))
        ])
    }
}
