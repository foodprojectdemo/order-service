package com.gitlab.foodprojectdemo.orderservice

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscount
import com.gitlab.foodprojectdemo.orderservice.domain.promo.Gift
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PercentDiscount
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.IdResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.util.UriComponentsBuilder

inline fun <reified T> logger(from: T): Logger =
    LoggerFactory.getLogger(from!!::class.java)

fun Long.toMoney() = Money(this)
fun Int.toMoney() = Money(this.toLong())
fun Money.toBenefit() = AbsoluteDiscount(this)

fun Int.toQuantity() = Quantity(this)
fun Percent.toBenefit() = PercentDiscount(this)
fun Dish.toBenefit() = Gift(this, Quantity.one)

fun <T> createdResponseEntity(serverHttpRequest: ServerHttpRequest, id: T) =
    ResponseEntity.created(
        UriComponentsBuilder.fromHttpRequest(serverHttpRequest).path("/{id}")
            .build(id)
    ).body(IdResponse(id))
