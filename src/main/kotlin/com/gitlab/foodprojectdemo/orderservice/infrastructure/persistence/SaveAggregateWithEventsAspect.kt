package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component
import org.springframework.transaction.reactive.TransactionalOperator
import reactor.core.publisher.Mono

/**
 * Saves domain events when aggregate is saving
 */
@Aspect
@Component
class SaveAggregateWithEventsAspect(
    private val mongoDomainEventRepository: MongoDomainEventRepository,
    private val transactionalOperator: TransactionalOperator
) {

    @Pointcut("execution(reactor.core.publisher.Mono com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainRepository.save(..))")
    private fun domainRepositorySaveMethod() {}

    @Pointcut("args(com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate)")
    private fun withArgs() {}

    /**
     * @see com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainRepository.save pointcut
     */
    @Around("domainRepositorySaveMethod() && withArgs()")
    fun save(proceedingJoinPoint: ProceedingJoinPoint): Any {
        val result = (proceedingJoinPoint.proceed() as Mono<*>)
        val aggregate = proceedingJoinPoint.args.first() as RootAggregate<*, *>

        return transactionalOperator.transactional(
            mongoDomainEventRepository
                .saveAll(aggregate.events)
                .then(result)
        )
    }
}
