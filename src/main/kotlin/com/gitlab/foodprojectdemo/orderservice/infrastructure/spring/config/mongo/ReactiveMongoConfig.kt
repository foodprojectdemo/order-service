package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.mongo

import com.mongodb.MongoClientSettings.Builder
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.boot.autoconfigure.mongo.MongoPropertiesClientSettingsBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory
import org.springframework.data.mongodb.ReactiveMongoTransactionManager
import org.springframework.data.mongodb.core.convert.MongoCustomConversions
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.reactive.TransactionalOperator
import java.util.concurrent.TimeUnit

@Configuration
@ComponentScan
@EnableReactiveMongoRepositories(basePackages = ["com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence"])
internal class ReactiveMongoConfig {

    @Bean
    fun mongoTimeoutCustomizer(
        properties: MongoProperties,
        environment: Environment
    ) = object : MongoPropertiesClientSettingsBuilderCustomizer(properties, environment) {
        override fun customize(settingsBuilder: Builder) {
            settingsBuilder
                .applyToClusterSettings { builder ->
                    builder.serverSelectionTimeout(5000, TimeUnit.MILLISECONDS)
                }
        }
    }

    @Bean
    fun mongoCustomConversions(converter: List<Converter<*, *>>) = MongoCustomConversions(converter)

    @Bean
    fun reactiveTransactionManager(reactiveMongoDatabaseFactory: ReactiveMongoDatabaseFactory) =
        ReactiveMongoTransactionManager(reactiveMongoDatabaseFactory)

    @Bean
    fun transactionalOperator(reactiveTransactionManager: ReactiveTransactionManager) =
        TransactionalOperator.create(reactiveTransactionManager)
//        TransactionalOperator.create(reactiveTransactionManager, object : TransactionDefinition {
//            override fun getName() = "general"
//            override fun getIsolationLevel() = TransactionDefinition.ISOLATION_REPEATABLE_READ
//        })
}
