package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

internal interface MongoCartRepository : ReactiveMongoRepository<CartDocument, CartId>
