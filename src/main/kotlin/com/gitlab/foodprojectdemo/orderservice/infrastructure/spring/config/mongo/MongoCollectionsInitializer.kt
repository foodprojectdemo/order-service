package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.mongo

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener.IncomingEvent
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.CartDocument
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.stereotype.Component
import reactor.kotlin.core.publisher.toFlux
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

@Component
internal class MongoCollectionsInitializer(private val reactiveMongoTemplate: ReactiveMongoTemplate) {

    @Suppress("LeakingThis")
    private val log = logger(this)

    @PostConstruct
    fun createCollections() {
        listOf(CartDocument::class, Order::class, DomainEvent::class, IncomingEvent::class).toFlux()
            .flatMap(::createIfNotExist)
            .collectList()
            .block()
    }

    fun createIfNotExist(aClass: KClass<*>) =
        reactiveMongoTemplate.collectionExists(aClass.java)
            .filter { it == false }
            .flatMap { reactiveMongoTemplate.createCollection(aClass.java) }
            .doOnNext {
                log.info("Created collection : {}", it.namespace)
            }
}
