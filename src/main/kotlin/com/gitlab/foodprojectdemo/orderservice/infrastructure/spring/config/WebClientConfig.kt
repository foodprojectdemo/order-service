package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.JacksonConfig
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.WebClientErrorHandlerAutoConfiguration
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.web.reactive.function.client.ExchangeStrategies

@Configuration
@Import(value = [JacksonConfig::class, WebClientAutoConfiguration::class, WebClientErrorHandlerAutoConfiguration::class])
class WebClientConfig {

    @Bean
    fun webClientCustomizer(exchangeStrategies: ExchangeStrategies) = WebClientCustomizer {
        it.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .exchangeStrategies(exchangeStrategies)
    }

    @Bean
    fun exchangeStrategies(objectMapper: ObjectMapper) =
        ExchangeStrategies
            .builder()
            .codecs { clientDefaultCodecsConfigurer ->
                clientDefaultCodecsConfigurer.defaultCodecs()
                    .jackson2JsonEncoder(Jackson2JsonEncoder(objectMapper, MediaType.APPLICATION_JSON))
                clientDefaultCodecsConfigurer.defaultCodecs()
                    .jackson2JsonDecoder(Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON))
            }
            .build()
}
