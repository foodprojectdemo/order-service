package com.gitlab.foodprojectdemo.orderservice.infrastructure.cartmapper

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.CartDocument
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.CartItemDocument
import com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence.MongoCartRepository
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.stereotype.Component
import org.springframework.util.ReflectionUtils
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import reactor.kotlin.core.publisher.toFlux

@Component
internal class CartMapper(
    private val mongoCartRepository: MongoCartRepository,
    private val dishRepository: DishRepository
) : CartRepository {

    @Suppress("LeakingThis")
    private val log = logger(this)

    override fun save(aggregate: Cart): Mono<Cart> =
        mongoCartRepository.save(CartDocument.from(aggregate)).flatMap(::toCart)

    override fun findById(id: CartId): Mono<Cart> =
        mongoCartRepository.findById(id)
            .flatMap(::toCart)

    private fun toCart(cartDocument: CartDocument): Mono<Cart> =
        cartDocument.items.toFlux()
            .distinct()
            .parallel()
            .runOn(Schedulers.boundedElastic())
            .map(CartItemDocument::dishId)
            .flatMap(dishRepository::getDish)
            .sequential()
            .collectMap(Dish::id)
            .map { dishMap -> createCart(cartDocument, dishMap) }
            .doOnError { log.error(it.message, it) }
            .onErrorResume { Mono.empty() }

    private fun createCart(cartDocument: CartDocument, dishMap: Map<DishId, Dish>) =
        Cart(cartDocument.id).apply {
            cartDocument.items
                .map { (dishId, quantity) -> dishId.toDishByMap(dishMap) to quantity }
                .filter { (dish, _) -> dish != null }
                .forEach { (dish, quantity) ->
                    addDish(dish!!, quantity)
                }

            cartDocument.promoCodeId?.let { promoCodeId ->
                applyPromoCode(promoCodeId)
            }

            setPreOrder(this, cartDocument.preOrder)
        }

    private fun setPreOrder(cart: Cart, preOrder: PreOrder?) {
        Cart::class.java.getDeclaredField("preOrder").let {
            ReflectionUtils.makeAccessible(it)
            ReflectionUtils.setField(it, cart, preOrder)
        }
    }

    private fun DishId.toDishByMap(dishMap: Map<DishId, Dish>): Dish? {
        val dish = dishMap[this]
        dish ?: log.error("DishId=$this is absent in map $dishMap")
        return dish
    }
}
