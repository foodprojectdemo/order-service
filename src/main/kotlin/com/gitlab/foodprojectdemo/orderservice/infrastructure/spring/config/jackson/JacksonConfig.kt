package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.NumberSerializer
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.CartIdDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.DishIdDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.DomainEventIdJsonDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.PercentDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.QuantityDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer.RestaurantIdDeserializer
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.serializer.DishIdSerializer
import org.springframework.boot.jackson.JsonComponent
import org.springframework.context.annotation.Bean

@JsonComponent
class JacksonConfig {
    @Bean
    fun objectMapper(): ObjectMapper =
        ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false)
            .registerModule(JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .registerModule(
                SimpleModule()
                    .addSerializer(DishId::class.java, DishIdSerializer())
                    .addSerializer(CustomerId::class.java, ToStringSerializer())
                    .addSerializer(OrderId::class.java, ToStringSerializer())
                    .addSerializer(PromoActionId::class.java, ToStringSerializer())
                    .addSerializer(PromoCodeId::class.java, ToStringSerializer())
                    .addSerializer(RestaurantId::class.java, ToStringSerializer())
                    .addSerializer(CartId::class.java, ToStringSerializer())
                    .addSerializer(Money::class.java, ToStringSerializer())
                    .addSerializer(Quantity::class.java, NumberSerializer(Int::class.java))
                    .addSerializer(Percent::class.java, ToStringSerializer())
                    .addDeserializer(DishId::class.java, DishIdDeserializer())
                    .addDeserializer(Quantity::class.java, QuantityDeserializer())
                    .addDeserializer(Percent::class.java, PercentDeserializer())
                    .addDeserializer(RestaurantId::class.java, RestaurantIdDeserializer())
                    .addDeserializer(CartId::class.java, CartIdDeserializer())
                    .addSerializer(DomainEventId::class.java, ToStringSerializer())
                    .addDeserializer(DomainEventId::class.java, DomainEventIdJsonDeserializer())
            )
            .registerModule(KotlinModule())
}
