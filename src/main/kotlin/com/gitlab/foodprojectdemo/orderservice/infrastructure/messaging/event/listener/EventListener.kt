package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer
import kotlin.reflect.KClass

@Component
class EventListener(
    private val incomingEventRepository: IncomingEventRepository
) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    private val subscription = ConcurrentHashMap<
        KClass<DomainEventDto<*>>,
        MutableCollection<Consumer<DomainEventDto<*>>>
        >()

    fun onEvent(event: DomainEventDto<*>) {
        val isExist = incomingEventRepository.existsById(event.id).block()

        if (isExist == true) {
            log.info("Event has already been processed {}", event)
            return
        }

        log.info("Received event: $event")
        subscription[event::class]?.forEach { consumer ->
            try {
                consumer.accept(event)
            } catch (throwable: Throwable) {
                log.error(throwable.message, throwable)
            }
        }

        incomingEventRepository.save(IncomingEvent(event.id)).block()
    }

    fun <E : DomainEventDto<*>> subscribe(event: KClass<E>, subscriber: Consumer<E>) {
        @Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
        val subscriber = subscriber as Consumer<DomainEventDto<*>>
        @Suppress("UNCHECKED_CAST")
        subscription.compute(event as KClass<DomainEventDto<*>>) { _, consumers ->
            consumers?.apply {
                add(subscriber)
            } ?: mutableListOf(subscriber)
        }
    }
}
