package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartItem
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

internal data class CartItemDocument(val dishId: DishId, val quantity: Quantity)

@Document("cart")
internal data class CartDocument(
    @Id
    val id: CartId,
    val items: List<CartItemDocument>,
    val promoCodeId: PromoCodeId?,
    val preOrder: PreOrder?
) {
    companion object {
        fun from(cart: Cart) = CartDocument(
            cart.id,
            cart.items.map(::toCartItemDocument),
            cart.promoCodeId,
            cart.preOrder
        )

        private fun toCartItemDocument(cartItem: CartItem) = CartItemDocument(cartItem.dish.id, cartItem.quantity)
    }
}
