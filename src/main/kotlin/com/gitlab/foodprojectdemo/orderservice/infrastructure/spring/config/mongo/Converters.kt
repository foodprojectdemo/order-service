package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.mongo

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.toMoney
import com.gitlab.foodprojectdemo.orderservice.toQuantity
import org.springframework.core.convert.converter.Converter
import org.springframework.data.convert.ReadingConverter
import org.springframework.data.convert.WritingConverter
import org.springframework.stereotype.Component
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Date
import java.util.UUID

@Component
@WritingConverter
class CartIdWritingConverter : Converter<CartId, UUID> {
    override fun convert(id: CartId): UUID {
        return UUID.fromString(id.toString())!!
    }
}

@Component
@WritingConverter
class OrderIdWritingConverter : Converter<OrderId, UUID> {
    override fun convert(id: OrderId) = UUID.fromString(id.toString())!!
}

@Component
@WritingConverter
class DomainEventIdWritingConverter : Converter<DomainEventId, UUID> {
    override fun convert(id: DomainEventId) = UUID.fromString(id.toString())!!
}

@Component
@WritingConverter
class DishIdWritingConverter : Converter<DishId, Long> {
    override fun convert(id: DishId) = id.toString().toLong()
}

@Component
@ReadingConverter
class DishIdReadingConverter : Converter<Long, DishId> {
    override fun convert(id: Long) = DishId(id)
}

@Component
@WritingConverter
class RestaurantIdWritingConverter : Converter<RestaurantId, Long> {
    override fun convert(id: RestaurantId) = id.toString().toLong()
}

@Component
@ReadingConverter
class RestaurantIdReadingConverter : Converter<Long, RestaurantId> {
    override fun convert(id: Long) = RestaurantId(id)
}

@Component
@WritingConverter
class CustomerIdWritingConverter : Converter<CustomerId, UUID> {
    override fun convert(id: CustomerId) = UUID.fromString(id.toString())!!
}

@Component
@ReadingConverter
class CustomerIdReadingConverter : Converter<UUID, CustomerId> {
    override fun convert(id: UUID) = CustomerId(id)
}

@Component
@ReadingConverter
class MoneyReadingConverter : Converter<Long, Money> {
    override fun convert(p0: Long) = if (p0 > 0) p0.toMoney() else Money.zero
}

@Component
@ReadingConverter
class QuantityReadingConverter : Converter<Int, Quantity> {
    override fun convert(p0: Int) = p0.toQuantity()
}

@Component
@WritingConverter
class MoneyWritingConverter : Converter<Money, Long> {
    override fun convert(money: Money) = money.toLong()
}

@Component
@WritingConverter
class QuantityWritingConverter : Converter<Quantity, Int> {
    override fun convert(quantity: Quantity) = quantity.toInt()
}

@Component
@WritingConverter
class ZonedDateTimeWritingConverter : Converter<ZonedDateTime, Date> {
    override fun convert(zonedDateTime: ZonedDateTime): Date? = Date.from(zonedDateTime.toInstant())
}

@Component
@ReadingConverter
class ZonedDateTimeReadingConverter : Converter<Date, ZonedDateTime> {
    override fun convert(date: Date): ZonedDateTime? = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
}
