package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AllGiftsPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.DiscountPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.MaxDiscountPromoActionStrategy
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionStrategyResolver
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Component
class StubPromoActionStrategyResolver : PromoActionStrategyResolver {
    override fun discountPromoActionStrategy(restaurantId: RestaurantId): Mono<DiscountPromoActionStrategy> =
        MaxDiscountPromoActionStrategy.toMono()

    override fun giftPromoActionStrategy(restaurantId: RestaurantId): Mono<GiftPromoActionStrategy> =
        AllGiftsPromoActionStrategy.toMono()
}
