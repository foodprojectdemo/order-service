package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import org.springframework.boot.jackson.JsonComponent
import java.util.UUID

@JsonComponent
class DomainEventIdJsonDeserializer : JsonDeserializer<DomainEventId>() {
    override fun deserialize(parser: JsonParser, context: DeserializationContext): DomainEventId {
        val node = parser.codec.readTree<JsonNode>(parser)
        val id = node.asText()
        try {
            return DomainEventId(UUID.fromString(id))
        } catch (e: IllegalArgumentException) {
            throw InvalidValueBusinessLogicException("Invalid domainEventId: $id")
        }
    }
}
