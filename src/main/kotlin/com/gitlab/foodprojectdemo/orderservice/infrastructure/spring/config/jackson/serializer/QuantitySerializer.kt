package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

class QuantitySerializer : StdSerializer<Quantity>(Quantity::class.java) {

    override fun serialize(quantity: Quantity, jsonGenerator: JsonGenerator, provider: SerializerProvider) {
        jsonGenerator.writeNumber(quantity.toString().toInt())
    }
}
