package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions

@Configuration
class KafkaSenderConfig {
    @Bean
    fun kafkaSender(properties: KafkaProperties): KafkaSender<String, String> =
        KafkaSender.create(SenderOptions.create(properties.buildProducerProperties()))
}
