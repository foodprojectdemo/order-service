package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException
import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.stereotype.Component

@Component
class KafkaEventListener(
    private val objectMapper: ObjectMapper,
    private val eventListener: EventListener
) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @KafkaListener(
        groupId = "order-event-listener",
        topics = ["\${kafka.topic.events-topic}"]
    )
    fun onEvent(json: String, ack: Acknowledgment) {
        log.info("received message: $json")

        parse(json)?.let(eventListener::onEvent)

        ack.acknowledge()
    }

    private fun parse(json: String): DomainEventDto<*>? {
        return try {
            val type = object : TypeReference<DomainEventDto<*>>() {}
            objectMapper.readValue(json, type)
        } catch (throwable: JsonProcessingException) {
            if (throwable is InvalidTypeIdException) {
                log.info("Skip unknown message {}", json)
            } else {
                log.error(throwable.message, throwable)
            }
            null
        }
    }
}
