package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId

class DishIdSerializer : StdSerializer<DishId>(DishId::class.java) {

    override fun serialize(dishId: DishId, jsonGenerator: JsonGenerator, provider: SerializerProvider) {
        jsonGenerator.writeNumber(dishId.toString().toLong())
    }
}
