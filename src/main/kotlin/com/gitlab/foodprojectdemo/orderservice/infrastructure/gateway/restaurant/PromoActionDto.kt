package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoActionId

@JsonTypeInfo(use = NAME, include = PROPERTY, property = "type")
@JsonSubTypes(
    Type(AbsoluteDiscountPromoActionDto::class, name = "absolute_promo_action"),
    Type(PercentDiscountPromoActionDto::class, name = "percent_promo_action"),
    Type(GiftByCartAmountPromoActionDto::class, name = "gift_promo_action"),
)
internal sealed class PromoActionDto {
    abstract val id: PromoActionId
    abstract val restaurantId: RestaurantId
}

internal data class DiscountItem(val amount: Money, val discount: Money)

internal data class AbsoluteDiscountPromoActionDto(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    val discounts: List<DiscountItem>
) : PromoActionDto()

internal data class PercentItem(val amount: Money, val percent: Percent)

internal data class PercentDiscountPromoActionDto(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    val discounts: Collection<PercentItem>
) : PromoActionDto()

internal data class GiftItem(val amount: Money, val dishId: DishId)

internal data class GiftByCartAmountPromoActionDto(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    val gifts: Collection<GiftItem>
) : PromoActionDto()
