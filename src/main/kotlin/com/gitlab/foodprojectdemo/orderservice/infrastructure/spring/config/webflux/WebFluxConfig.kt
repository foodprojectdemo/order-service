package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.webflux

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.JacksonConfig
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.format.FormatterRegistry
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
@Import(JacksonConfig::class)
class WebFluxConfig(val objectMapper: ObjectMapper) : WebFluxConfigurer {

    override fun configureHttpMessageCodecs(configurer: ServerCodecConfigurer) {
        configurer.defaultCodecs().jackson2JsonEncoder(Jackson2JsonEncoder(objectMapper))
        configurer.defaultCodecs().jackson2JsonDecoder(Jackson2JsonDecoder(objectMapper))
    }

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverter(StringToDishIdConverter())
        registry.addConverter(StringToCartIdConverter())
        registry.addConverter(StringToOrderIdConverter())
        registry.addConverter(StringToRestaurantIdConverter())
        registry.addConverter(StringToCustomerIdConverter())
    }
}
