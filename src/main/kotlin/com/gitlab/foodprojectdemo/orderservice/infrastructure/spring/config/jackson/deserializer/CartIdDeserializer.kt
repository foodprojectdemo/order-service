package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.logger
import java.util.UUID

class CartIdDeserializer : StdDeserializer<CartId>(CartId::class.java) {
    val log = logger(this)
    override fun deserialize(parser: JsonParser, deserializationContext: DeserializationContext): CartId {
        val node = parser.codec.readTree<JsonNode>(parser)
        val id = node.asText()
        try {
            return CartId(UUID.fromString(id))
        } catch (e: IllegalArgumentException) {
            throw InvalidValueBusinessLogicException("Invalid cartId: $id")
        }
    }
}
