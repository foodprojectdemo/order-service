package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.toMoney
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
internal class DishRepositoryAdapter(private val restaurantGateway: RestaurantGateway) : DishRepository {

    override fun getDish(id: DishId): Mono<Dish> =
        restaurantGateway.getDish(id)
            .map { it.toDish() }

    private fun DishDto.toDish() = Dish(id, restaurantId, name, amount.toMoney())
}
