package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.toMoney
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

internal data class DishDto(val id: DishId, val restaurantId: RestaurantId, val name: String, val amount: Int)

@ConstructorBinding
@ConfigurationProperties(prefix = "restaurant-service")
internal data class RestaurantGatewayProperties(
    val url: String,
    val apiBaseUri: String,
    val dishUri: String,
    val promoActionsUri: String,
    val promoCodeUri: String
) {
    val baseUrl = url + apiBaseUri
}

@Component
@EnableConfigurationProperties(RestaurantGatewayProperties::class)
internal class RestaurantGateway(
    webClientBuilder: WebClient.Builder,
    private val properties: RestaurantGatewayProperties
) {

    private val webClient: WebClient = webClientBuilder.baseUrl(properties.baseUrl).build()

    fun getDish(id: DishId): Mono<DishDto> =
        webClient.get().uri(properties.dishUri, id)
            .retrieve()
            .bodyToMono(DishDto::class.java)

    private fun DishDto.toDish() = Dish(id, restaurantId, name, amount.toMoney())

    fun getRestaurantPromoActions(restaurantId: RestaurantId): Flux<PromoActionDto> =
        webClient.get().uri(properties.promoActionsUri, restaurantId)
            .retrieve()
            .bodyToFlux(PromoActionDto::class.java)

    fun getRestaurantPromoCode(promoCodeId: PromoCodeId, restaurantId: RestaurantId): Mono<PromoCodeDto> =
        webClient.get().uri(properties.promoCodeUri, restaurantId, promoCodeId)
            .retrieve()
            .bodyToMono(PromoCodeDto::class.java)
}
