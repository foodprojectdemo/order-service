package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import reactor.core.publisher.Mono

interface IncomingEventRepository {
    fun save(incomingEvent: IncomingEvent): Mono<IncomingEvent>
    fun existsById(id: DomainEventId): Mono<Boolean>
}
