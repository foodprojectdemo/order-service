package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId

@JsonTypeInfo(use = NAME, include = PROPERTY, property = "type")
@JsonSubTypes(
    Type(AbsoluteDiscountPromoCodeDto::class, name = "absolute_promo_code"),
    Type(PercentDiscountPromoCodeDto::class, name = "percent_promo_code"),
)
internal sealed class PromoCodeDto {
    abstract val code: PromoCodeId
    abstract val restaurantId: RestaurantId
}

internal data class AbsoluteDiscountPromoCodeDto(
    override val code: PromoCodeId,
    override val restaurantId: RestaurantId,
    val discount: Money,
) : PromoCodeDto()

internal data class PercentDiscountPromoCodeDto(
    override val code: PromoCodeId,
    override val restaurantId: RestaurantId,
    val percent: Percent,
) : PromoCodeDto()
