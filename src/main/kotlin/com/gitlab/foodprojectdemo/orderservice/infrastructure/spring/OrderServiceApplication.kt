package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication(
    scanBasePackages = [
        "com.gitlab.foodprojectdemo.orderservice.application",
        "com.gitlab.foodprojectdemo.orderservice.domain",
        "com.gitlab.foodprojectdemo.orderservice.infrastructure",
        "com.gitlab.foodprojectdemo.orderservice.ui",
    ]
)
class OrderServiceApplication

fun main(args: Array<String>) {
    runApplication<OrderServiceApplication>(*args)
}
