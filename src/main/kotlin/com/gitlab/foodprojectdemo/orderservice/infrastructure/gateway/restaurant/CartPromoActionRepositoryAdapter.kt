package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.ByCartAmountCondition
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoActionRepository
import com.gitlab.foodprojectdemo.orderservice.domain.promo.GiftByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PercentDiscountByCartAmountPromoAction
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@Component
internal class CartPromoActionRepositoryAdapter(
    private val restaurantGateway: RestaurantGateway,
    private val dishRepository: DishRepository
) : CartPromoActionRepository {

    override fun getRestaurantCartPromoActions(restaurantId: RestaurantId): Flux<CartPromoAction> =
        restaurantGateway.getRestaurantPromoActions(restaurantId)
            .flatMap(::toCartPromoAction)

    fun toCartPromoAction(dto: PromoActionDto): Mono<out CartPromoAction> =
        when (dto) {
            is AbsoluteDiscountPromoActionDto -> dto.toCartPromoAction()
            is PercentDiscountPromoActionDto -> dto.toCartPromoAction()
            is GiftByCartAmountPromoActionDto -> dto.toCartPromoAction()
        }

    private fun AbsoluteDiscountPromoActionDto.toCartPromoAction(): Mono<AbsoluteDiscountByCartAmountPromoAction> =
        AbsoluteDiscountByCartAmountPromoAction(
            id, restaurantId,
            discounts.map {
                ByCartAmountCondition(it.amount, it.discount.toBenefit())
            }
        ).toMono()

    private fun PercentDiscountPromoActionDto.toCartPromoAction(): Mono<PercentDiscountByCartAmountPromoAction> =
        PercentDiscountByCartAmountPromoAction(
            id, restaurantId,
            discounts.map {
                ByCartAmountCondition(it.amount, it.percent.toBenefit())
            }
        ).toMono()

    private fun GiftByCartAmountPromoActionDto.toCartPromoAction(): Mono<GiftByCartAmountPromoAction> {
        val dishMap = gifts.map(GiftItem::dishId).toFlux()
            .flatMap(dishRepository::getDish)
            .collectMap(Dish::id)

        @Suppress("NAME_SHADOWING")
        return dishMap.map { dishMap ->
            val conditions = gifts.filter {
                dishMap.containsKey(it.dishId)
            }.map {
                ByCartAmountCondition(it.amount, dishMap[it.dishId]!!.toBenefit())
            }

            GiftByCartAmountPromoAction(id, restaurantId, conditions)
        }
    }
}
