package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.function.TupleUtils.function
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import reactor.kotlin.core.publisher.toMono

@Component
class DomainEventPublisher(
    private val kafkaSender: KafkaSender<String, String>,
    private val objectMapper: ObjectMapper,
    @Value("\${kafka.topic.events-topic}")
    private val eventTopic: String
) {

    fun publish(event: DomainEvent<*, *>): Mono<SenderResult<DomainEventId>> {
        return event.toMono().zipWith(Mono.fromCallable { objectMapper.writeValueAsString(event) })
            .map(
                function { domainEvent, json ->
                    val key = "${domainEvent.aggregate}-${domainEvent.aggregateId}"
                    SenderRecord.create(
                        ProducerRecord(eventTopic, key, json),
                        domainEvent.id
                    )
                }
            )
            .transform(kafkaSender::send)
    }
}
