package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.webflux

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import org.springframework.core.convert.converter.Converter
import java.util.UUID

private fun uuid(id: String, idName: String) =
    try {
        UUID.fromString(id)
    } catch (e: IllegalArgumentException) {
        throw ApplicationException("Invalid $idName: $id")
    }

class StringToCartIdConverter : Converter<String, CartId> {
    override fun convert(id: String) = CartId(uuid(id, "CartId"))
}

class StringToOrderIdConverter : Converter<String, OrderId> {
    override fun convert(id: String) = OrderId(uuid(id, "OrderId"))
}

class StringToCustomerIdConverter : Converter<String, CustomerId> {
    override fun convert(id: String) = CustomerId(uuid(id, "CustomerId"))
}

class StringToDishIdConverter : Converter<String, DishId> {
    override fun convert(id: String) =
        try {
            DishId(id.toLong())
        } catch (e: IllegalArgumentException) {
            throw ApplicationException("Invalid DishId: $id")
        }
}

class StringToRestaurantIdConverter : Converter<String, RestaurantId> {
    override fun convert(id: String) =
        try {
            RestaurantId(id.toLong())
        } catch (e: IllegalArgumentException) {
            throw ApplicationException("Invalid RestaurantId: $id")
        }
}
