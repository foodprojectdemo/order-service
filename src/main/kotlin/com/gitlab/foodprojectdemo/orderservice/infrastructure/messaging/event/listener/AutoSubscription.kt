package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.gitlab.foodprojectdemo.orderservice.application.event.DomainEventDto
import com.gitlab.foodprojectdemo.orderservice.application.event.EventSubscriber
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import java.lang.invoke.LambdaMetafactory
import java.lang.invoke.MethodHandles.lookup
import java.lang.invoke.MethodType.methodType
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.util.function.BiConsumer
import java.util.function.Consumer
import javax.annotation.PostConstruct
import kotlin.jvm.internal.Reflection
import kotlin.reflect.KClass

@Component
class AutoSubscription(private val context: ApplicationContext, private val eventListener: EventListener) {

    @Suppress("LeakingThis")
    private val log = logger(this)

    private val lookup = lookup()

    @PostConstruct
    fun subscribe() {
        context.beanDefinitionNames.map(context::getBean).forEach { bean ->
            bean::class.java.declaredMethods.filter(this::isValidMethod)
                .forEach { method ->
                    log.info("subscribe $method")
//                    eventListener.subscribe(getEventTypeByMethod(method)) { event ->
//                        log.info("invoke $method with $event")
//                        method(bean, event)
//                    }
                    eventListener.subscribe(getEventTypeByMethod(method), createEventConsumer(bean, method))
                }
        }
    }

    private fun <E : DomainEventDto<*>> createEventConsumer(bean: Any, method: Method): Consumer<E> {
        val handler = lookup.unreflect(method)
        val lambdaType = methodType(BiConsumer::class.java)
        val erasedLambdaMethodType = handler.type().erase()

        val callSite = LambdaMetafactory.metafactory(
            lookup,
            "accept",
            lambdaType,
            erasedLambdaMethodType,
            handler,
            handler.type(),
        )

        @Suppress("UNCHECKED_CAST")
        val biConsumer = callSite.target() as BiConsumer<Any, E>

        return Consumer<E> { event ->
            log.info("invoke $method with $event")
            biConsumer.accept(bean, event)
        }
    }

    private fun isValidMethod(method: Method): Boolean {
        return isPublicAnnotatedMethod(method) && isValidMethodSignature(method)
    }

    private fun isPublicAnnotatedMethod(method: Method): Boolean {
        return method.isAnnotationPresent(EventSubscriber::class.java) && Modifier.isPublic(method.modifiers)
    }

    private fun isValidMethodSignature(method: Method): Boolean {
        return method.returnType == Void::class.javaPrimitiveType &&
            method.parameterCount == 1 &&
            DomainEventDto::class.java.isAssignableFrom(method.parameterTypes[0])
    }

    @Suppress("UNCHECKED_CAST")
    private fun getEventTypeByMethod(method: Method): KClass<DomainEventDto<*>> {
        return Reflection.createKotlinClass(method.parameterTypes[0]) as KClass<DomainEventDto<*>>
    }
}
