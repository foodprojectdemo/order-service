package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DomainEventRepository {
    fun findAll(): Flux<DomainEvent<*, *>>
    fun deleteById(id: DomainEventId): Mono<Void>
}
