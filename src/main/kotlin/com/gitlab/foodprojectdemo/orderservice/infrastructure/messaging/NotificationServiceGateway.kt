package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService
import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService.EmailNotification
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderRecord
import reactor.kotlin.core.publisher.toMono

@Service
class NotificationServiceGateway(
    @Value("\${kafka.topic.emails-topic}")
    private val emailTopic: String,
    private val objectMapper: ObjectMapper,
    private val kafkaSender: KafkaSender<String, String>,
) : NotificationService {

    override fun sendEmailNotification(emailNotification: EmailNotification): Mono<Void> {

        val record = emailNotification.toMono()
            .flatMap { Mono.fromCallable { objectMapper.writeValueAsString(it) } }
            .map { json ->
                SenderRecord.create(ProducerRecord<String, String>(emailTopic, json), emailNotification.id)
            }

        return kafkaSender.send(record).then()
    }
}
