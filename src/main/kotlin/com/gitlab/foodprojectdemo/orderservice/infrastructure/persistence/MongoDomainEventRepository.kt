package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher.DomainEventRepository
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface MongoDomainEventRepository : ReactiveMongoRepository<DomainEvent<*, *>, DomainEventId>, DomainEventRepository
