package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException

class RestaurantIdDeserializer : StdDeserializer<RestaurantId>(RestaurantId::class.java) {
    override fun deserialize(parser: JsonParser, deserializationContext: DeserializationContext): RestaurantId {
        val node = parser.codec.readTree<JsonNode>(parser)
        val id = node.asText()
        try {
            return RestaurantId(id.toLong())
        } catch (e: NumberFormatException) {
            throw InvalidValueBusinessLogicException("Invalid restaurantId: $id")
        }
    }
}
