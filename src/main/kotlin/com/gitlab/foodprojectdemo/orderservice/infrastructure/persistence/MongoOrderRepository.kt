package com.gitlab.foodprojectdemo.orderservice.infrastructure.persistence

import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderRepository
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface MongoOrderRepository : ReactiveMongoRepository<Order, OrderId>, OrderRepository
