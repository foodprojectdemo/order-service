package com.gitlab.foodprojectdemo.orderservice.infrastructure.spring.config.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent

class PercentDeserializer : StdDeserializer<Percent>(Percent::class.java) {
    override fun deserialize(parser: JsonParser, deserializationContext: DeserializationContext): Percent {
        val node = parser.codec.readTree<JsonNode>(parser)
        val percent = node.asText()
        try {
            return Percent(percent.toInt())
        } catch (e: NumberFormatException) {
            throw InvalidValueBusinessLogicException("Invalid percent: $percent")
        }
    }
}
