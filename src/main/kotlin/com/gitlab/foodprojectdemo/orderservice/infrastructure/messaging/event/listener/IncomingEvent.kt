package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.listener

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import org.springframework.data.mongodb.core.mapping.Document

@Document("incoming_event")
data class IncomingEvent(val id: DomainEventId)
