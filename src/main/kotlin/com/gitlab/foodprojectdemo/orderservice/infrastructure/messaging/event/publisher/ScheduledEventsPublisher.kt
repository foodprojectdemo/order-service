package com.gitlab.foodprojectdemo.orderservice.infrastructure.messaging.event.publisher

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import reactor.kafka.sender.SenderResult

@Component
class ScheduledEventsPublisher(
    private val domainEventPublisher: DomainEventPublisher,
    private val domainEventRepository: DomainEventRepository
) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @Scheduled(
        fixedRateString = "\${scheduled-publisher.fixed-rate-milliseconds:1000}",
        initialDelayString = "\${scheduled-publisher.initial-delay-milliseconds:1000}"
    )
    fun publish() {
        domainEventRepository.findAll()
            .doOnNext {
                log.info("Publishing event: {}", it)
            }
            .flatMap(domainEventPublisher::publish)
            .doOnNext {
                log.info("SenderResult {}", it)
            }
            .map(SenderResult<DomainEventId>::correlationMetadata)
            .flatMap(domainEventRepository::deleteById)
            .blockLast()
    }
}
