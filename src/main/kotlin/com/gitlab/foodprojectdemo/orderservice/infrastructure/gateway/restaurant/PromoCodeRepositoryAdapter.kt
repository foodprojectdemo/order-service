package com.gitlab.foodprojectdemo.orderservice.infrastructure.gateway.restaurant

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.AbsoluteDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PercentDiscountPromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeRepository
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
internal class PromoCodeRepositoryAdapter(private val restaurantGateway: RestaurantGateway) : PromoCodeRepository {

    override fun getRestaurantPromoCode(promoCodeId: PromoCodeId, restaurantId: RestaurantId): Mono<PromoCode> =
        restaurantGateway.getRestaurantPromoCode(promoCodeId, restaurantId)
            .map { it.toPromoCode() }

    private fun PromoCodeDto.toPromoCode(): PromoCode =
        when (this) {
            is AbsoluteDiscountPromoCodeDto -> AbsoluteDiscountPromoCode(code, restaurantId, discount.toBenefit())
            is PercentDiscountPromoCodeDto -> PercentDiscountPromoCode(code, restaurantId, percent.toBenefit())
        }
}
