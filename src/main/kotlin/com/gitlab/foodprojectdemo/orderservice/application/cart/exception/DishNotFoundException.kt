package com.gitlab.foodprojectdemo.orderservice.application.cart.exception

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId

class DishNotFoundException(dishId: DishId) : ApplicationNotFoundException("Dish with ID=$dishId not found")
