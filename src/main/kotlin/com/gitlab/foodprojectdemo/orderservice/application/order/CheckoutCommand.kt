package com.gitlab.foodprojectdemo.orderservice.application.order

import com.fasterxml.jackson.annotation.JsonCreator
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Address
import com.gitlab.foodprojectdemo.orderservice.domain.order.ContactInformation

@Suppress("DataClassPrivateConstructor")
data class CheckoutCommand private constructor(
    val customerId: CustomerId,
    val cartId: CartId,
    val address: Address,
    val contactInformation: ContactInformation
) {
    companion object {
        @JvmStatic
        @JsonCreator
        fun valueOf(
            customerId: CustomerId,
            cartId: CartId,

            shippingAddressStreet: String,
            shippingAddressHouse: String,
            shippingAddressFlat: Int,

            contactInformationName: String,
            contactInformationPhone: String,
            contactInformationEmail: String
        ) = CheckoutCommand(
            customerId,
            cartId,
            Address(shippingAddressStreet, shippingAddressHouse, shippingAddressFlat),
            ContactInformation(contactInformationName, contactInformationPhone, contactInformationEmail)
        )
    }
}
