package com.gitlab.foodprojectdemo.orderservice.application.order

import com.gitlab.foodprojectdemo.orderservice.domain.order.Order

enum class NewOrderStatus {
    ACCEPTED {
        override fun changeStatus(order: Order) = order.accept()
    },
    REJECTED {
        override fun changeStatus(order: Order) = order.reject()
    },
    READY {
        override fun changeStatus(order: Order) = order.ready()
    };

    abstract fun changeStatus(order: Order): Order
}
