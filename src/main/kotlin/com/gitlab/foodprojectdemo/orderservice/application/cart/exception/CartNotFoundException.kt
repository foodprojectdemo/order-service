package com.gitlab.foodprojectdemo.orderservice.application.cart.exception

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId

class CartNotFoundException(cartId: CartId) : ApplicationNotFoundException("Cart with ID=$cartId not found")
