package com.gitlab.foodprojectdemo.orderservice.application.event

import kotlin.annotation.AnnotationRetention.RUNTIME

@Retention(RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class EventSubscriber()
