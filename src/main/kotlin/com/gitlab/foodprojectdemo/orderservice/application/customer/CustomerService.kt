package com.gitlab.foodprojectdemo.orderservice.application.customer

import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono

@Service
class CustomerService(private val customerRepository: CustomerRepository) {

    @Transactional
    fun signUpCustomer(): Mono<Customer> = customerRepository.save(Customer())

    @Transactional(readOnly = true)
    fun getCustomer(customerId: CustomerId): Mono<Customer> = customerRepository.findById(customerId)
}
