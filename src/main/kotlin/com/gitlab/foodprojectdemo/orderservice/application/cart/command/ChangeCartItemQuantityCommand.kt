package com.gitlab.foodprojectdemo.orderservice.application.cart.command

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class ChangeCartItemQuantityCommand(val cartId: CartId, val index: Int, val quantity: Quantity)
