package com.gitlab.foodprojectdemo.orderservice.application.cart.exception

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId

class PreOrderNotFoundException(cartId: CartId) : ApplicationException("PreOrder with ID=$cartId not found")
