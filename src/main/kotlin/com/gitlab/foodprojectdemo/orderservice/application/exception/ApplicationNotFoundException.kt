package com.gitlab.foodprojectdemo.orderservice.application.exception

open class ApplicationNotFoundException(message: String) : ApplicationException(message)
