package com.gitlab.foodprojectdemo.orderservice.application.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.command.AddDishToCartCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ApplyPromoCodeCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ChangeCartItemQuantityCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.RemoveCartItemCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.CartItemNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.CartNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.DishNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.PreOrderNotFoundException
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.PromoCodeNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartItem
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartRepository
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishRepository
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.CartPromoService
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCode
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import reactor.function.TupleUtils.function
import reactor.kotlin.core.publisher.toMono

@Service
class CartService(
    private val cartRepository: CartRepository,
    private val dishRepository: DishRepository,
    private val cartPromoService: CartPromoService,
    private val promoCodeRepository: PromoCodeRepository
) {

    @Transactional
    fun createCart(): Mono<Cart> = cartRepository.save(Cart())

    @Transactional(readOnly = true)
    fun getCart(cartId: CartId): Mono<Cart> =
        getCartOrError(cartId)
            .flatMap(cartPromoService::applyTo)

    @Transactional
    fun addDishToCart(command: AddDishToCartCommand): Mono<Cart> =
        Mono.zip(getCartOrError(command.cartId), getDishOrError(command.dishId), command.quantity.toMono())
            .map(function(Cart::addDish))
            .flatMap(cartRepository::save)

    @Transactional
    fun changeCartItemQuantity(command: ChangeCartItemQuantityCommand): Mono<Cart> =
        updateCartIfPresentCartItemWithIndex(command.cartId, command.index) { cart, cartItem ->
            cart.changeQuantity(cartItem, command.quantity)
        }
            .flatMap(cartRepository::save)

    @Transactional
    fun removeCartItem(command: RemoveCartItemCommand): Mono<Cart> =
        updateCartIfPresentCartItemWithIndex(command.cartId, command.index, Cart::remove)
            .flatMap(cartRepository::save)

    @Transactional
    fun applyPromoCode(command: ApplyPromoCodeCommand): Mono<Cart> =
        getCartOrError(command.cartId)
            .flatMap { cart ->
                cart.toMono().zipWith(getPromoCodeOrError(command.promoCodeId, cart.restaurantId).map(PromoCode::id))
            }
            .map(function(Cart::applyPromoCode))
            .flatMap(cartRepository::save)

    @Transactional
    fun removePromoCode(cartId: CartId): Mono<Cart> =
        getCartOrError(cartId)
            .map(Cart::removePromoCode)
            .flatMap(cartRepository::save)

    @Transactional
    fun clearCart(cartId: CartId): Mono<Cart> =
        getCartOrError(cartId)
            .map(Cart::clear)
            .flatMap(cartRepository::save)

    @Transactional
    fun placePreOrder(cartId: CartId): Mono<PreOrder> =
        getCart(cartId)
            .map(Cart::placePreOrder)
            .flatMap(cartRepository::save)
            .flatMap { cart ->
                cart.preOrder.toMono()
            }

    @Transactional(readOnly = true)
    fun getPreOrder(cartId: CartId): Mono<PreOrder> =
        getCartOrError(cartId)
            .flatMap { cart ->
                cart.preOrder.toMono()
            }.switchIfEmpty(PreOrderNotFoundException(cartId).toMono())

    private fun updateCartIfPresentCartItemWithIndex(
        cartId: CartId,
        cartItemIndex: Int,
        transformer: (Cart, CartItem) -> Cart
    ): Mono<Cart> =
        getCartOrError(cartId)
            .map { cart ->
                cart.items.getOrNull(cartItemIndex)?.let {
                    transformer(cart, it)
                } ?: throw CartItemNotFoundException(cartItemIndex)
            }

    private fun getPromoCodeOrError(promoCodeId: PromoCodeId, restaurantId: RestaurantId): Mono<PromoCode> =
        promoCodeRepository.getRestaurantPromoCode(promoCodeId, restaurantId)
            .switchIfEmpty(PromoCodeNotFoundException(promoCodeId, restaurantId).toMono())

    private fun getDishOrError(dishId: DishId): Mono<Dish> =
        dishRepository.getDish(dishId)
            .switchIfEmpty(DishNotFoundException(dishId).toMono())

    private fun getCartOrError(cartId: CartId): Mono<Cart> =
        cartRepository.findById(cartId)
            .switchIfEmpty(CartNotFoundException(cartId).toMono())
}
