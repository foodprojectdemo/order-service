package com.gitlab.foodprojectdemo.orderservice.application.order

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class OrderService(
    private val cartService: CartService,
    private val orderRepository: OrderRepository
) {

    @Transactional(readOnly = true)
    fun getOrder(orderId: OrderId) = orderRepository.findById(orderId)

    @Transactional(readOnly = true)
    fun getCustomerOrders(customerId: CustomerId): Flux<Order> =
        orderRepository.findByCustomerId(customerId)

    @Transactional(readOnly = true)
    fun getRestaurantOrders(restaurantId: RestaurantId): Flux<Order> =
        orderRepository.findByRestaurantId(restaurantId)

    @Transactional
    fun changeStatus(orderId: OrderId, newOrderStatus: NewOrderStatus): Mono<Order> =
        orderRepository.findById(orderId)
            .map(newOrderStatus::changeStatus)
            .flatMap(orderRepository::save)

    @Transactional
    fun checkout(command: CheckoutCommand): Mono<Order> =
        getPreOrderOrError(command.cartId)
            .map { preOrder ->
                Order.place(
                    preOrder,
                    command.customerId,
                    command.contactInformation,
                    command.address
                )
            }
            .flatMap(orderRepository::save)
            .flatMap { order ->
                clearCart(command.cartId)
                    .thenReturn(order)
            }

    private fun clearCart(cartId: CartId): Mono<Cart> =
        cartService.clearCart(cartId)

    private fun getPreOrderOrError(cartId: CartId): Mono<PreOrder> =
        cartService.getPreOrder(cartId)
}
