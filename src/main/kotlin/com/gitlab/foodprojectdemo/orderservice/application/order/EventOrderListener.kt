package com.gitlab.foodprojectdemo.orderservice.application.order

import com.gitlab.foodprojectdemo.orderservice.application.event.EventSubscriber
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketAcceptedEvent
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketReadyEvent
import com.gitlab.foodprojectdemo.orderservice.application.event.TicketRejectedEvent
import com.gitlab.foodprojectdemo.orderservice.application.order.NewOrderStatus.ACCEPTED
import com.gitlab.foodprojectdemo.orderservice.application.order.NewOrderStatus.READY
import com.gitlab.foodprojectdemo.orderservice.application.order.NewOrderStatus.REJECTED
import org.springframework.stereotype.Service

@Service
class EventOrderListener(private val orderService: OrderService) {
    @EventSubscriber
    fun onEventChangeOrder(event: TicketAcceptedEvent) {
        orderService.changeStatus(event.aggregateId, ACCEPTED).block()
    }

    @EventSubscriber
    fun onEventChangeOrder(event: TicketRejectedEvent) {
        orderService.changeStatus(event.aggregateId, REJECTED).block()
    }

    @EventSubscriber
    fun onEventChangeOrder(event: TicketReadyEvent) {
        orderService.changeStatus(event.aggregateId, READY).block()
    }
}
