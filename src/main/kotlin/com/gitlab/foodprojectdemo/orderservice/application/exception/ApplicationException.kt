package com.gitlab.foodprojectdemo.orderservice.application.exception

open class ApplicationException : RuntimeException {

    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)

    override val message: String
        get() = super.message!!
}
