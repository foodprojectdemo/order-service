package com.gitlab.foodprojectdemo.orderservice.application.cart.command

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class AddDishToCartCommand(val cartId: CartId, val dishId: DishId, val quantity: Quantity)
