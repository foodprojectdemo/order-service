package com.gitlab.foodprojectdemo.orderservice.application.cart.exception

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationNotFoundException

class CartItemNotFoundException(index: Int) : ApplicationNotFoundException("Cart Item with index $index not found")
