package com.gitlab.foodprojectdemo.orderservice.application.notification

import com.gitlab.foodprojectdemo.orderservice.application.event.EventSubscriber
import com.gitlab.foodprojectdemo.orderservice.application.event.OrderPlacedEvent
import com.gitlab.foodprojectdemo.orderservice.application.notification.NotificationService.EmailNotification
import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import org.springframework.stereotype.Service

@Service
class EventNotificationListener(
    private val orderService: OrderService,
    private val notificationService: NotificationService
) {

    @EventSubscriber
    fun onEventSendNotification(event: OrderPlacedEvent) {
        orderService.getOrder(event.aggregateId)
            .map { order ->
                EmailNotification.create(
                    "New order ${order.id}",
                    "Hi ${order.contactInformation.name}! New order was placed",
                    order.contactInformation.email
                )
            }
            .flatMap(notificationService::sendEmailNotification)
            .block()
    }
}
