package com.gitlab.foodprojectdemo.orderservice.application.event

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import java.time.ZonedDateTime

data class TicketReadyEvent(
    override val id: DomainEventId,
    override val aggregate: String,
    override val aggregateId: OrderId,
    override val occurredOn: ZonedDateTime
) : DomainEventDto<OrderId>
