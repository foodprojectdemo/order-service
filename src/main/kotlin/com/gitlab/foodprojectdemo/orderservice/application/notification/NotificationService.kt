package com.gitlab.foodprojectdemo.orderservice.application.notification

import reactor.core.publisher.Mono
import java.util.UUID

interface NotificationService {

    @Suppress("DataClassPrivateConstructor")
    data class EmailNotification private constructor(
        val id: UUID,
        val subject: String,
        val text: String,
        val toEmail: String
    ) {
        companion object {
            fun create(
                subject: String,
                text: String,
                toEmail: String
            ) = EmailNotification(UUID.randomUUID(), subject, text, toEmail)
        }
    }

    fun sendEmailNotification(emailNotification: EmailNotification): Mono<Void>
}
