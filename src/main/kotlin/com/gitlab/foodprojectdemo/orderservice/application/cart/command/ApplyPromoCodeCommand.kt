package com.gitlab.foodprojectdemo.orderservice.application.cart.command

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId

data class ApplyPromoCodeCommand(val cartId: CartId, val promoCodeId: PromoCodeId)
