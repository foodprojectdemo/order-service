package com.gitlab.foodprojectdemo.orderservice.application.cart.command

import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId

data class RemoveCartItemCommand(val cartId: CartId, val index: Int)
