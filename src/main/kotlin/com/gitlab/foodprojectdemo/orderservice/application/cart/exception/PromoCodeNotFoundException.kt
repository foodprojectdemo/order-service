package com.gitlab.foodprojectdemo.orderservice.application.cart.exception

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationException
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId

class PromoCodeNotFoundException(promoCodeId: PromoCodeId, restaurantId: RestaurantId) :
    ApplicationException("PromoCode with ID=$promoCodeId from restaurant $restaurantId not found")
