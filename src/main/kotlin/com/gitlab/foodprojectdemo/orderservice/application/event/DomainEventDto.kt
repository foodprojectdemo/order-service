package com.gitlab.foodprojectdemo.orderservice.application.event

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import java.time.ZonedDateTime

@JsonTypeInfo(use = NAME, property = "type")
@JsonSubTypes(
    Type(value = TicketAcceptedEvent::class, name = "ticket_accepted"),
    Type(value = TicketRejectedEvent::class, name = "ticket_rejected"),
    Type(value = TicketReadyEvent::class, name = "ticket_ready"),
    Type(value = OrderPlacedEvent::class, name = "order_placed"),
)
interface DomainEventDto<T> {
    val id: DomainEventId
    val aggregate: String
    val aggregateId: T
    val occurredOn: ZonedDateTime
}
