package com.gitlab.foodprojectdemo.orderservice.domain.cart

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainRepository

interface CartRepository : DomainRepository<Cart, CartId>
