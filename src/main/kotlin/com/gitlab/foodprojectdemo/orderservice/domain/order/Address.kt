package com.gitlab.foodprojectdemo.orderservice.domain.order

data class Address(val street: String, val house: String, val flat: Int)
