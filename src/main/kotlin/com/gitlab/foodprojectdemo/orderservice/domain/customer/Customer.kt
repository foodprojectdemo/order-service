package com.gitlab.foodprojectdemo.orderservice.domain.customer

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Entity
import java.io.Serializable
import java.util.UUID

data class CustomerId(private val id: UUID = UUID.randomUUID()) : Serializable {
    override fun toString() = id.toString()
}

class Customer(id: CustomerId = CustomerId()) : Entity<CustomerId>(id)
