package com.gitlab.foodprojectdemo.orderservice.domain.misc

import com.gitlab.foodprojectdemo.orderservice.domain.exception.BusinessLogicException

@Suppress("DataClassPrivateConstructor")
data class Percent private constructor(val percent: Int) {

    companion object {
        val zero = Percent(0)
        val hundred = Percent(100)

        operator fun invoke(percent: Int): Percent {
            if (percent !in 1 until 100)
                throw BusinessLogicException("Invalid percent value $percent. Must be between 1 and 100")

            return Percent(percent)
        }
    }

    operator fun times(amount: Money): Money = if (amount == Money.zero) Money.zero else amount * (percent / 100.0)

    fun toInt() = percent

    override fun toString(): String {
        return "$percent%"
    }

    operator fun minus(other: Percent) =
        if (this.percent == other.percent) zero else invoke(this.percent - other.percent)
}
