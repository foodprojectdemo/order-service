package com.gitlab.foodprojectdemo.orderservice.domain.misc

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.time.ZonedDateTime
import java.util.UUID
import kotlin.reflect.cast

data class DomainEventId(val id: UUID = UUID.randomUUID()) : Serializable {
    override fun toString() = id.toString()
}

@Document("event")
abstract class DomainEvent<A : RootAggregate<A, ID>, ID : Any>(
    @Id
    val id: DomainEventId,
    val type: String,
    val aggregate: String,
    val aggregateId: ID,
    val occurredOn: ZonedDateTime,
    val body: Any?
) {
    constructor(aggregate: A, type: String, body: Any? = null) : this(
        DomainEventId(),
        type,
        aggregate.name,
        aggregate.id,
        SystemTime.now(),
        body
    )

    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class)
            return false

        val casted = this::class.cast(other)

        if (id != casted.id) return false

        return true
    }

    override fun hashCode() = id.hashCode()

    override fun toString(): String {
        return "${this::class.simpleName}(id=$id, type='$type', aggregate='$aggregate', aggregateId=$aggregateId, occurredOn=$occurredOn, body=$body)"
    }
}
