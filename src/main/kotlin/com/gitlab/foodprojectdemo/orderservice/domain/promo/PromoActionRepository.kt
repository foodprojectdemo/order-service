package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import reactor.core.publisher.Flux

interface PromoActionRepository {
    fun findByRestaurantId(restaurantId: RestaurantId): Flux<PromoAction<*>>
}
