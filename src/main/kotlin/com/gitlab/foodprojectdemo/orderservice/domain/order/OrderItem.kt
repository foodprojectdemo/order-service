package com.gitlab.foodprojectdemo.orderservice.domain.order

import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class OrderItem(
    val dishId: DishId,
    val name: String,
    val price: Money,
    val quantity: Quantity
) {
    val totalAmount: Money
        get() = price * quantity
}

fun Collection<OrderItem>.sum(): Money = this.map { it.totalAmount }.fold(Money.zero, Money::plus)
