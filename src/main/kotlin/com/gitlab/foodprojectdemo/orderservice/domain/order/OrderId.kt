package com.gitlab.foodprojectdemo.orderservice.domain.order

import java.io.Serializable
import java.util.UUID

data class OrderId(private val id: UUID = UUID.randomUUID()) : Serializable {
    override fun toString() = id.toString()
}
