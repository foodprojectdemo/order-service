package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import java.io.Serializable

data class PromoActionId(private val id: Long) : Serializable {
    override fun toString() = id.toString()
}

sealed class PromoAction<T : Any> : Promo {
    abstract val id: PromoActionId
    abstract val conditions: List<Condition<T>>

    fun verify(verifiable: T): Benefit? {
        return conditions
            .filter { condition ->
                condition.satisfy(verifiable)
            }
            .map(Condition<T>::benefit)
            .fold(null as Benefit?) { _, last ->
                last
            }
    }
}

sealed class CartPromoAction : PromoAction<Cart>() {
    abstract override val conditions: List<CartCondition>
}

data class AbsoluteDiscountByCartAmountPromoAction(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    override val conditions: List<ByCartAmountCondition<AbsoluteDiscount>>
) : CartPromoAction()

data class PercentDiscountByCartAmountPromoAction(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    override val conditions: List<ByCartAmountCondition<PercentDiscount>>
) : CartPromoAction()

data class GiftByCartAmountPromoAction(
    override val id: PromoActionId,
    override val restaurantId: RestaurantId,
    override val conditions: List<ByCartAmountCondition<Gift>>
) : CartPromoAction()
