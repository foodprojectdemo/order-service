package com.gitlab.foodprojectdemo.orderservice.domain.exception

open class InvalidValueBusinessLogicException(message: String?, code: String? = null) : BusinessLogicException(message, code)
