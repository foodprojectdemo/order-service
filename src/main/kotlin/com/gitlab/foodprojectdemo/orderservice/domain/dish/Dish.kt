package com.gitlab.foodprojectdemo.orderservice.domain.dish

import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import java.io.Serializable

@Suppress("DataClassPrivateConstructor")
data class DishId private constructor(private val id: Long) : Serializable {

    companion object {

        operator fun invoke(id: Long): DishId {
            if (id < 1) throw InvalidValueBusinessLogicException("DishId must be greater than 0")

            return DishId(id)
        }
    }

    override fun toString() = id.toString()
}

@Suppress("DataClassPrivateConstructor")
data class RestaurantId private constructor(private val id: Long) : Serializable {

    companion object {

        operator fun invoke(id: Long): RestaurantId {
            if (id < 1) throw InvalidValueBusinessLogicException("RestaurantId must be greater than 0")

            return RestaurantId(id)
        }
    }

    override fun toString() = id.toString()
}

data class Dish(
    val id: DishId,
    val restaurantId: RestaurantId,
    val name: String,
    val price: Money
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Dish

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
