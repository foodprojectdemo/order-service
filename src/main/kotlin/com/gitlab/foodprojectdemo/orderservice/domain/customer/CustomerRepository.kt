package com.gitlab.foodprojectdemo.orderservice.domain.customer

import reactor.core.publisher.Mono

interface CustomerRepository {
    fun save(customer: Customer): Mono<Customer>
    fun findById(customerId: CustomerId): Mono<Customer>
}
