package com.gitlab.foodprojectdemo.orderservice.domain.misc

import com.gitlab.foodprojectdemo.orderservice.domain.exception.BusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import org.springframework.data.annotation.Transient
import java.util.Currency

class InvalidMoneyAmountException : InvalidValueBusinessLogicException("Money must be greater than 0")

/**
 * only integer number and dollars
 */
@Suppress("DataClassPrivateConstructor")
data class Money private constructor(private val amount: Long) : Comparable<Money> {
    companion object {
        val zero = Money(0)

        operator fun invoke(amount: Long): Money {
            if (amount < 1) throw InvalidMoneyAmountException()

            return Money(amount)
        }
    }

    @Transient
    val currency: Currency = Currency.getInstance("USD")

    operator fun plus(other: Money): Money {
        checkTheSameCurrency(other)
        return if (other > zero) {
            invoke(this.amount + other.amount)
        } else {
            this
        }
    }

    operator fun minus(other: Money): Money {
        checkTheSameCurrency(other)
        return if (this.amount == other.amount) zero else invoke(this.amount - other.amount)
    }

    override operator fun compareTo(other: Money): Int {
        checkTheSameCurrency(other)
        return amount.compareTo(other.amount)
    }

    operator fun times(quantity: Quantity) = if (amount == 0L) zero else invoke(amount * quantity.toInt())

    operator fun times(coefficient: Double): Money {
        if (amount < 0) throw BusinessLogicException("coefficient must be greater or equals than 0")
        return if (coefficient == 0.0) zero else invoke((amount * coefficient).toLong())
    }

    operator fun times(coefficient: Int): Money {
        if (amount < 0) throw BusinessLogicException("coefficient must be greater or equals than 0")
        return if (coefficient == 0) zero else invoke(amount * coefficient)
    }

    operator fun times(percent: Percent): Money = percent * this

    operator fun div(other: Money): Double {
        checkTheSameCurrency(other)
        return amount.toDouble() / other.amount.toDouble()
    }

    operator fun div(quantity: Quantity): Money = invoke((amount.toDouble() / quantity.toInt()).toLong())

    operator fun div(d: Double): Money = invoke((amount.toDouble() / d).toLong())

    operator fun rem(quantity: Quantity): Long = amount % quantity.toInt()

    private fun checkTheSameCurrency(other: Money) {
        if (other.currency != currency)
            throw BusinessLogicException("only the same currencies cannot participate in the operation")
    }

    override fun toString(): String {
        return "${currency.symbol}$amount"
    }

    fun toLong(): Long {
        return amount
    }

    fun toInt(): Int {
        return amount.toInt()
    }
}
