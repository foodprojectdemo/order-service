package com.gitlab.foodprojectdemo.orderservice.domain.misc

abstract class Entity<ID : Any>(val id: ID) {

    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        val casted = other as Entity<*>

        if (id != casted.id) return false

        return true
    }

    override fun hashCode() = id.hashCode()
}
