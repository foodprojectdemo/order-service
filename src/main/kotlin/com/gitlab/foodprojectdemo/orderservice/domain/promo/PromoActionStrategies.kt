package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money

sealed class PromoActionStrategy

sealed class DiscountPromoActionStrategy : PromoActionStrategy() {
    abstract fun define(discounts: Collection<Money>): Money
}

sealed class GiftPromoActionStrategy : PromoActionStrategy() {
    abstract fun define(discounts: Collection<Gift>): Collection<Gift>
}

object MaxDiscountPromoActionStrategy : DiscountPromoActionStrategy() {
    override fun define(discounts: Collection<Money>): Money = discounts.maxOrNull() ?: Money.zero
}

object SumDiscountPromoActionStrategy : DiscountPromoActionStrategy() {
    override fun define(discounts: Collection<Money>): Money =
        discounts.fold(Money.zero, Money::plus)
}

object AllGiftsPromoActionStrategy : GiftPromoActionStrategy() {
    override fun define(discounts: Collection<Gift>): Collection<Gift> = discounts
}
