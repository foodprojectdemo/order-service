package com.gitlab.foodprojectdemo.orderservice.domain.order

enum class OrderStatus {
    NEW {
        override fun accept() = ACCEPTED
        override fun reject() = REJECTED
    },
    ACCEPTED {
        override fun accept() = this
        override fun ready() = READY
    },
    REJECTED {
        override fun reject() = this
    },
    READY {
        override fun ready() = this
    };

    open fun accept(): OrderStatus = throw illegalStateException("accept")

    open fun reject(): OrderStatus = throw illegalStateException("reject")

    open fun ready(): OrderStatus = throw illegalStateException("ready")

    private fun illegalStateException(action: String) =
        IllegalStateException("Invalid action $action for ${this::name} state")
}
