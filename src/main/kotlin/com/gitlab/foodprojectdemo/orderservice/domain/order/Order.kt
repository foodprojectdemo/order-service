package com.gitlab.foodprojectdemo.orderservice.domain.order

import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrderItem
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import com.gitlab.foodprojectdemo.orderservice.domain.misc.SystemTime
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderAcceptedEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderPlacedEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderReadyEvent
import com.gitlab.foodprojectdemo.orderservice.domain.order.event.OrderRejectedEvent
import org.springframework.data.mongodb.core.mapping.Document
import java.time.ZonedDateTime

@Document("order")
class Order private constructor(
    id: OrderId,
    val customerId: CustomerId,
    val restaurantId: RestaurantId,
    val items: Collection<OrderItem>,
    val discount: Money,
    val address: Address,
    val contactInformation: ContactInformation
) : RootAggregate<Order, OrderId>(id) {

    companion object {
        fun place(
            preOrder: PreOrder,
            customerId: CustomerId,
            contactInformation: ContactInformation,
            address: Address
        ) = Order(
            OrderId(),
            customerId,
            preOrder.restaurantId,
            preOrder.items.toOrderItems(),
            preOrder.discount,
            address,
            contactInformation
        ).apply {
            addEvent(OrderPlacedEvent(this))
        }

        private fun Collection<PreOrderItem>.toOrderItems() =
            map { preOrderItem ->
                OrderItem(
                    preOrderItem.dishId,
                    preOrderItem.name,
                    preOrderItem.price,
                    preOrderItem.quantity
                )
            }
    }

    var status: OrderStatus = OrderStatus.NEW
        private set

    var createdAt: ZonedDateTime = SystemTime.now()
        private set

    val subTotal: Money
        get() = items.sum()

    val total: Money
        get() = subTotal - discount

    fun accept() = this.apply {
        if (status != status.accept()) {
            status = status.accept()
            addEvent(OrderAcceptedEvent(this))
        }
    }

    fun reject() = this.apply {
        if (status != status.reject()) {
            status = status.reject()
            addEvent(OrderRejectedEvent(this))
        }
    }

    fun ready() = this.apply {
        if (status != status.ready()) {
            status = status.ready()
            addEvent(OrderReadyEvent(this))
        }
    }

    override fun toString(): String {
        return "Order(id=$id, " +
            "customerId=$customerId, " +
            "restaurantId=$restaurantId, " +
            "items=$items, " +
            "discount=$discount, " +
            "address=$address, " +
            "contactInformation=$contactInformation, " +
            "status=$status, " +
            "createdAt=$createdAt, " +
            "subTotal=$subTotal, " +
            "total=$total, " +
            "events=$events)"
    }
}
