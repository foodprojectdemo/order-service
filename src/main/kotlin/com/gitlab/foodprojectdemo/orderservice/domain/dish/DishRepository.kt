package com.gitlab.foodprojectdemo.orderservice.domain.dish

import reactor.core.publisher.Mono

interface DishRepository {
    fun getDish(id: DishId): Mono<Dish>
}
