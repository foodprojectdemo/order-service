package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId

interface Promo {
    val restaurantId: RestaurantId
}
