package com.gitlab.foodprojectdemo.orderservice.domain.cart

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money.Companion.zero
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity.Companion.empty
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity.Companion.one
import com.gitlab.foodprojectdemo.orderservice.domain.misc.RootAggregate
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId
import java.io.Serializable
import java.util.UUID

data class CartId(private val id: UUID = UUID.randomUUID()) : Serializable {
    override fun toString() = id.toString()
}

data class CartItem(
    val dish: Dish,
    val quantity: Quantity
) {
    val totalAmount: Money
        get() = dish.price * quantity
}

class Cart(id: CartId = CartId()) : RootAggregate<Cart, CartId>(id) {

    private var _restaurantId: RestaurantId? = null
    val restaurantId: RestaurantId
        get() = _restaurantId ?: throw EmptyCartRestaurantIdAccessException()

    var preOrder: PreOrder? = null
        private set

    private val _items: MutableMap<Dish, Quantity> = linkedMapOf()
    val items: List<CartItem>
        get() = _items.toCartItemList()

    private val _gifts: MutableMap<Dish, Quantity> = linkedMapOf()
    val gifts: List<CartItem>
        get() = _gifts.toCartItemList()

    val subTotal: Money
        get() = items.map { it.totalAmount }.fold(zero, Money::plus)

    var discount: Money = zero
        private set

    val total: Money
        get() = subTotal - discount

    val isEmpty: Boolean
        get() = _items.isEmpty()

    val isNotEmpty: Boolean
        get() = _items.isNotEmpty()

    var promoCodeId: PromoCodeId? = null
        private set

    fun addDish(dish: Dish, quantity: Quantity = one) = apply {
        defineRestaurantId(dish)
        _items.addQuantity(dish, quantity)
        invalidatePreOrder()
    }

    fun changeQuantity(cartItem: CartItem, quantity: Quantity) = apply {
        if (items.contains(cartItem)) {
            _items[cartItem.dish] = quantity
            invalidatePreOrder()
        }
    }

    fun remove(cartItem: CartItem) = apply {
        if (items.contains(cartItem)) {
            _items.remove(cartItem.dish)

            if (isEmpty) resetRestaurantId()

            invalidatePreOrder()
        }
    }

    fun clear() = apply {
        _items.clear()
        assignDiscount(zero)
        clearGifts()
        resetRestaurantId()
        invalidatePreOrder()
    }

    fun addGift(gift: Dish, quantity: Quantity = one) = apply {
        if (isEmpty)
            throw CannotAddGiftToEmptyCartException()
        defineRestaurantId(gift)
        _gifts.addQuantity(gift, quantity)
        invalidatePreOrder()
    }

    fun clearGifts() = apply {
        _gifts.clear()
        invalidatePreOrder()
    }

    infix fun assignDiscount(discount: Money) = apply {
        this.discount = checkMaxAvailableDiscount(discount)
        invalidatePreOrder()
    }

    infix fun add(discount: Money) = assignDiscount(this.discount + discount)

    private fun checkMaxAvailableDiscount(newDiscount: Money): Money =
        if (newDiscount <= subTotal)
            newDiscount
        else
            subTotal

    fun applyPromoCode(promoCodeId: PromoCodeId) = apply {
        this.promoCodeId = promoCodeId
    }

    fun removePromoCode() = apply {
        promoCodeId = null
        invalidatePreOrder()
    }

    fun placePreOrder() = apply {
        if (isEmpty) throw CannotPlacePreOrderForEmptyCartException()
        preOrder = PreOrder(this)
    }

    fun invalidatePreOrder() {
        preOrder = null
    }

    private fun defineRestaurantId(dish: Dish) {
        if (_restaurantId == null)
            _restaurantId = dish.restaurantId
        else if (dish.restaurantId != _restaurantId)
            throw AnotherRestaurantDishException()
    }

    private fun resetRestaurantId() {
        _restaurantId = null
    }

    private fun MutableMap<Dish, Quantity>.addQuantity(dish: Dish, quantity: Quantity) =
        compute(dish) { _, currentQuantity: Quantity? ->
            (currentQuantity ?: empty) + quantity
        }

    private fun MutableMap<Dish, Quantity>.toCartItemList() =
        map { (dish, quantity) -> CartItem(dish, quantity) }

    override fun toString(): String {
        return "Cart(_restaurantId=$_restaurantId, preOrder=$preOrder, items=$items, gifts=$gifts, subTotal=$subTotal, discount=$discount, total=$total, promoCodeId=$promoCodeId)"
    }
}
