package com.gitlab.foodprojectdemo.orderservice.domain.misc

import java.time.Clock.systemDefaultZone
import java.time.ZonedDateTime

object SystemTime {
    private val clock = systemDefaultZone()
    fun now(): ZonedDateTime = ZonedDateTime.now(clock)
}
