package com.gitlab.foodprojectdemo.orderservice.domain.order

data class ContactInformation(val name: String, val phone: String, val email: String)
