package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

/**
 * Some Benefit if verifiable satisfies condition
 */
interface Condition<V : Any> {
    val benefit: Benefit
    fun satisfy(verifiable: V): Boolean
}

interface CartCondition : Condition<Cart> {
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun satisfy(cart: Cart): Boolean
}

class ByCartAmountCondition<B : Benefit>(
    private val cartAmount: Money,
    override val benefit: B,
) : CartCondition {

    override fun satisfy(cart: Cart) = cart.subTotal >= cartAmount

    override fun toString(): String {
        return "ByCartAmount(cartAmount=$cartAmount, benefit=$benefit)"
    }
}

class CartContainsDishWithMinQuantityCondition<B : Benefit>(
    override val benefit: B,
    val dish: Dish,
    val quantity: Quantity,
) : CartCondition {

    override fun satisfy(cart: Cart): Boolean =
        cart.items.filter {
            it.dish == dish && it.quantity >= quantity
        }.any()
}

infix fun Cart.satisfies(cartCondition: CartCondition) = cartCondition.satisfy(this)
