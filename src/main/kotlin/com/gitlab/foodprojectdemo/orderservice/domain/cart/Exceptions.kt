package com.gitlab.foodprojectdemo.orderservice.domain.cart

import com.gitlab.foodprojectdemo.orderservice.domain.exception.BusinessLogicException

class EmptyCartRestaurantIdAccessException :
    BusinessLogicException("You cannot get restaurantId for an empty cart", "EMPTY_CART_RESTAURANT_ID_ACCESS")

class CannotAddGiftToEmptyCartException :
    BusinessLogicException("You cannot add gifts to an empty cart", "ADDING_GIFT_TO_EMPTY_CART")

class CannotPlacePreOrderForEmptyCartException :
    BusinessLogicException("You cannot place pre-order", "PLACE_PREORDER_FOR_EMPTY_CART")

class PreOrderIsNotPresentException :
    BusinessLogicException("Pre-order is not present. Try place pre-order", "PREORDER_IS_NOT_PRESENT")

class AnotherRestaurantDishException :
    BusinessLogicException("You cannot add dishes from different restaurants into the cart", "ANOTHER_RESTAURANT_DISH")
