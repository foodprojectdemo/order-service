package com.gitlab.foodprojectdemo.orderservice.domain.order.event

import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEvent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainEventId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import org.springframework.data.annotation.PersistenceConstructor
import java.time.ZonedDateTime

class OrderPlacedEvent : DomainEvent<Order, OrderId> {

    @PersistenceConstructor
    private constructor(
        id: DomainEventId,
        type: String,
        aggregate: String,
        aggregateId: OrderId,
        occurredOn: ZonedDateTime,
        body: Any?
    ) : super(id, type, aggregate, aggregateId, occurredOn, body)

    constructor(aggregate: Order) : super(aggregate, "order_placed")
}
