package com.gitlab.foodprojectdemo.orderservice.domain.misc

import reactor.core.publisher.Mono

interface DomainRepository<A : RootAggregate<A, ID>, ID : Any> {
    fun save(aggregate: A): Mono<A>
    fun findById(id: ID): Mono<A>
}
