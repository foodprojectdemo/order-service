package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import reactor.core.publisher.Flux

interface CartPromoActionRepository {
    fun getRestaurantCartPromoActions(restaurantId: RestaurantId): Flux<CartPromoAction>
}
