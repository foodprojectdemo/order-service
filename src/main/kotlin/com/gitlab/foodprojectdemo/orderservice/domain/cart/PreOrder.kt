package com.gitlab.foodprojectdemo.orderservice.domain.cart

import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money.Companion.zero
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class PreOrderItem(
    val dishId: DishId,
    val name: String,
    val price: Money,
    val quantity: Quantity
) {
    val totalAmount: Money
        get() = price * quantity
}

data class PreOrder(
    val restaurantId: RestaurantId,
    val items: List<PreOrderItem>,
    val discount: Money,
) {
    companion object {
        private fun Collection<CartItem>.toPreOrderItems(isFree: Boolean = false) =
            map {
                PreOrderItem(
                    it.dish.id,
                    it.dish.name,
                    if (isFree) zero else it.dish.price,
                    it.quantity
                )
            }
    }

    constructor(cart: Cart) : this(
        cart.restaurantId,
        cart.items.toPreOrderItems() + cart.gifts.toPreOrderItems(true),
        cart.discount
    )

    val subTotal: Money
        get() = items.sum()

    val total: Money
        get() = subTotal - discount

    private fun Collection<PreOrderItem>.sum(): Money = this.map { it.totalAmount }.fold(zero, Money::plus)

    override fun toString(): String {
        return "PreOrder(restaurantId=$restaurantId, items=$items, discount=$discount, subTotal=$subTotal, total=$total)"
    }
}
