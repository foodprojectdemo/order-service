package com.gitlab.foodprojectdemo.orderservice.domain.exception

open class BusinessLogicException(message: String?, val code: String? = null) : RuntimeException(message)
