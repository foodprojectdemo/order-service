package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import reactor.core.publisher.Mono

interface PromoCodeRepository {
    fun getRestaurantPromoCode(promoCodeId: PromoCodeId, restaurantId: RestaurantId): Mono<PromoCode>
}
