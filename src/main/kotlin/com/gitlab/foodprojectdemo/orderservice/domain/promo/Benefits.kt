package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.Dish
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Percent
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

sealed class Benefit

sealed class Discount : Benefit()

data class AbsoluteDiscount(val money: Money) : Discount()

data class PercentDiscount(val percent: Percent) : Discount()

data class Gift(val dish: Dish, val quantity: Quantity) : Benefit()
//
// class FreeShipping : Benefit()
//
// data class Bonus(val benefit: Int) : Benefit()
