package com.gitlab.foodprojectdemo.orderservice.domain.misc

import org.springframework.data.annotation.Transient

abstract class RootAggregate<A : RootAggregate<A, ID>, ID : Any>(id: ID) : Entity<ID>(id) {
    @Transient
    private val camelRegex = "(?<=[a-zA-Z])[A-Z]".toRegex()

    @Transient
    private val snakeRegex = "_[a-zA-Z]".toRegex()

    private fun String.camelToSnakeCase(): String {
        return camelRegex.replace(this) {
            "_${it.value}"
        }.toLowerCase()
    }

    @Transient
    val name = this::class.simpleName!!.camelToSnakeCase()

    @Transient
    private val _events = mutableListOf<DomainEvent<A, ID>>()
    val events: List<DomainEvent<A, ID>>
        get() = _events.toList()

    protected fun addEvent(event: DomainEvent<A, ID>) {
        _events.add(event)
    }

    fun clearEvents() {
        _events.clear()
    }
}
