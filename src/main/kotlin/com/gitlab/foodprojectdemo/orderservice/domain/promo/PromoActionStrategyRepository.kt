package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

interface PromoActionStrategyRepository {
    fun <T : PromoActionStrategy> findByRestaurantId(restaurantId: RestaurantId, strategyClass: KClass<T>): Mono<T>
}
