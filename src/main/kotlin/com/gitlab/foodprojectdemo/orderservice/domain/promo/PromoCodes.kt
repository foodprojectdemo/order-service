package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import java.io.Serializable

data class PromoCodeId(private val code: String) : Serializable {
    override fun toString() = code
}

sealed class PromoCode : Promo {
    abstract val id: PromoCodeId
    abstract override val restaurantId: RestaurantId
    abstract val benefit: Benefit
}

data class AbsoluteDiscountPromoCode(
    override val id: PromoCodeId,
    override val restaurantId: RestaurantId,
    override val benefit: AbsoluteDiscount
) : PromoCode()

data class PercentDiscountPromoCode(
    override val id: PromoCodeId,
    override val restaurantId: RestaurantId,
    override val benefit: PercentDiscount
) : PromoCode()

data class GiftPromoCode(
    override val id: PromoCodeId,
    override val restaurantId: RestaurantId,
    override val benefit: Gift
) : PromoCode()
