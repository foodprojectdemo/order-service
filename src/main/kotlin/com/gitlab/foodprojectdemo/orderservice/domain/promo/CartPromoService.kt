package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Money.Companion.zero
import com.gitlab.foodprojectdemo.orderservice.toBenefit
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.function.TupleUtils.function
import reactor.kotlin.core.publisher.toMono

@Service
class CartPromoService(
    private val cartPromoActionRepository: CartPromoActionRepository,
    private val promoActionStrategyResolver: PromoActionStrategyResolver,
    private val promoCodeRepository: PromoCodeRepository
) {
    private val defaultBenefit = zero.toBenefit().toMono()

    fun applyTo(cart: Cart): Mono<Cart> {
        if (cart.isEmpty)
            return cart.toMono()

        val benefits = promoBenefits(cart).cache()

        return Mono.zip(
            cart.toMono(),
            discount(cart, benefits),
            gifts(cart, benefits),
            promoCodeBenefit(cart)
        ).map(function(::applyAllPromo))
    }

    private fun discount(cart: Cart, benefits: Flux<Benefit>): Mono<Money> {
        val discounts = benefits
            .filter { it is Discount }
            .cast(Discount::class.java)
            .flatMap { discount ->
                when (discount) {
                    is AbsoluteDiscount -> discount.money
                    is PercentDiscount -> discount.percent * cart.subTotal
                }.toMono()
            }.collectList()

        return getDiscountPromoActionStrategy(cart.restaurantId).zipWith(discounts)
            .map(function(DiscountPromoActionStrategy::define))
    }

    private fun gifts(cart: Cart, benefits: Flux<Benefit>): Mono<Collection<Gift>> {
        val gifts = benefits
            .filter { it is Gift }
            .cast(Gift::class.java)
            .collectList()

        return getGiftPromoActionStrategy(cart.restaurantId).zipWith(gifts)
            .map(function(GiftPromoActionStrategy::define))
    }

    private fun promoBenefits(cart: Cart): Flux<Benefit> =
        cartPromoActionRepository.getRestaurantCartPromoActions(cart.restaurantId)
            .flatMap { cartPromoAction ->
                cartPromoAction.verify(cart).toMono()
            }

    private fun promoCodeBenefit(cart: Cart): Mono<out Benefit> =
        cart.promoCodeId?.let { promoCodeId ->
            promoCodeRepository.getRestaurantPromoCode(promoCodeId, cart.restaurantId)
                .map(PromoCode::benefit)
                .switchIfEmpty(defaultBenefit)
        } ?: defaultBenefit

    private fun applyAllPromo(cart: Cart, discount: Money, gifts: Collection<Gift>, promoCodeBenefit: Benefit): Cart {
        resetPromo(cart)
        cart add discount
        gifts.forEach { gift -> cart.addGift(gift.dish, gift.quantity) }

        when (promoCodeBenefit) {
            is AbsoluteDiscount -> cart add promoCodeBenefit.money
            is PercentDiscount -> cart add cart.subTotal * promoCodeBenefit.percent
            is Gift -> cart.addGift(promoCodeBenefit.dish, promoCodeBenefit.quantity)
        }

        return cart
    }

    private fun resetPromo(cart: Cart) {
        cart assignDiscount zero
        cart.clearGifts()
    }

    private fun getDiscountPromoActionStrategy(restaurantId: RestaurantId): Mono<DiscountPromoActionStrategy> =
        promoActionStrategyResolver.discountPromoActionStrategy(restaurantId)
            .defaultIfEmpty(MaxDiscountPromoActionStrategy)

    private fun getGiftPromoActionStrategy(restaurantId: RestaurantId): Mono<GiftPromoActionStrategy> =
        promoActionStrategyResolver.giftPromoActionStrategy(restaurantId)
            .defaultIfEmpty(AllGiftsPromoActionStrategy)
}
