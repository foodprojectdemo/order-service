package com.gitlab.foodprojectdemo.orderservice.domain.promo

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import reactor.core.publisher.Mono

interface PromoActionStrategyResolver {
    fun discountPromoActionStrategy(restaurantId: RestaurantId): Mono<DiscountPromoActionStrategy>
    fun giftPromoActionStrategy(restaurantId: RestaurantId): Mono<GiftPromoActionStrategy>
}
