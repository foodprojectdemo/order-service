package com.gitlab.foodprojectdemo.orderservice.domain.order

import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.DomainRepository
import reactor.core.publisher.Flux

interface OrderRepository : DomainRepository<Order, OrderId> {
    fun findByCustomerId(customerId: CustomerId): Flux<Order>
    fun findByRestaurantId(restaurantId: RestaurantId): Flux<Order>
}
