package com.gitlab.foodprojectdemo.orderservice.domain.misc

import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException

@Suppress("DataClassPrivateConstructor")
data class Quantity private constructor(private val quantity: Int) : Number(), Comparable<Quantity> {

    companion object {
        val empty = Quantity(0)
        val one = Quantity(1)

        operator fun invoke(quantity: Int): Quantity {
            if (quantity < 1) throw InvalidValueBusinessLogicException("Quantity must be greater than 0, but was given $quantity")
            return Quantity(quantity)
        }
    }

    operator fun plus(other: Quantity) = Quantity(this.quantity + other.quantity)

    operator fun minus(quantity: Quantity): Quantity {
        return Quantity(this.quantity - quantity.quantity)
    }

    override operator fun compareTo(other: Quantity): Int {
        return quantity.compareTo(other.quantity)
    }

    override fun toByte(): Byte = quantity.toByte()

    override fun toChar(): Char = quantity.toChar()

    override fun toDouble(): Double = quantity.toDouble()

    override fun toFloat(): Float = quantity.toFloat()

    override fun toInt(): Int = quantity

    override fun toLong(): Long = quantity.toLong()

    override fun toShort(): Short = quantity.toShort()

    override fun toString(): String {
        return quantity.toString()
    }
}
