package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.createdResponseEntity
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.CartPresentation
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.IdResponse
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/v1/carts")
class CartController(private val cartService: CartService) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @PostMapping
    @ResponseStatus(CREATED)
    fun create(serverHttpRequest: ServerHttpRequest): Mono<ResponseEntity<IdResponse<CartId>>> =
        cartService.createCart()
            .doOnNext {
                log.info("Created new cart: {}", it)
            }.map {
                createdResponseEntity(serverHttpRequest, it.id)
            }

    @GetMapping("{cartId}")
    fun get(@PathVariable cartId: CartId): Mono<CartPresentation> =
        cartService.getCart(cartId)
            .map(CartPresentation.Companion::from)
            .doOnNext {
                log.info("Get cart: {}", it)
            }
}
