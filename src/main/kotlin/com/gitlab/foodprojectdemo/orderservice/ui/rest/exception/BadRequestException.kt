package com.gitlab.foodprojectdemo.orderservice.ui.rest.exception

class BadRequestException(message: String, cause: Throwable) : RuntimeException(message, cause)
