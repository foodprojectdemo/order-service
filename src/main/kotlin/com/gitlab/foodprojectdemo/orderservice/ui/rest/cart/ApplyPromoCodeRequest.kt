package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ApplyPromoCodeCommand
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.promo.PromoCodeId

data class ApplyPromoCodeRequest(val promoCodeId: PromoCodeId) {
    fun toCommand(cartId: CartId) =
        ApplyPromoCodeCommand(cartId, promoCodeId)
}
