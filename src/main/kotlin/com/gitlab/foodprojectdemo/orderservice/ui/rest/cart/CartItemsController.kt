package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.cart.command.RemoveCartItemCommand
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.DishNotFoundException
import com.gitlab.foodprojectdemo.orderservice.createdResponseEntity
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.exception.BadRequestException
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.IdResponse
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.onErrorMap
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/api/v1/carts/{cartId}/items")
class CartItemsController(private val cartService: CartService) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @PostMapping
    @ResponseStatus(CREATED)
    fun addDish(
        @PathVariable cartId: CartId,
        @RequestBody request: Mono<AddDishToCartRequest>,
        serverHttpRequest: ServerHttpRequest
    ): Mono<ResponseEntity<IdResponse<Int>>> =
        request.map { it.toCommand(cartId) }
            .doOnNext {
                log.info("Add dish command: {}", it)
            }
            .flatMap(cartService::addDishToCart)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .onErrorMap(DishNotFoundException::class) { BadRequestException(it.message, it) }
            .map {
                createdResponseEntity(serverHttpRequest, it.items.lastIndex)
            }

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    fun clear(@PathVariable cartId: CartId): Mono<Void> =
        cartService.clearCart(cartId)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .then()

    @PatchMapping("/{index}")
    @ResponseStatus(NO_CONTENT)
    fun changeQuantity(
        @PathVariable cartId: CartId,
        @PathVariable index: Int,
        @RequestBody request: Mono<ChangeCartItemQuantityRequest>
    ): Mono<Void> =
        request.map { it.toCommand(cartId, index) }
            .doOnNext {
                log.info("Change quantity command: {}", it)
            }.flatMap(cartService::changeCartItemQuantity)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .then()

    @DeleteMapping("/{index}")
    @ResponseStatus(NO_CONTENT)
    fun removeItem(@PathVariable cartId: CartId, @PathVariable index: Int): Mono<Void> =
        RemoveCartItemCommand(cartId, index).toMono()
            .doOnNext {
                log.info("Remove item command: {}", it)
            }.flatMap(cartService::removeCartItem)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .then()
}
