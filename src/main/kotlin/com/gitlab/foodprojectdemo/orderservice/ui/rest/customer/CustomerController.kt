package com.gitlab.foodprojectdemo.orderservice.ui.rest.customer

import com.gitlab.foodprojectdemo.orderservice.application.customer.CustomerService
import com.gitlab.foodprojectdemo.orderservice.createdResponseEntity
import com.gitlab.foodprojectdemo.orderservice.domain.customer.Customer
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.ui.rest.exception.NotFoundException
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.IdResponse
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/api/v1/customers")
class CustomerController(private val customerService: CustomerService) {

    @PostMapping
    fun signUp(serverHttpRequest: ServerHttpRequest): Mono<ResponseEntity<IdResponse<CustomerId>>> =
        customerService.signUpCustomer()
            .map { customer ->
                createdResponseEntity(serverHttpRequest, customer.id)
            }

    @GetMapping("/{customerId}")
    fun get(@PathVariable customerId: CustomerId): Mono<Customer> =
        customerService.getCustomer(customerId)
            .switchIfEmpty(NotFoundException("Customer with ID=$customerId not found").toMono())
}
