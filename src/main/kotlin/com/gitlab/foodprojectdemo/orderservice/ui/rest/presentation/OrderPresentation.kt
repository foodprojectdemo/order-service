package com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation

import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.domain.order.Address
import com.gitlab.foodprojectdemo.orderservice.domain.order.ContactInformation
import com.gitlab.foodprojectdemo.orderservice.domain.order.Order
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderItem

@Suppress("DataClassPrivateConstructor")
data class OrderPresentation private constructor(
    val id: String,
    val customerId: String,
    val restaurantId: RestaurantId,
    val status: String,
    val address: Address,
    val contactInformation: ContactInformation,
    val items: Collection<OrderItemPresentation>,
    val subTotal: String,
    val discount: String,
    val total: String,
) {
    constructor(order: Order) : this(
        order.id.toString(),
        order.customerId.toString(),
        order.restaurantId,
        order.status.name,
        order.address,
        order.contactInformation,
        order.items.map(OrderItemPresentation.Companion::from),
        order.subTotal.toString(),
        order.discount.toString(),
        order.total.toString()
    )
}

data class OrderItemPresentation(
    val dishId: String,
    val name: String,
    val price: String,
    val quantity: Int,
    val totalAmount: Int,
) {
    companion object {
        fun from(orderItem: OrderItem) = OrderItemPresentation(
            orderItem.dishId.toString(),
            orderItem.name,
            orderItem.price.toString(),
            orderItem.quantity.toInt(),
            orderItem.totalAmount.toInt()
        )
    }
}
