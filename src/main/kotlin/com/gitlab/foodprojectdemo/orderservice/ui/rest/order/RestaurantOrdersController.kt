package com.gitlab.foodprojectdemo.orderservice.ui.rest.order

import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.domain.dish.RestaurantId
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.OrderPresentation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
@RequestMapping("/api/v1/restaurant/{restaurantId}/orders")
class RestaurantOrdersController(private val orderService: OrderService) {

    @GetMapping
    fun restaurantOrders(@PathVariable restaurantId: RestaurantId): Flux<OrderPresentation> =
        orderService.getRestaurantOrders(restaurantId).map(::OrderPresentation)
}
