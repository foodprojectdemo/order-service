package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.command.ChangeCartItemQuantityCommand
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class ChangeCartItemQuantityRequest(val quantity: Quantity) {
    fun toCommand(cartId: CartId, index: Int) =
        ChangeCartItemQuantityCommand(cartId, index, quantity)
}
