package com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation

data class IdResponse<T>(val id: T)
