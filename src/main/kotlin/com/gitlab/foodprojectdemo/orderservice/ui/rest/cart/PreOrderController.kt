package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.PreOrderPresentation
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/v1/carts/{cartId}/pre-order")
class PreOrderController(private val cartService: CartService) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun placePreOrder(
        @PathVariable cartId: CartId,
        serverHttpRequest: ServerHttpRequest
    ): Mono<ResponseEntity<Void>> =
        cartService.placePreOrder(cartId)
            .doOnNext {
                log.info("Created pre order: {}", it)
            }.map {
                ResponseEntity.created(serverHttpRequest.uri).build()
            }

    @GetMapping
    fun getPreOrder(@PathVariable cartId: CartId): Mono<PreOrderPresentation> =
        cartService.getPreOrder(cartId)
            .map(PreOrderPresentation.Companion::from)
            .doOnNext {
                log.info("Get pre order: {}", it)
            }
}
