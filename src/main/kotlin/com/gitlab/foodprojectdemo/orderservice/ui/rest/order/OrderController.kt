package com.gitlab.foodprojectdemo.orderservice.ui.rest.order

import com.gitlab.foodprojectdemo.orderservice.application.order.CheckoutCommand
import com.gitlab.foodprojectdemo.orderservice.application.order.OrderService
import com.gitlab.foodprojectdemo.orderservice.createdResponseEntity
import com.gitlab.foodprojectdemo.orderservice.domain.customer.CustomerId
import com.gitlab.foodprojectdemo.orderservice.domain.order.OrderId
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.IdResponse
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.OrderPresentation
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/v1/orders")
class OrderController(private val orderService: OrderService) {

    @Suppress("LeakingThis")
    private val log = logger(this)

    @GetMapping
    fun orders(@RequestParam customerId: CustomerId): Flux<OrderPresentation> =
        orderService.getCustomerOrders(customerId).map(::OrderPresentation)

    @GetMapping("/{orderId}")
    fun order(@PathVariable orderId: OrderId): Mono<OrderPresentation> =
        orderService.getOrder(orderId).map(::OrderPresentation)
            .doOnNext {
                log.info("Order: {}", it)
            }

    @PostMapping
    fun checkout(
        @RequestBody request: Mono<CheckoutCommand>,
        serverHttpRequest: ServerHttpRequest
    ): Mono<ResponseEntity<IdResponse<OrderId>>> =
        request
            .doOnNext {
                log.info("Checkout command: {}", it)
            }
            .flatMap(orderService::checkout)
            .doOnNext {
                log.info("Order: {}", it)
            }
            .map { order ->
                createdResponseEntity(serverHttpRequest, order.id)
            }
}
