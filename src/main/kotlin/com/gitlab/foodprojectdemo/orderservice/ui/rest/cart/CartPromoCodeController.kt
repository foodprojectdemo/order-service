package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.CartService
import com.gitlab.foodprojectdemo.orderservice.application.cart.exception.PromoCodeNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.exception.BadRequestException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.onErrorMap

@RestController
@RequestMapping("/api/v1/carts/{cartId}/promo-code")
class CartPromoCodeController(private val cartService: CartService) {
    @Suppress("LeakingThis")
    private val log = logger(this)

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun apply(@PathVariable cartId: CartId, @RequestBody request: Mono<ApplyPromoCodeRequest>): Mono<Void> =
        request.map { it.toCommand(cartId) }
            .doOnNext {
                log.info("Apply promo code command: {}", it)
            }
            .flatMap(cartService::applyPromoCode)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .onErrorMap(PromoCodeNotFoundException::class) { BadRequestException(it.message, it) }
            .then()

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun remove(@PathVariable cartId: CartId): Mono<Void> =
        cartService.removePromoCode(cartId)
            .doOnNext {
                log.info("Cart: {}", it)
            }
            .then()
}
