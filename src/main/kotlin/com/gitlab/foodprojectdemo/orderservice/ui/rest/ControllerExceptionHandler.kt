package com.gitlab.foodprojectdemo.orderservice.ui.rest

import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationException
import com.gitlab.foodprojectdemo.orderservice.application.exception.ApplicationNotFoundException
import com.gitlab.foodprojectdemo.orderservice.domain.exception.BusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.domain.exception.InvalidValueBusinessLogicException
import com.gitlab.foodprojectdemo.orderservice.logger
import com.gitlab.foodprojectdemo.orderservice.ui.rest.exception.BadRequestException
import com.gitlab.foodprojectdemo.orderservice.ui.rest.exception.NotFoundException
import com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.server.ServerWebInputException
import kotlin.reflect.KClass

@RestControllerAdvice
class ControllerExceptionHandler {
    private val log = logger(this)

    private fun <T : Throwable> findCause(throwable: Throwable, expected: KClass<T>): T? {
        if (expected.isInstance(throwable)) {
            @Suppress("UNCHECKED_CAST")
            return throwable as T
        } else {
            return throwable.cause?.let { findCause(it, expected) }
        }
    }

    @ExceptionHandler(ServerWebInputException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private fun serverWebInputException(exception: ServerWebInputException): ErrorResponse {
        log.info("serverWebInputException")
        log.debug(exception.toString(), exception)

        return findCause(exception, InvalidValueBusinessLogicException::class)?.let {
            ErrorResponse(
                it.message
            )
        } ?: ErrorResponse("Bad Request")
    }

    @ExceptionHandler(IllegalArgumentException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private fun illegalArgumentException(exception: IllegalArgumentException): ErrorResponse {
        log.info("illegalArgumentException")
        log.info(exception.message, exception)
        log.debug(exception.toString(), exception)
        return ErrorResponse("Bad Request")
    }

    @ExceptionHandler(BusinessLogicException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    private fun domainError(exception: BusinessLogicException): ErrorResponse {
        log.debug(exception.toString(), exception)
        return ErrorResponse(exception.message, exception.code)
    }

    @ExceptionHandler(ApplicationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private fun applicationError(exception: ApplicationException): ErrorResponse {
        log.debug(exception.toString(), exception)
        return ErrorResponse(exception.message)
    }

    @ExceptionHandler(BadRequestException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private fun badRequestError(exception: BadRequestException): ErrorResponse {
        log.debug(exception.toString(), exception)
        return ErrorResponse(exception.message)
    }

    @ExceptionHandler(ApplicationNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private fun exception(notFoundException: ApplicationNotFoundException): ErrorResponse {
        log.debug(notFoundException.toString(), notFoundException)
        return ErrorResponse(notFoundException.message)
    }

    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private fun exception(notFoundException: NotFoundException): ErrorResponse {
        log.debug(notFoundException.toString(), notFoundException)
        return ErrorResponse(notFoundException.message)
    }

    @ExceptionHandler(Throwable::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private fun exception(e: Throwable): ErrorResponse {
        log.info(e.toString(), e)
        return ErrorResponse(e.message)
    }
}
