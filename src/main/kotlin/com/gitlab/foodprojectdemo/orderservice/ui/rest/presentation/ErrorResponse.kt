package com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation

data class ErrorResponse(val message: String?, val error: String? = null)
