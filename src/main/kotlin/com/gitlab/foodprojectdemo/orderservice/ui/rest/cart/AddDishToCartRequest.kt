package com.gitlab.foodprojectdemo.orderservice.ui.rest.cart

import com.gitlab.foodprojectdemo.orderservice.application.cart.command.AddDishToCartCommand
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartId
import com.gitlab.foodprojectdemo.orderservice.domain.dish.DishId
import com.gitlab.foodprojectdemo.orderservice.domain.misc.Quantity

data class AddDishToCartRequest(val dishId: DishId, val quantity: Quantity) {
    fun toCommand(cartId: CartId) =
        AddDishToCartCommand(cartId, dishId, quantity)
}
