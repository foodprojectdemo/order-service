package com.gitlab.foodprojectdemo.orderservice.ui.rest.presentation

import com.gitlab.foodprojectdemo.orderservice.domain.cart.Cart
import com.gitlab.foodprojectdemo.orderservice.domain.cart.CartItem
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrder
import com.gitlab.foodprojectdemo.orderservice.domain.cart.PreOrderItem

data class CartItemPresentation(
    val dishId: String,
    val price: String,
    val totalAmount: String,
    val quantity: Int
) {
    companion object {
        fun from(cartItem: CartItem) = CartItemPresentation(
            cartItem.dish.id.toString(),
            cartItem.dish.price.toString(),
            cartItem.totalAmount.toString(),
            cartItem.quantity.toInt()
        )

        fun from(preOrderItem: PreOrderItem) = CartItemPresentation(
            preOrderItem.dishId.toString(),
            preOrderItem.price.toString(),
            preOrderItem.totalAmount.toString(),
            preOrderItem.quantity.toInt()
        )
    }
}

data class CartPresentation(
    val id: String,
    val items: List<CartItemPresentation>,
    val gifts: List<CartItemPresentation>,
    val total: String,
    val subTotal: String,
    val discount: String,
    val promoCodeId: String?
) {

    companion object {
        fun from(cart: Cart) = CartPresentation(
            cart.id.toString(),
            cart.items.map(CartItemPresentation.Companion::from),
            cart.gifts.map(CartItemPresentation.Companion::from),
            cart.total.toString(),
            cart.subTotal.toString(),
            cart.discount.toString(),
            cart.promoCodeId?.toString()
        )
    }
}

data class PreOrderPresentation(
    val restaurantId: String,
    val items: List<CartItemPresentation>,
    val total: String,
    val subTotal: String,
    val discount: String

) {
    companion object {
        fun from(preOrder: PreOrder) = PreOrderPresentation(
            preOrder.restaurantId.toString(),
            preOrder.items.map(CartItemPresentation.Companion::from),
            preOrder.total.toString(),
            preOrder.subTotal.toString(),
            preOrder.discount.toString()
        )
    }
}
